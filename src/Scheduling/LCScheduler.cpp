/*
 * LCScheduler.cpp
 *
 *  Created on: 10.07.2014
 *      Author: marc
 */

#include <Scheduling/LCScheduler.hpp>
#include <Scheduling/MCPScheduler.hpp>
#include <Graph/MCGraphAnalyser.hpp>
#include <Graph/ClusterGraph.hpp>

#include <boost/unordered_map.hpp>

#include <iostream>


LCScheduler::LCScheduler(MCGraph const * in, MPIParameter const & sp) : ClusterScheduler(in,sp)
{

}

LCScheduler::~LCScheduler()
{

}

template<typename T>
std::vector<T> cut(std::vector<T> const & in, stype n)
{
	std::vector<T> res;
	for(stype i=0;i<in.size();++i)
	{
		if(i==n)
		{
			continue;
		}
		res.push_back(in[i]);
	}
	return res;
}
template<typename T>
std::vector<std::vector<T> > cut(std::vector<std::vector<T> > const & in, stype k, stype l)
{
	std::vector<std::vector<T> > res;
	for(stype i=0;i<in.size();++i)
	{
		if(i==k) continue;
		res.push_back(cut(in[i],l));
	}
	return res;
}

template<typename T>
void print(std::vector<T> const & in)
{
	for(stype i=0;i<in.size();++i)
	{
		std::cout << in[i] << "\n";
	}
}


double connectivity(stype e, stype v1, stype v2)
{
	if(v1<v2) return e/v1;
	else return e/v2;
}



std::pair<std::list<const MCNode *>, MCNode::cost_type> LCScheduler::rec_crit_path_not_visited(const MCNode * first, std::vector<bool> & visited) const
{
	std::pair<std::list<const MCNode *>, MCNode::cost_type> res, temp;
	res.second = 0;
	stype id = 0;
	for(auto it : *first)
	{
		temp = rec_crit_path_not_visited(it.second,visited);
		if(temp.second>res.second || (temp.second == res.second && it.first < id))
		{
			res = temp;
			id = it.first;
		}
	}

	if(!visited[first->get_id()])
	{
		res.second += first->get_costs();
		res.first.push_front(first);
	}
	return res;
}

UnorderedSchedule LCScheduler::get_cluster()
{
	UnorderedSchedule sched;
	stype num_visited = 0, tempu;
	std::vector<bool> visited(g->size(),false);
	if(buffer.size()!=0)
		return buffer;
	while(num_visited<g->size())
	{
		tempu = sched.push_back(rec_crit_path_not_visited(g->get_first(),visited).first);
		num_visited+=sched[tempu].size();

		for(auto it = sched[tempu].begin();it!=sched[tempu].end();++it)
		{
			visited[(*it)->get_id()] = true;

		}

	}
	buffer = sched;
	return sched;
}
