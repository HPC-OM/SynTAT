/*
 * MCScheduler.cpp
 *
 *  Created on: 10.07.2014
 *      Author: marc
 */

#include <Scheduling/MCScheduler.hpp>

#include <vector>
#include <algorithm>
#include <Scheduling/OrderedSchedule.hpp>
#include <Graph/MCGraphAnalyser.hpp>
#include <Graph/MCGraph_lib.hpp>
#include <IO/GraphMLReader.hpp>

#include <Misc/math_opt_funcs.hpp>


MCScheduler::MCScheduler(MCGraph const * in, MPIParameter const & sp) : g(in),sp(&sp) {
	shared_comm_costs = 4000;
	distri_comm_costs = 20000;
}

MCScheduler::~MCScheduler() {

}

MCGraph const * MCScheduler::get_graph() const
{
	return g;
}

MCNode::cost_type MCScheduler:: makespan(UnorderedSchedule const & in, MCGraph const * g) const
{
	MCNode::cost_type res = 0;
	auto mks = get_makespans(in,g);
	for(stype i=0;i<mks.size();++i)
		if(res < mks[i])
			res = mks[i];
	return res;
}

std::vector<MCNode::cost_type> MCScheduler::get_makespans(UnorderedSchedule const & in) const
{
	return get_makespans(in,g);
}

std::vector<MCNode::cost_type> MCScheduler::get_makespans(UnorderedSchedule const & in, MCGraph const * in_g) const
{
	SingleLinkGraph sg(in_g);

	GraphMLReader r;
	if(!in.check())
	{
		std::cout << "get makespan: no valid sched\n";
		exit(0);
	}
	if(in.size() == 1)
	{
		std::vector<MCNode::cost_type> res(1,0);
		for(unsigned i=0;i<in_g->size();++i)
			res[0] += in_g->at(i)->get_costs();
		return res;
	}
	std::vector<stype> sends(in.size(),0);

	std::vector<stype> recvs(sg.size()), tmap = in.get_task_map(), lvl = get_lvls(in_g);
	for(unsigned i=0;i<sg.size();++i)
	{
		for(auto p : *sg[i])
		{
			if(tmap[p.first]!=tmap[sg[i]->get_id()])
			{
				if(in.size()==1)
					std::cout << "hier stimmt was nicht\n";
				++recvs[p.first];
			}
		}
	}
	for(stype i=0;i<in.size();++i)
	{
		const MCNode * last = NULL;
		for(const MCNode * n : in[i])
		{
			stype count_sends = 0;
			for(auto p : *n)
			{
				MCNode::cost_type temp = 0;
				if(tmap[n->get_id()] != tmap[p.first])
				{
					temp = sp->get_latency();
					++count_sends;
				}
				sg.at(n->get_id())->set_comm_costs(p.first,temp);
			}
			sends[i] += count_sends;
			sg.at(n->get_id())->set_costs(n->get_costs()+sp->get_send_costs(count_sends)+sp->get_recv_costs(recvs[n->get_id()]));
			if(last !=NULL)
			{
				if(n->get_id() == last->get_id())
					std::cout << n->get_id() << " = " << last->get_id() << "same\n";
				sg.make_edge(last->get_id(),n->get_id(),0);
			}
			last = n;

		}
	}
	//r.write("out2.graphml",&sg);
	std::vector<MCNode::cost_type> blvls = get_blvls(&sg),res(in.size(),0);
	for(stype i=0;i<in.size();++i)
	{
		res[i] = blvls[in[i].front()->get_id()] + sp->get_wait(sends[i]);
	}
	return res;
}


UnorderedSchedule MCScheduler::bound_unbound_isolated(UnorderedSchedule const & in, stype num_clusters) const
{
	ClusterGraph cg(in);
	cg.sort_nodes();
	std::vector<stype> nodes = cg.get_cluster_nums();

	//greedy strategie: füge zu den größten [num_clusters] die übrigen kleinen hinzu, nicht ideal
	stype index = nodes.size()-num_clusters,temp;
	for(stype i=0;i<nodes.size()-num_clusters;++i)
	{
		if(index+1 == nodes.size()) temp =nodes.size()-num_clusters;
		else temp = index+1;
		if(cg[index]>cg[temp])
			index = temp;
		cg.merge(index,i);
	}
	return cg.get_unordered_schedule();
}




UnorderedSchedule MCScheduler::get_single_cluster_sched() const
{
	UnorderedSchedule sched(g->size());
	for(stype i=0;i<sched.size();++i)
		sched[i].insert(sched[i].end(),g->at(i));

	return sched;
}

UnorderedSchedule MCScheduler::get_default_schedule()
{
	OrderedSchedule<> sched(1);
	MCGraphAnalyser ga(g,sp);
	std::vector<AnalysedNode> vs = ga.get_analysed_nodes(AnalysedNode::LVL);

	std::sort(vs.begin(),vs.end(),AnalysedNode::lvl_comp);
	for(unsigned i=0;i<vs.size();++i) sched[0].insert(vs[i]);
	return sched;
}


UnorderedSchedule MCScheduler::get_schedule(stype num_cores, stype *nodes, bool merge)
{
		return get_schedule(num_cores,nodes);
}
