/*
 * TFScheduler.cpp
 *
 *  Created on: 08.08.2014
 *      Author: marc
 */

#include <Scheduling/TFScheduler.hpp>
#include <Graph/AnalysedNode.hpp>
#include <Graph/MCGraphAnalyser.hpp>
#include <Scheduling/Schedule.hpp>
#include <set>
#include <map>
#include <limits>
#include <algorithm>


TFScheduler::TFScheduler(MCGraph const * in, MPIParameter const & sp) : MCScheduler(in,sp) {

}

TFScheduler::~TFScheduler() {

}


UnorderedSchedule TFScheduler::get_schedule(stype num_cores, stype * nodes)
{
	MCGraphAnalyser ga(g,sp);
	std::vector<AnalysedNode> vs = ga.get_analysed_nodes(AnalysedNode::BLVL);
	Schedule sched(g,*sp,num_cores);

	std::list<const MCNode *> free_l = sched.free();

	long long int temp, min = 0;
	stype core = 0;
	const MCNode * n;

	while(!free_l.empty())
	{
		min = std::numeric_limits<long long int>::max();
		core = 0;
		n = free_l.front();
		free_l.pop_front();
		for(stype i=0;i<num_cores;++i)
		{
			temp = sched.earliest_start_time_on(n,i);
			if(temp < min || (temp == min && vs[n->get_id()].get_blvl() > vs[n->get_id()].get_blvl()))
			{
				min = temp;
				core = i;
			}
		}
		sched.schedule(n,sched.earliest_time(n,core),core);
		free_l = sched.free();
	}

	return sched.get_unordered_sched();
}

UnorderedSchedule TFScheduler::get_schedule_unbound()
{

	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule TFScheduler::get_schedule_distributed()
{
	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule TFScheduler::sub_schedule(UnorderedSchedule const & in,MCGraphAnalyser & ga) const
{
	struct cmp
	{
		bool operator()(const SchedStream * lhs, const SchedStream * rhs) const
		{
			return lhs->id < rhs->id;
		}

	};
	UnorderedSchedule res(in.size());
	std::vector<AnalysedNode> anodes = ga.get_analysed_nodes(AnalysedNode::ALAP);
	for(stype i=0;i<in.size();++i)
	{
		std::set<AnalysedNode,AnalysedNode::alap_comp_struct> sub;
		for(const MCNode * n : in[i])
		{
			sub.insert(anodes[n->get_id()]);
		}
		for(const AnalysedNode & a : sub)
		{
			res[i].push_back(a.get_node());
		}
	}



	return res;
}
