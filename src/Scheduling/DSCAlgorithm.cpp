/*
 * DSCAlgorithm.cpp
 *
 *  Created on: 25.11.2014
 *      Author: marc
 */

#include <Scheduling/DSCAlgorithm.hpp>
#include <Graph/MCGraph_lib.hpp>
#include <limits>

DSCAlgorithm::DSCAlgorithm(const MCGraph * in,MPIParameter const & sp) : ClusterScheduler(in,sp) {


}

DSCAlgorithm::~DSCAlgorithm() {

}

UnorderedSchedule DSCAlgorithm::get_cluster()
{
	//std::cout << "num tasks: " << g->size() << "\n";
	typedef std::pair<MCNode::cost_type,MCNode::cost_type> cost_pair;
	UnorderedSchedule res;
	SingleLinkGraph sg(g);
	std::vector<MCNode::cost_type> tlvl(sg.size(),0),blvl;
	std::vector<MCNode::cost_type> real_costs(sg.size(),0), send_costs(sg.size(),0);
	std::vector<MCNode::cost_type> node_start(sg.size(),0), cluster_end(0,std::numeric_limits<MCNode::cost_type>::max());
	std::vector<stype> num_sends(sg.size(),0), num_recvs(sg.size(),0);
	std::vector<stype> cluster(sg.size(),std::numeric_limits<stype>::max()),deps;
	MCGraph * tg = sg.travers();

	const MCNode * n, * pn;
	MCNode * ncn;
	MCNode::cost_type temp, best_cl_costs, p_best_cl_costs, p_worst_n_costs, worst_n_costs, temp_cl_costs, temp_n_costs,prio_tmp;
	stype p_best_cl,best_cl;

	struct Priority
	{
		bool operator()(std::pair<MCNode::cost_type,MCNode::cost_type> const & i, std::pair<MCNode::cost_type,MCNode::cost_type> const & j) const
		{
			return i.first+i.second > j.first+j.second;
		}
	};
		// Free list mit Sortierung zum Log n finden des nächsten Knotens
	Priority prio;


	std::multimap<std::pair<MCNode::cost_type,MCNode::cost_type>,const MCNode *,Priority> free_l(prio),
																						  p_free_l(prio),prev_l;
	std::vector<std::multimap<std::pair<MCNode::cost_type,MCNode::cost_type>,const MCNode *,Priority>::iterator> free_ref(g->size(),free_l.end()),
																												 p_free_ref(g->size(),p_free_l.end());


	deps = find_dependencies(g);
	for(stype i=0;i<sg.size();++i)
	{
		temp = 0; // for costs
		ncn = sg.at(i);
		for(auto p : *ncn)
		{
			if(ncn->get_costs()!=0)
				ncn->set_comm_costs(p.first,sp->get_comm_time());
			else
				--deps[p.first];
		}
		if(ncn->get_costs()==0)
			continue;
		real_costs[i] = ncn->get_costs();
		num_sends[i] = ncn->size();
		num_recvs[i] = deps[i];
		send_costs[i] = sp->get_send_costs(ncn->size());

		temp += sp->get_recv_costs(num_recvs[i]);
		temp += send_costs[i];
		temp += ncn->get_costs();

		ncn->set_costs(temp);


	}
	blvl = get_blvls(&sg);
	std::list<std::pair<stype,MCNode::cost_type> > reduce;
	for(stype i=1;i<deps.size();++i)
	{
		if(deps[i]==0)
		{
			if(sg.at(i)->get_costs()!=0)
			{
				for(auto p : *(sg.at(i)))
				{
					reduce.push_back({p.first,sg.at(i)->get_costs()+sp->get_latency()});
				}
				cluster[i] = res.size();
				res.push_back(g->at(i));
				cluster_end.push_back(sg.at(i)->get_costs());
				tlvl[i] = sg.at(i)->get_costs();
			}
		}
	}
	res[0].push_front(g->get_first());
	for(auto p : reduce)
	{
		--deps[p.first];
		if(deps[p.first]==0)
		{
			MCNode::cost_type tc = p.second;

			if(p_free_ref[p.first]!=p_free_l.end())
			{
				tc = std::max(tc,p_free_ref[p.first]->first.first+p_free_ref[p.first]->first.second);
				p_free_l.erase(p_free_ref[p.first]);
				p_free_ref[p.first] = p_free_l.end();
			}
			free_ref[p.first] = free_l.insert({{tc,blvl[p.first]},sg.at(p.first)});
		}
		else
		{
			MCNode::cost_type tc = p.second;
			if(p_free_ref[p.first]!=p_free_l.end())
			{
				tc = std::max(tc,p_free_ref[p.first]->first.first+p_free_ref[p.first]->first.second);
				p_free_l.erase(p_free_ref[p.first]);
			}
			p_free_ref[p.first] = p_free_l.insert({{tc,blvl[p.first]},sg.at(p.first)});
		}

	}

	std::multimap<std::pair<MCNode::cost_type,MCNode::cost_type>,const MCNode *,Priority>::iterator it,save;
	std::list<stype> merge_nodes;
	bool flag = true;
	while(!free_l.empty())
	{
		n = free_l.begin()->second;
		if(p_free_l.empty())
		{
			pn = NULL;
			prio_tmp = 0;
		}
		else
		{
			pn = p_free_l.begin()->second;
		}
		prio_tmp = free_l.begin()->first.first+free_l.begin()->first.second;
		free_l.erase(free_l.begin());
		free_ref[n->get_id()] = free_l.end();
		worst_n_costs = 0;
		best_cl_costs = std::numeric_limits<MCNode::cost_type>::max();
		p_worst_n_costs = 0;
		p_best_cl_costs = std::numeric_limits<MCNode::cost_type>::max();
		best_cl = 0;
		p_best_cl = 0;
		prev_l.clear();
		merge_nodes.clear();
		// analyse the partial free node
		if(prio_tmp < p_free_l.begin()->first.first+p_free_l.begin()->first.second)
			for(auto p : *(tg->at(pn->get_id())))
			{
				if(cluster[p.first] == std::numeric_limits<stype>::max())
					continue;
				 // einmal tlvl ohne clustern und einmal tlvl mit!!!!
				temp_n_costs = tlvl[p.first] + sp->get_recv_costs(num_recvs[pn->get_id()]);
				//und einmal tlvl mit!!!!
				temp_cl_costs = (cluster_end[cluster[p.first]] + sp->get_send_costs(num_sends[p.first]-1) + sp->get_recv_costs(num_recvs[pn->get_id()]-1)) - sp->get_send_costs(num_sends[p.first]) ;
				if(temp_n_costs > p_worst_n_costs)
				{
					p_worst_n_costs = temp_n_costs;
					if(temp_cl_costs < p_best_cl_costs)
					{
						p_best_cl = p.first;
						p_best_cl_costs = temp_cl_costs;
					}
				}
			}

		// sort the preds of the first node
		for(auto p: *(tg->at(n->get_id())))
		{
			temp_n_costs = tlvl[p.first] + sp->get_recv_costs(num_recvs[n->get_id()]);
			//und einmal tlvl mit!!!!
			temp_cl_costs = (cluster_end[cluster[p.first]] + sp->get_send_costs(num_sends[p.first]-1) + sp->get_recv_costs(num_recvs[n->get_id()]-1)) - sp->get_send_costs(num_sends[p.first]) ;
			if(temp_n_costs > worst_n_costs)
			{
				worst_n_costs = temp_n_costs;
				if(temp_cl_costs < best_cl_costs)
				{
					best_cl = p.first;
					best_cl_costs = temp_cl_costs;
				}
			}
			prev_l.insert(std::pair<std::pair<MCNode::cost_type,MCNode::cost_type>,const MCNode *>(std::pair<MCNode::cost_type,MCNode::cost_type>(tlvl[p.first],sg[p.first]->get_costs()+sp->get_send_costs(1)),g->at(p.first)));

		}
		flag = true; //if false n could stay in unit cluster
		bool is_one;
		best_cl = std::numeric_limits<stype>::max();
		for(it = prev_l.begin();it!=prev_l.end();++it)
		{
			is_one = true;
			if(it->second->size()>1 && it->first.first+it->first.second != 0)
			{
				for(auto p : *(it->second))
					if(cluster[p.first] == best_cl)
					{
						is_one = false;
						break;
					}
			}
			if(!is_one)
			{
				std::pair<std::pair<MCNode::cost_type, MCNode::cost_type>, const MCNode *> reseter = *it;
				it = prev_l.erase(it);
				reseter.first.first = 0;
				reseter.first.second = 0;
				prev_l.insert(reseter);
			}
		}
		// find all nodes to merge (minimizing procedure)

		it = prev_l.begin();
		stype prev_node;
		best_cl = it->second->get_id();
		MCNode::cost_type cur_costs = cluster_end[cluster[best_cl]];
		++it;
		while(it!=prev_l.end() && it->first.first+it->first.second != 0)
		{
			temp_cl_costs = res[cluster[it->second->get_id()]].get_costs();
			prev_node = it->second->get_id();

			if(cur_costs+temp_cl_costs <= best_cl_costs)
			{
				cur_costs += res[cluster[it->second->get_id()]].get_costs();
				merge_nodes.push_back(prev_node);
				if(cluster[it->second->get_id()] == p_best_cl)
					flag = false;

			}
			else
			{
				break;
			}
			++it;
		}



		// Test if minimizing was succes II
		//TLVL verbessert durch clustern?

		if(best_cl_costs < worst_n_costs && (p_best_cl_costs > p_worst_n_costs || flag))
		{
			if(cluster[n->get_id()])
			{
				//std::cout << "node " << n->get_id() << " is ok to sched\n";
			}
			else
			{
				std::cout << n->get_id() << " is already sched\n";
				exit(0);
			}
			MCNode::cost_type cur_start = cluster_end[cluster[best_cl]];
			//std::cout << "merge: ";
			for(stype m : merge_nodes)
			{
				//std::cout << m << " ";
				if(cluster[m] == cluster[best_cl])
					continue;
				res[cluster[best_cl]].insert(res[cluster[m]]);
				res[cluster[m]].clear();
				cluster[m] = cluster[best_cl];
				node_start[n->get_id()] = cur_start;
				cur_start += real_costs[m];
				tlvl[m] = cur_costs; //dadurch wird das tlvl invalidiert und zur Beendigungszeit des Knotens
			}
			//std::cout << "\n";
			cluster[n->get_id()] = cluster[best_cl];
			res[cluster[best_cl]].push_back(g->at(n->get_id()));
			--num_sends[best_cl];
			--num_recvs[n->get_id()];
			cluster_end[cluster[best_cl]] = std::max(best_cl_costs+sp->get_recv_costs(num_recvs[n->get_id()])-sp->get_recv_costs(num_recvs[n->get_id()]+1)  ,cur_start)+sp->get_send_costs(num_sends[best_cl]) - sp->get_send_costs(num_sends[best_cl]+1) + sp->get_send_costs(num_sends[n->get_id()]) + g->at(n->get_id())->get_costs() ;
			node_start[n->get_id()] = best_cl_costs;
			tlvl[n->get_id()] = cluster_end[cluster[best_cl]]; //dadurch wird das tlvl invalidiert und zur Beendigungszeit des Knotens
			//TODO Update der geschedulten Nachfolgern von best_cl (höherer aufwand)
			tlvl[best_cl] = tlvl[best_cl] - sp->get_send_costs(num_sends[best_cl]+1) + sp->get_send_costs(num_sends[best_cl]);
		}
		else
		{
			//seperates cluster
			//std::cout << "seperate\n";
			cluster[n->get_id()] = res.size();
			cluster_end.push_back(worst_n_costs + sp->get_send_costs(num_sends[n->get_id()]) + real_costs[n->get_id()]);
			tlvl[n->get_id()] = cluster_end[res.size()]; //dadurch wird das tlvl invalidiert und zur Beendigungszeit des Knotens

			res.push_back(g->at(n->get_id()));
			node_start[n->get_id()] = worst_n_costs;
		}
		//update tlvl und deps
		for(auto p : *(n))
		{
			temp = tlvl[n->get_id()] + sp->get_recv_costs(num_recvs[p.first]);
			if(temp>tlvl[p.first])
				tlvl[p.first] = temp;
			if(--deps[p.first] == 0)
			{
				MCNode::cost_type tc = node_start[n->get_id()] + sg.at(n->get_id())->get_costs() + sp->get_latency();
				if(p_free_ref[p.first] != p_free_l.end())
				{
					tc = std::max(tc, p_free_ref[p.first]->first.first);
					p_free_l.erase(p_free_ref[p.first]);
				}
				free_ref[p.first] = free_l.insert(std::pair<cost_pair,const MCNode *>(cost_pair(tc,blvl[p.first]),p.second));
				p_free_ref[p.first] = p_free_l.end();
			}
			else
			{
				MCNode::cost_type tc = node_start[n->get_id()] + sg.at(n->get_id())->get_costs() + sp->get_latency();
				if(p_free_ref[p.first] != p_free_l.end())
				{
					tc = std::max(p_free_ref[p.first]->first.first,tc);
						p_free_l.erase(p_free_ref[p.first]);
				}
				p_free_ref[p.first] = p_free_l.insert(std::pair<cost_pair,const MCNode *>(cost_pair(tc,blvl[p.first]),p.second));
			}

		}

	}

	delete tg;
	temp = 0;
	for(unsigned j=0;j<g->size();++j)
	{
		if(tlvl[j] > temp)
			temp = tlvl[j];
	}
	res.clear_empty();
	return res;
}
