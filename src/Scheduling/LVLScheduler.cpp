/*
 * LVLScheduler.cpp
 *
 *  Created on: 16.09.2014
 *      Author: marc
 */

#include <Scheduling/LVLScheduler.hpp>
#include <Graph/MCGraphAnalyser.hpp>
#include <Scheduling/OrderedSchedule.hpp>

#include <algorithm>

LVLScheduler::LVLScheduler(MCGraph const * in, MPIParameter const & sp)  : MCScheduler(in,sp) {

}

LVLScheduler::~LVLScheduler() {
}

UnorderedSchedule LVLScheduler::get_schedule(unsigned num_cores, unsigned * nodes)
{
	MCGraphAnalyser ga(g,sp);
	UnorderedSchedule res(num_cores);
	std::vector<AnalysedNode> vs = ga.get_analysed_nodes(AnalysedNode::LVL);
	stype max_lvl = 0;
	for(stype i=0;i<vs.size();++i)
		if(max_lvl<vs[i].get_lvl())
			max_lvl = vs[i].get_lvl();
	std::vector<std::list<const MCNode *> > lvls(max_lvl+1,std::list<const MCNode *>());
	for(stype i=0;i<vs.size();++i)
		lvls[vs[i].get_lvl()].push_back(g->at(i));

	for(stype i=0;i<lvls.size();++i)
		lvls[i].sort(AnalysedNode::lvl_comp);
	stype temp_p;
	MCNode::cost_type temp_c;
	std::multimap<MCNode::cost_type,stype> procs;
	for(stype i=0;i<lvls.size();++i)
	{
		for(unsigned j=0;j<num_cores;++j)
			procs.insert(std::pair<MCNode::cost_type,stype>(0,j));
		for(const MCNode * n : lvls[i])
		{
			temp_c = procs.begin()->first;
			temp_p = procs.begin()->second;
			procs.erase(procs.begin());
			procs.insert(std::pair<MCNode::cost_type,stype>(temp_c+n->get_costs(),temp_p));
			res[temp_p].push_back(n);
		}
	}

	return res;
}

UnorderedSchedule LVLScheduler::get_schedule_unbound()
{

	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule LVLScheduler::get_schedule_distributed()
{
	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}
