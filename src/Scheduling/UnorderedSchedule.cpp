/*
 * UnorderedSchedule.cpp
 *
 *  Created on: 09.07.2014
 *      Author: marc
 */

#include <Scheduling/UnorderedSchedule.hpp>
#include <map>

UnorderedSchedule::UnorderedSchedule(stype cores) {

	sched = std::vector<MCSingleSchedule>(cores,MCSingleSchedule());
}

UnorderedSchedule::UnorderedSchedule() {
}

UnorderedSchedule::~UnorderedSchedule() {
}

stype UnorderedSchedule::get_num_cores() const
{
	return sched.size();
}

stype UnorderedSchedule::get_num_tasks() const
{
	stype res = 0;
	for(stype i=0;i<size();++i) res += sched[i].size();
	return res;
}

stype UnorderedSchedule::size() const
{
	return get_num_cores();
}

void UnorderedSchedule::erase(stype core)
{
	sched.erase(sched.begin()+core);
}

bool UnorderedSchedule::check() const
{
	std::vector<bool> is_in(get_num_tasks(),false);
	bool flag = true;
	for(stype i=0;i<size();++i)
		for(const MCNode * n : sched[i])
			if(is_in[n->get_id()])
			{
				std::cout << n->get_id() << " is twice\n";
				return false;
			}
			else
				is_in[n->get_id()] = true;
	return flag;
}

stype UnorderedSchedule::push_back(const MCNode * node)
{
	stype res = sched.size();
	sched.push_back(MCSingleSchedule());
	sched[res].push_back(node);
	return res;
}

stype UnorderedSchedule::push_back(std::list<const MCNode *> const & in)
{
	stype res = sched.size();
	sched.push_back(MCSingleSchedule());
	sched[sched.size()-1].insert(in);
	return res;
}

void UnorderedSchedule::clear_empty()
{
	stype s=0,e=size()-1;
	while(s!=e)
	{
		if(!sched[s].empty())
			++s;
		else if(sched[e].empty())
			--e;
		else
		{
			sched[s] = (sched[e]);
			sched[e].clear();
		}
	}
	sched.erase(sched.begin()+e+1,sched.end());
}

void UnorderedSchedule::clear()
{
	sched.clear();
}


/**Don't use
 *
 */
stype UnorderedSchedule::get_num_task(stype core) const
{
	std::cout << "get_num_taks isnt implemented\n";
	return 0;
}

MCSingleSchedule UnorderedSchedule::get_schedule(stype core_num) const
{
	return sched[core_num];
}

MCSingleSchedule & UnorderedSchedule::operator[](stype num)
{
	return sched[num];
}

MCSingleSchedule UnorderedSchedule::operator[](stype num) const
{
	return sched[num];
}

UnorderedSchedule::cost_type UnorderedSchedule::get_costs() const
{
	cost_type res = sched[0].get_costs();
	for(stype i=1;i<sched.size();++i)
		res += sched[i].get_costs();
	return res;
}

UnorderedSchedule::cost_type UnorderedSchedule::get_max_costs() const
{
	cost_type res = sched[0].get_costs();
	for(stype i=1;i<sched.size();++i)
		if(res<sched[i].get_costs()) res = sched[i].get_costs();
	return res;
}

std::vector<stype> UnorderedSchedule::get_task_map() const
{
	stype n = get_num_tasks();
	std::vector<stype> res(n,0);
	for(stype i=0;i<sched.size();++i)
	{
		for(const MCNode * n : sched[i])
		{
			res[n->get_id()] = i;
		}
	}

	return res;
}

std::ostream & operator<<(std::ostream & in, UnorderedSchedule const & sched)
{
	for(stype i=0;i<sched.size();++i)
	{
		in << "core " << i << " (s:" << sched[i].size() << ",c:" << sched[i].get_costs() << "): ";

		//if(sched[i].size()>5) continue;
		for(const MCNode * n : sched[i])
			in << n->get_id() << " ";
		in << "\n";
	}

	return in;
}


std::vector<std::vector<std::pair<stype,bool> > > UnorderedSchedule::get_adj_matrix() const
{
	std::vector<std::vector<std::pair<stype,bool> > > res(size(),std::vector<std::pair<stype,bool> >(size(),std::pair<stype,bool>(0,false)));
	std::vector<stype> task_map = get_task_map();

	for(unsigned i=0;i<size();++i)
	{
		for(const MCNode * n : sched[i])
		{
			for(std::pair<stype,const MCNode *> p : *n)
			{
				++res[i][task_map[p.first]].first;
				res[i][task_map[p.first]].second = true;
				++res[task_map[p.first]][i].first;
				res[task_map[p.first]][i].second = false;
			}
		}
	}

	return res;
}

