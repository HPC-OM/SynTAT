/*
 * MCSchedule.cpp
 *
 *  Created on: 09.07.2014
 *      Author: marc
 */

#include <Scheduling/MCSchedule.hpp>
#include <map>

MCSchedule::MCSchedule(unsigned cores) {
	node_map = std::vector<unsigned>(cores,0u);

	sched = std::vector<MCSingleSchedule>(cores,MCSingleSchedule());
}

MCSchedule::MCSchedule() {
	node_map.push_back(0);
	sched = std::vector<MCSingleSchedule>(1,MCSingleSchedule());
}

MCSchedule::~MCSchedule() {

}

unsigned MCSchedule::get_num_cores() const
{
	return sched.size();
}

unsigned MCSchedule::get_node_num(unsigned core) const
{
	return node_map[core];
}

unsigned MCSchedule::get_num_tasks() const
{
	unsigned res = 0;
	for(unsigned i=0;i<size();++i) res += sched[i].size();
	return res;
}

unsigned MCSchedule::size() const
{
	return get_num_cores();
}

unsigned MCSchedule::push_back(const MCNode * node)
{
	unsigned res = sched.size();
	sched.push_back(MCSingleSchedule());
	sched[res].push_back(node);
	return res;
}

unsigned MCSchedule::push_back(std::list<const MCNode *> const & in)
{
	unsigned res = sched.size();
	sched.push_back(MCSingleSchedule());
	for(std::list<const MCNode *>::const_iterator it = in.begin();it!=in.end();++it)
	{
		sched[res].push_back(*it);
	}
	return res;
}


void MCSchedule::clear()
{
	sched.clear();
}


/**Don't use
 *
 */
unsigned MCSchedule::get_num_task(unsigned core) const
{
	return task_map[core];
}

MCSingleSchedule MCSchedule::get_schedule(unsigned core_num) const
{
	return sched[core_num];
}

MCSingleSchedule & MCSchedule::operator[](unsigned num)
{
	return sched[num];
}

MCSingleSchedule MCSchedule::operator[](unsigned num) const
{
	return sched[num];
}

MCSchedule::cost_type MCSchedule::get_costs() const
{
	cost_type res = sched[0].get_costs();
	for(unsigned i=1;i<sched.size();++i)
		if(res<sched[i].get_costs()) res = sched[i].get_costs();
	return res;
}

std::vector<unsigned> MCSchedule::get_task_map() const
{
	unsigned n = get_num_tasks();
	std::vector<unsigned> res(n,0);
	for(unsigned i=0;i<sched.size();++i)
	{
		for(const MCNode * n : sched[i])
		{
			res[n->get_id()] = i;
		}
	}

	return res;
}

std::ostream & operator<<(std::ostream & in, MCSchedule const & sched)
{
	/*typedef MCSingleSchedule::sched_type::const_iterator it_type;

	std::vector<it_type> its;
	for(unsigned i=0;i<sched.size();++i)
	{
		its.push_back(sched[i].cbegin());
	}
	bool flag = true;
	for(unsigned i=0;i<its.size();++i)
	{
		in << "core" << i << 	"	";
	}
	in << "\n";
	while(flag)
	{
		flag = false;
		for(unsigned i=0;i<its.size();++i)
		{
			if(its[i] != sched[i].cend())
			{
				in << (*its[i])->get_id() << "	";
				flag = true;
				++(its[i]);
			}
			else in << 	"	";
		}
		in << "\n";

	}

	for(unsigned i=0;i<sched.size();++i)
	{
		in << "Schedule for " << i << " size= " << sched[i].size() << "\n";
		for(it_type it = sched[i].cbegin();it!=sched[i].end();++it)
			in << (*it)->get_id() << "\n";
		in << "\n";
	}*/

	for(unsigned i=0;i<sched.size();++i)
	{
		in << "Schedule for " << i << " size= " << sched[i].size() << "\n";
		for(const MCNode * n : sched[i])
			in << n->get_id() << "\n";
		in << "\n";
	}

	return in;
}

