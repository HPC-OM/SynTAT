/*
 * MPIParameter.cpp
 *
 *  Created on: 12.12.2014
 *      Author: marc
 */

#include <Scheduling/MPIParameter.hpp>
#include <Graph/MCGraph_lib.hpp>

#include <Misc/math_opt_funcs.hpp>

MPIParameter::MPIParameter()
{
	waita = 0; waitb = 0;
	send = 0;
	recv = 0;
	comm = 0;
	latency = 0;
}

MPIParameter::~MPIParameter()
{

}

MPIParameter::MPIParameter(std::map<stype, MCNode::cost_type> const & recv, std::map<stype, MCNode::cost_type> const & send, std::map<stype,MCNode::cost_type> const & comm, std::map<stype,MCNode::cost_type> const & wait)
{
	init(recv,send,comm,wait);
}

MCNode::cost_type MPIParameter::get_wait(stype num_req) const
{
	if(num_req==0)
		return 0;
	return waita * num_req + waitb;
}

MCNode::cost_type MPIParameter::get_recv_costs(stype num_recvs) const
{
	return num_recvs*recv;
}

MCNode::cost_type MPIParameter::get_send_costs(stype num_sends) const
{
	return num_sends*send;
}

MCNode::cost_type MPIParameter::get_in_costs(stype num_recvs) const
{
	return get_recv_costs(num_recvs) + get_wait(num_recvs);
}

MCNode::cost_type MPIParameter::get_out_costs(stype num_sends) const
{
	return get_send_costs(num_sends);
}

MCNode::cost_type MPIParameter::get_latency(stype source_core, stype target_core) const
{
	return this->latency;
}

MCNode::cost_type MPIParameter::get_comm_time(stype source_core, stype target_core) const
{
	return this->comm;
}



void MPIParameter::init(std::map<stype, MCNode::cost_type> const & recv, std::map<stype, MCNode::cost_type> const & send, std::map<stype,MCNode::cost_type> const & comm, std::map<stype,MCNode::cost_type> const & wait)
{
	std::array<double,2> waits = mdkq_lin<stype,MCNode::cost_type>(wait);
	waita = waits[0]; waitb = waits[1];
	this->recv = average_div<stype,MCNode::cost_type>(recv);
	this->send = average_div<stype,MCNode::cost_type>(send);
	this->comm = average_div<stype,MCNode::cost_type>(comm);
	if(this->comm < this->send+waita+waitb)
		this->latency = 0;
	else
		this->latency = this->comm - (this->send+waitb);
}

MPIParameter MPIParameter::default_min()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	recv.insert(std::pair<stype,MCNode::cost_type>(1,122));
	recv.insert(std::pair<stype,MCNode::cost_type>(2,212));
	/*recv.insert(std::pair<stype,MCNode::cost_type>(3,305));
	recv.insert(std::pair<stype,MCNode::cost_type>(4,398));
	recv.insert(std::pair<stype,MCNode::cost_type>(5,493));*/

	send.insert(std::pair<stype,MCNode::cost_type>(1,227));
	send.insert(std::pair<stype,MCNode::cost_type>(2,432));
	/*send.insert(std::pair<stype,MCNode::cost_type>(3,667));
	send.insert(std::pair<stype,MCNode::cost_type>(4,867));
	send.insert(std::pair<stype,MCNode::cost_type>(5,1140));*/

	wait.insert(std::pair<stype,MCNode::cost_type>(1,72));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,99));
	/*wait.insert(std::pair<stype,MCNode::cost_type>(3,125));
	wait.insert(std::pair<stype,MCNode::cost_type>(4,154));
	wait.insert(std::pair<stype,MCNode::cost_type>(5,180));*/


	comm.insert(std::pair<stype,MCNode::cost_type>(1,270));
	comm.insert(std::pair<stype,MCNode::cost_type>(2,499));
	/*comm.insert(std::pair<stype,MCNode::cost_type>(3,734));
	comm.insert(std::pair<stype,MCNode::cost_type>(4,975));
	comm.insert(std::pair<stype,MCNode::cost_type>(5,1218));*/


	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::distributed()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	recv.insert(std::pair<stype,MCNode::cost_type>(1,137));
	recv.insert(std::pair<stype,MCNode::cost_type>(2,274));


	send.insert(std::pair<stype,MCNode::cost_type>(1,451));
	send.insert(std::pair<stype,MCNode::cost_type>(2,902));


	wait.insert(std::pair<stype,MCNode::cost_type>(1,81));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,112));
	wait.insert(std::pair<stype,MCNode::cost_type>(3,142));
	wait.insert(std::pair<stype,MCNode::cost_type>(4,170));
	wait.insert(std::pair<stype,MCNode::cost_type>(5,203));


	comm.insert(std::pair<stype,MCNode::cost_type>(1,3328));
	comm.insert(std::pair<stype,MCNode::cost_type>(2,6656));

	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::shared()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	recv.insert(std::pair<stype,MCNode::cost_type>(1,136));
	recv.insert(std::pair<stype,MCNode::cost_type>(2,272));


	send.insert(std::pair<stype,MCNode::cost_type>(1,286));
	send.insert(std::pair<stype,MCNode::cost_type>(2,561));


	wait.insert(std::pair<stype,MCNode::cost_type>(1,86));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,172));


	comm.insert(std::pair<stype,MCNode::cost_type>(1,859));
	comm.insert(std::pair<stype,MCNode::cost_type>(2,1717));

	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::only_latency()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	recv.insert(std::pair<stype,MCNode::cost_type>(1,0));
	recv.insert(std::pair<stype,MCNode::cost_type>(2,0));


	send.insert(std::pair<stype,MCNode::cost_type>(1,0));
	send.insert(std::pair<stype,MCNode::cost_type>(2,0));


	wait.insert(std::pair<stype,MCNode::cost_type>(1,0));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,0));


	comm.insert(std::pair<stype,MCNode::cost_type>(1,150));
	comm.insert(std::pair<stype,MCNode::cost_type>(2,300));

	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::pthreads()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	//recv.insert(std::pair<stype,MCNode::cost_type>(1,11));
	recv.insert(std::pair<stype,MCNode::cost_type>(1,200));


	send.insert(std::pair<stype,MCNode::cost_type>(1,200)); //mal 2


	wait.insert(std::pair<stype,MCNode::cost_type>(1,0));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,0));


	comm.insert(std::pair<stype,MCNode::cost_type>(1,50));

	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::pthreads_man()
{
	std::map<stype,MCNode::cost_type> recv, send, comm, wait;
	//recv.insert(std::pair<stype,MCNode::cost_type>(1,11));
	recv.insert(std::pair<stype,MCNode::cost_type>(1,100));
	recv.insert(std::pair<stype,MCNode::cost_type>(2,150));


	send.insert(std::pair<stype,MCNode::cost_type>(1,90)); // mal 2
	send.insert(std::pair<stype,MCNode::cost_type>(2,150)); //mal 2


	wait.insert(std::pair<stype,MCNode::cost_type>(1,0));
	wait.insert(std::pair<stype,MCNode::cost_type>(2,0));


	comm.insert(std::pair<stype,MCNode::cost_type>(1,7000));
	comm.insert(std::pair<stype,MCNode::cost_type>(2,7000));

	MPIParameter res(recv, send, comm, wait);
	return res;
}

MPIParameter MPIParameter::default_skalar(double in)
{
	MPIParameter res = MPIParameter::distributed();

	res.comm *= in;
	res.latency *= in;
	res.recv *= in;
	res.send *= in;
	res.waita *= in;
	res.waitb *= in;
	return res;
}

SingleLinkGraph MPIParameter::apply_costs(const MCGraph * g) const
{
	SingleLinkGraph res(g);
	std::vector<stype> deps = find_dependencies(g);
	stype i=0;
	if(res[0]->get_costs()==0)
		++i;
	for(;i<res.size();++i)
	{
		res[i]->set_costs(res[i]->get_costs() + get_in_costs(deps[i]) + get_out_costs(res[i]->size()));
		for(auto p : *(res[i]))
		{
			res[i]->set_comm_costs(p.first,get_latency());
		}
	}
	return res;
}

