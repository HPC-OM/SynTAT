/*
 * Schedule.cpp
 *
 *  Created on: 09.01.2015
 *      Author: marc
 */

#include <Scheduling/Schedule.hpp>
#include <Graph/MCGraph_lib.hpp>

Schedule::Schedule(const MCGraph *g, MPIParameter const & sp, stype size) : g(g), tg(g->travers()), sg(sp.apply_costs(g)),sp(&sp) {

	flush();
	node_start = std::vector<MCNode::cost_type>(g->size(),std::numeric_limits<MCNode::cost_type>::max());
	node_deps = find_dependencies(g);
	task_map = std::vector<stype>(g->size(),std::numeric_limits<stype>::max());
	free_its = std::vector<std::list<const MCNode *>::iterator >(g->size(),free_nodes.end());
	sched = std::vector<std::list<const MCNode *> >(size,std::list<const MCNode *>());
	slots = std::vector<std::set<Slot> >(size,std::set<Slot>());
	act_recv = node_deps;
	act_send = std::vector<stype>(g->size(),0);
	for(unsigned i=0;i<g->size();++i)
		if(g->at(i)->get_costs() != 0)
			act_send[i] = g->at(i)->size();

	for(stype i=0;i<size;++i)
	{
		slots[i].insert(slots[i].end(),{0,std::numeric_limits<MCNode::cost_type>::max(),i,sched[i].end(),sched[i].end()});
	}

	if(g->get_first()->get_costs()==0)
	{
		sched[0].push_back(g->get_first());
		task_map[0] = 0;
		node_start[0] = 0;

		for(auto p : *(sg.get_first()))
		{
			free_its[p.first] = free_nodes.insert(free_nodes.end(),g->at(p.first));
			node_deps[p.first] = 0;
			act_send[p.first] = 0;
		}
	}
	current_node_start = std::vector<MCNode::cost_type>(sched.size(),0);
}

Schedule::~Schedule() {
	delete tg;
}

void Schedule::schedule(const MCNode * n, Slot const & s, stype core)
{
	Slot r;
	if(task_map[n->get_id()]!=std::numeric_limits<MCNode::cost_type>::max())
		return;
	if(n->get_id() != current_node->get_id() || current_open_slots[core]==slots[0].end())
	{
		flush();
		r = earliest_slot(n,core);
	}
	else
	{
		r = s;
	}
	//find place to insert
	std::list<const MCNode *>::iterator place = sched[core].insert(r.succ,g->at(n->get_id()));
	task_map[n->get_id()] = core;
	node_start[n->get_id()] = current_node_start[core];

	new_free_nodes.clear();
	free_nodes.erase(free_its[n->get_id()]); //TODO error
	free_its[n->get_id()] = free_nodes.end();

	for(auto p : *n)
	{
		--node_deps[p.first];
		if(node_deps[p.first]==0)
		{
			free_its[p.first] = free_nodes.insert(free_nodes.end(),g->at(p.first));
			new_free_nodes.push_back(g->at(p.first));
		}
	}

	//update costs
	for(auto p : *(tg->at(n->get_id())))
	{
		if(task_map[p.first] == core)
		{
			MCNode::cost_type temp = sg.at(p.first)->get_costs()-sp->get_out_costs(act_send[p.first]);
			--act_send[p.first];
			sg.at(p.first)->set_costs(temp + sp->get_out_costs(act_send[p.first]));

			temp = sg.at(n->get_id())->get_costs() -sp->get_in_costs(act_recv[n->get_id()]);
			--act_recv[n->get_id()];
			sg.at(n->get_id())->set_costs(temp + sp->get_in_costs(act_recv[n->get_id()]));
		}
	}

	//update slot insert after element
	std::set<Slot>::iterator slot_after = slots[core].erase(current_open_slots[core]);
	// is there a slot left before the new node?
	//std::cout << "slot start: " << r.start << " node start: " << current_node_start[core] << "\n";
	//std::cout << "slots[" << core << "].size = " << slots[core].size() << "\n";
	if((long long int)current_node_start[core]-(long long int)r.start>7ll)
	{
		Slot s_new = {r.start,current_node_start[core],core,r.prev,place};
		std::cout << "before slot: " << s_new.start << "," << s_new.end << "," << s_new.core << "\n";
		slots[core].insert(slot_after,s_new);
	}
	// update slot after
	if((long long int)r.end - current_node_start[core]+sg.at(n->get_id())->get_costs() > 7ll)
	{
		Slot s_new = {std::max(current_node_start[core],r.start)+sg.at(n->get_id())->get_costs(),r.end,core,place,r.succ};
		slots[core].insert(slot_after,s_new);
	}

	flush();
}

Slot Schedule::earliest_slot(const MCNode * n, stype core)
{
	if(current_node == NULL || current_node->get_id() != n->get_id())
	{
		flush();
		current_node = n;
	}
	else if(current_open_slots[core] != slots[0].end())
		return *current_open_slots[core];


	MCNode::cost_type max_t = earliest_start_time_on(n,core);
	Slot res = {max_t,max_t+g->at(n->get_id())->get_costs(),core};

	std::set<Slot>::iterator it = slots[core].upper_bound(res);

	while((long long int)it->end-it->start < (long long int)res.end-res.start)
	{
		++it;
	}

	if(it == slots[core].end())
	{
		std::cout << "No slot failure\n";
		exit(1);
	}

	current_open_slots[core] = it;
	return *it;
}

Slot Schedule::earliest_time(const MCNode * n, stype core)
{
	if(current_node == NULL || current_node->get_id() != n->get_id())
	{
		flush();
		current_node = n;
	}
	else if(current_open_slots[core] != slots[0].end())
		return *current_open_slots[core];

	earliest_start_time_on(n,core);
	current_open_slots[core] = --(slots[core].end());

	return *(current_open_slots[core]);
}

stype Schedule::earliest_core(const MCNode * n)
{
	flush();
	MCNode::cost_type e_temp, early = std::numeric_limits<MCNode::cost_type>::max();
	stype best = 0;
	for(stype i=0;i<sched.size();++i)
	{
		e_temp = earliest_start_time_on(n, i);
		if(e_temp < early)
		{
			early = e_temp;
			best = i;
		}
	}

	return best;
}

stype Schedule::size() const
{
	return sched.size();
}


std::list<const MCNode *> Schedule::free() const
{
	return free_nodes;
}

std::list<const MCNode *> & Schedule::free()
{
	return free_nodes;
}

std::list<const MCNode *> Schedule::new_free() const
{
	return new_free_nodes;
}

void Schedule::flush()
{
	current_open_slots = std::vector< std::set<Slot>::iterator >(sched.size(),slots[0].end());
	current_node = NULL;
	current_node_start = std::vector<MCNode::cost_type>(sched.size(),0);
}

MCNode::cost_type Schedule::makespan() const
{
	MCNode::cost_type res=0, temp;
	for(stype i=0;i<sched.size();++i)
	{
		temp = makespan(i);
		if(temp>res)
			res = temp;
	}
	return res;
}

MCNode::cost_type Schedule::makespan(stype core) const
{
	return slots[core].rbegin()->start;
}

MCNode::cost_type Schedule::earliest_start_time_on(const MCNode * n, stype core)
{
	MCNode::cost_type temp,max_t = 0;
	for(auto p : *(tg->at(n->get_id())))
	{
		temp = node_start[p.first]+sg.at(p.first)->get_costs();
		if(task_map[p.first]!=core)
			temp += sg.at(p.first)->get_comm_costs(n->get_id()) + sp->get_in_costs(act_recv[n->get_id()]);
		else
			temp -= sp->get_out_costs(act_send[p.first]) - sp->get_out_costs(act_send[p.first]-1)  - sp->get_in_costs(act_recv[n->get_id()]-1);
		if(max_t > temp)
			max_t = temp;
	}
	temp = makespan(core);
	if(temp > max_t)
		max_t = temp;
	current_node_start[core] = max_t;
	return max_t;
}


UnorderedSchedule Schedule::get_unordered_sched() const
{
	UnorderedSchedule res;
	for(stype i = 0; i<sched.size();++i)
		res.push_back(sched[i]);
	return res;
}
