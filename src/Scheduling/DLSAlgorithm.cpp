/*
 * DLSAlgorithm.cpp
 *
 *  Created on: 16.09.2014
 *      Author: marc
 */

#include <Scheduling/DLSAlgorithm.hpp>

#include <unordered_map>

DLSAlgorithm::DLSAlgorithm(const MCGraph * in, MPIParameter const & sp) : MCScheduler(in,sp) {

}

DLSAlgorithm::~DLSAlgorithm() {
}


UnorderedSchedule DLSAlgorithm::get_schedule_linear(unsigned num_cores)
{
	SingleLinkGraph sg(sp->apply_costs(g));
	/*MCGraph * tg = sg.travers();
	std::vector<stype> cluster_of(g->size(),std::numeric_limits<stype>::max()), c_conns;
	std::vector<MCNode::cost_type> c_costs, costs_behind_task(g->size(),0);
	std::list<const MCNode *> task_queue(1,g->get_first());

	//need in while
	const MCNode * n;
	while(!task_queue.empty())
	{
		n = task_queue.front();
		//update queue
		for(auto p : *n)
		{
			task_queue.push_back(p.second);
		}
		task_queue.pop_front();
	}


*///TODO
	Schedule sched(g,*sp,num_cores);
	return sched.get_unordered_sched();
}


UnorderedSchedule DLSAlgorithm::get_schedule(unsigned num_cores, unsigned * nodes)
{
	MCGraphAnalyser ga(g,sp);
	std::vector<AnalysedNode> vs = ga.get_analysed_nodes(AnalysedNode::BLVL);
	Schedule sched(g,*sp,num_cores);
	std::list<const MCNode *> free_l = sched.free();

	long long int temp, max = 0;
	stype core = 0;
	const MCNode * best = NULL;

	while(!free_l.empty())
	{
		best = NULL;
		max = std::numeric_limits<long long int>::min();
		core = 0;
		for(const MCNode * n : free_l)
		{
			for(stype i=0;i<num_cores;++i)
			{

				temp = (long long int)vs[n->get_id()].get_blvl() - sched.earliest_start_time_on(n,i);
				if(temp > max)
				{
					max = temp;
					core = i;
					best = n;
				}
			}
		}
		if(best == NULL)
		{
			std::cout << "DLC says: No valid core found\n";
			exit(1);
		}
		sched.schedule(best,sched.earliest_time(best,core),core);
		free_l = sched.free();
	}

	return sched.get_unordered_sched();
}

UnorderedSchedule DLSAlgorithm::get_schedule_unbound()
{
	std::cout << "DLSScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule DLSAlgorithm::get_schedule_distributed()
{
	std::cout << "DLSScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}
