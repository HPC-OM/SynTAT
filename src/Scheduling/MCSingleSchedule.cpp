/*
 * MCSingleSchedule.cpp
 *
 *  Created on: 24.07.2014
 *      Author: marc
 */

#include <Scheduling/MCSingleSchedule.hpp>

MCSingleSchedule::MCSingleSchedule() {
	current_costs = 0;
	sched = std::list<const MCNode *>();
}

MCSingleSchedule::MCSingleSchedule(sched_type const & in) {
	current_costs = 0;
	for(auto it = in.begin();it!=in.end();++it)
	{
		push_back(*it);
	}
}

MCSingleSchedule::~MCSingleSchedule() {

}

MCSingleSchedule::sched_type::iterator MCSingleSchedule::begin()
{
	return sched.begin();
}

MCSingleSchedule::sched_type::iterator MCSingleSchedule::MCSingleSchedule::end()
{
	return sched.end();
}

MCSingleSchedule::sched_type::const_iterator MCSingleSchedule::begin() const
{
	return sched.begin();
}

MCSingleSchedule::sched_type::const_iterator MCSingleSchedule::end() const
{
	return sched.end();
}

MCSingleSchedule::sched_type::iterator MCSingleSchedule::insert(MCSingleSchedule::sched_type::iterator it, const MCNode * node)
{
	sched_type::iterator res = sched.insert(it,node);
	current_costs += node->get_costs();
	return res;
}

void MCSingleSchedule::insert(std::list<const MCNode *> const & in)
{
		for(std::list<const MCNode *>::const_iterator it = in.begin();it!=in.end();++it)
		{
			push_back(*it);
		}
}

void MCSingleSchedule::push_back(const MCNode * in)
{
	current_costs += in->get_costs();
	sched.push_back(in);
}

void MCSingleSchedule::push_front(const MCNode * in)
{
	current_costs += in->get_costs();
	sched.push_front(in);
}

const MCNode * MCSingleSchedule::back() const
{
	return sched.back();
}

const MCNode * MCSingleSchedule::front() const
{
	return sched.front();
}

MCSingleSchedule::size_type MCSingleSchedule::size() const
{
	return sched.size();
}

bool MCSingleSchedule::empty() const
{
	return sched.empty();
}

void MCSingleSchedule::clear()
{
	sched.clear();
	current_costs = 0;
}

MCSingleSchedule::cost_type MCSingleSchedule::get_costs() const
{
	return current_costs;
}


std::vector<unsigned> MCSingleSchedule::get_ids() const
{
	unsigned index = 0;
	std::vector<unsigned> res(sched.size(),index);
	for(sched_type::const_iterator it=sched.begin();it!=sched.end();++it)
	{
		 res[index++] = (*it)->get_id();
	}
	return res;
}

std::ostream & operator<<(std::ostream & in, MCSingleSchedule const & sched)
{
	for(MCSingleSchedule::sched_type::const_iterator it = sched.sched.begin();it!=sched.sched.end();++it)
	{
		in << (*it)->get_id() << " ";
	}
	return in << "\n";
}
