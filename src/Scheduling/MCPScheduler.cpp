/*
 * MCPScheduler.cpp
 *
 *  Created on: 08.08.2014
 *      Author: marc
 */

#include <Scheduling/MCPScheduler.hpp>
#include <Graph/AnalysedNode.hpp>
#include <Graph/MCGraph_lib.hpp>
#include <Scheduling/Schedule.hpp>
#include <set>
#include <map>
#include <limits>
#include <algorithm>


MCPScheduler::MCPScheduler(MCGraph const * in, MPIParameter const & sp) : MCScheduler(in,sp) {

}

MCPScheduler::~MCPScheduler() {

}


UnorderedSchedule MCPScheduler::get_schedule(stype num_cores, stype * nodes)
{
	struct Prio
	{
		std::vector<MCNode::cost_type> alap;

		bool operator()(const MCNode * l, const MCNode * r) const
		{
			return alap[l->get_id()] < alap[r->get_id()];
		}
	};

	Prio p;
	p.alap = get_blvls(g,true);
	MCNode::cost_type cp = get_crit_costs(g,true);
	for(stype i=0;i<p.alap.size();++i)
	{
		p.alap[i] = cp - p.alap[i];
	}

	std::multiset<const MCNode *, Prio> ins(p);
	for(stype i=0;i<g->size();++i)
		ins.insert(g->at(i));

	Schedule sched(g,*sp,num_cores);
	stype bestcore;
	for(auto n : ins)
	{
		bestcore = sched.earliest_core(n);
		sched.schedule(n,sched.earliest_slot(n,bestcore),bestcore);
	}
	return sched.get_unordered_sched();
}

UnorderedSchedule MCPScheduler::get_schedule_unbound()
{

	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule MCPScheduler::get_schedule_distributed()
{
	std::cout << "LCScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule MCPScheduler::sub_schedule(UnorderedSchedule const & in,MCGraphAnalyser & ga) const
{
	struct cmp
	{
		bool operator()(const SchedStream * lhs, const SchedStream * rhs) const
		{
			return lhs->id < rhs->id;
		}

	};
	UnorderedSchedule res(in.size());
	std::vector<AnalysedNode> anodes = ga.get_analysed_nodes(AnalysedNode::ALAP);
	for(stype i=0;i<in.size();++i)
	{
		std::set<AnalysedNode,AnalysedNode::alap_comp_struct> sub;
		for(const MCNode * n : in[i])
		{
			sub.insert(anodes[n->get_id()]);
		}
		for(const AnalysedNode & a : sub)
		{
			res[i].push_back(a.get_node());
		}
	}



	return res;
}
