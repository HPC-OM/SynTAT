/*
 * ClusterScheduler.cpp
 *
 *  Created on: 21.11.2014
 *      Author: marc
 */

#include <Scheduling/ClusterScheduler.hpp>
#include <Graph/MCGraph_lib.hpp>
#include <algorithm>
#include <IO/GraphMLReader.hpp>

ClusterScheduler::ClusterScheduler(MCGraph const * in, MPIParameter const & sp) : MCScheduler(in,sp){
	maping = LOAD_BALANCE;
	order = SEND;
}

ClusterScheduler::~ClusterScheduler() {
}


void ClusterScheduler::set_mapping(ClusterScheduler::Mapping m)
{
	maping = m;
}

UnorderedSchedule ClusterScheduler::get_schedule(unsigned num_cores, unsigned * nodes)
{
	UnorderedSchedule temp = get_cluster(), temp2;
	/*if(temp.check())
		std::cout << "ClusterScheduler: sched is valid\n";
	else
	{
		std::cout << "ClusterScheduler: no valid sched produced\n";
	}*/
	switch(maping)
	{
	case LOAD_BALANCE:
		temp2 = get_load_balance(num_cores,temp);
		break;
	case COMM_MIN:
		temp2 = get_comm_min(num_cores,temp);
		break;
	case ICS_EXT:
		temp2 = get_extend_ics(num_cores,temp);
		break;
	default:
		temp2 = get_load_balance(num_cores,temp);
	}
	/*if(temp2.check())
		std::cout << "ClusterScheduler: sched is valid (2)\n";
	else
	{
		std::cout << "ClusterScheduler: no valid sched produced (2)\n";
	}*/
	switch(order)
	{
	case MCP:
		return get_mcp_order(temp2);
	case SEND:
		return get_send_first_order(temp2);
	default:
		return get_mcp_order(temp2);
	}
}

UnorderedSchedule ClusterScheduler::get_schedule_distributed()
{
	std::cout << "ClusterScheduler::get_schedule_distributed() is not implemented\n";
	return get_default_schedule();
}

UnorderedSchedule ClusterScheduler::get_schedule_unbound()
{

	return get_cluster();
}

void ClusterScheduler::set_load_mapping(ClusterScheduler::Mapping s)
{
	maping = s;
}

bool comp_order_pair(std::pair<stype,MCNode::cost_type> const & lhs, std::pair<stype,MCNode::cost_type> const & rhs)
{
	return lhs.second > rhs.second;
}

UnorderedSchedule ClusterScheduler::get_load_balance(stype size,UnorderedSchedule & in)
{
	UnorderedSchedule res(size);
	std::vector<stype> task_map = in.get_task_map();
	std::list<std::pair<stype,MCNode::cost_type> > orders;
	for(unsigned i=0;i<in.size();++i)
		orders.push_back(std::pair<stype,MCNode::cost_type>(i,in[i].get_costs()));

	orders.sort(comp_order_pair);
	std::multimap<MCNode::cost_type,std::list<stype> > maps;
	for(unsigned i=0;i<size;++i)
		maps.insert(std::pair<MCNode::cost_type,std::list<stype> >(0,std::list<stype>()));
	std::pair<MCNode::cost_type,std::list<stype> > temp;
	for(std::pair<stype,MCNode::cost_type> & p : orders)
	{
		temp = *(maps.begin());
		maps.erase(maps.begin());
		temp.first += p.second;
		temp.second.push_back(p.first);
		maps.insert(temp);
	}
	unsigned i=0;
	for(auto p : maps)
	{
		for(auto n : p.second)
		{
			res[i].insert(in[n]);
		}
		++i;
	}

	return res;
}

UnorderedSchedule ClusterScheduler::get_cluster_connect(stype size,UnorderedSchedule & in)
{
	UnorderedSchedule res(size);
	std::vector<stype>  task_map = in.get_task_map();
	std::vector<std::map<stype,stype> > sends(in.size(),std::map<stype,stype>()),recvs(in.size(),std::map<stype,stype>());
	for(unsigned i = 0;i<in.size();++i)
	{
		for(auto n : in[i])
		{
			for(auto p : *n)
			{
				if(task_map[p.first] != i)
				{
					++sends[i][task_map[p.first]];
					++recvs[task_map[p.first]][i];
				}
			}
		}
	}
	std::list<std::pair<stype,MCNode::cost_type> > orders;
	for(unsigned i=0;i<in.size();++i)
		orders.push_back(std::pair<stype,MCNode::cost_type>(i,in[i].get_costs()));
	orders.sort(comp_order_pair);

	//Phase 1: Überprüfe Cluster nach zu hohen Comm-Costs
	for(auto it = orders.rbegin();it!=orders.rend();++it)
	{
		for(auto p : sends[it->first])
		{
			if(sp->get_comm_time()*sends[it->first][p.first] > res[it->first].get_costs()+res[p.first].get_costs())
			{
					//TODO
			}
		}
	}
	//Phase 2: Merge Cluster mit wenigsten zuwachs


	return res;
}


UnorderedSchedule ClusterScheduler::get_comm_merge(stype size,UnorderedSchedule & in)
{
	//TODO so umbalsteln, dass immer Cluster gemerged werden, wodurch sich die Kommunikation verringert.
	UnorderedSchedule res;
	std::vector<stype>  task_map = in.get_task_map();
	std::vector<std::vector<MCNode::cost_type> > cons = std::vector<std::vector<MCNode::cost_type> >(in.size(),std::vector<MCNode::cost_type>(in.size(),0));
	std::vector<MCNode::cost_type> recv_costs(in.size(),0),send_costs(in.size(),0);
	std::vector<bool> clustered(in.size(),false);
	std::vector<std::set<stype> > recvs_from(in.size(),std::set<stype>()), sends_to(in.size(),std::set<stype>());
	stype c1, c2;
	for(stype i=0;i<in.size();++i)
		for(const MCNode * n : in[i])
		{
			for(auto p : *n)
			{
				if(n->get_costs()!=0 && p.second->get_costs() != 0)
				{
					c1 = task_map[n->get_id()];
					c2 = task_map[p.first];
					if(c1 != c2) {
						recvs_from[c2].insert(c1);
						sends_to[c1].insert(c2);
					}
				}
			}
		}

	std::list<const MCNode *> temp;
	std::list<stype> added;
	MCNode::cost_type t_costs, max_costs = in.get_costs()/size + 1;
	stype i = 0;
	bool flag = false;
	while(i<in.size())
	{
		if(flag)
			i = 0;
		flag = false;
		if(!clustered[i])
		{
			added.clear();
			added.push_back(i);
			temp = in[i];
			t_costs = in[i].get_costs();
			//is idc
			std::set<stype>::iterator c = sends_to[i].begin();
			while(c!=sends_to[i].end())
			{
				if(recvs_from[*c].size() == 1 && t_costs < max_costs)
				{
					//merge
					flag = true;
					clustered[*c] = true;
					temp.insert(temp.end(),in[*c].begin(),in[*c].end());
					t_costs += in[*c].get_costs();
					stype buffer = *c;
					sends_to[i].erase(*c);
					sends_to[i].insert(sends_to[buffer].begin(),sends_to[buffer].end());
					if(sends_to[buffer].size()>0)
						added.push_back(buffer);
					sends_to[buffer].clear();
					recvs_from[buffer].clear();
					c = sends_to[i].begin();
				}
				else
				{
					++c;
				}
			}

			if(flag)
			{
				clustered[i] = true;
				res.push_back(temp);

				for(auto c : sends_to[i])
					for(auto old : added)
					{
						recvs_from[c].erase(old);
					}
			}
		}
		if(!flag)
			++i;
	}

	for(stype i=0;i<in.size();++i)
		if(!clustered[i])
			res.push_back(in[i]);
	//std::cout << "ics reduced " << in.size() << " cluster to " << res.size() << " clusters\n";
	return get_load_balance(size,res);
}

UnorderedSchedule ClusterScheduler::get_comm_min(stype size,UnorderedSchedule & in)
{
	if(in.size()==size)
		return in;
	std::vector<stype>  task_map = in.get_task_map();
	std::vector<std::vector<MCNode::cost_type> > cons = std::vector<std::vector<MCNode::cost_type> >(in.size(),std::vector<MCNode::cost_type>(in.size(),0));
	stype c1,c2;
	for(stype i=0;i<in.size();++i)
		for(const MCNode * n : in[i])
		{
			for(auto p : *n)
			{
				c1 = task_map[n->get_id()];
				c2 = task_map[p.first];
				if(c1<c2)
				{
					cons[c1][c2] += sp->get_send_costs(1);
					cons[c2][c1] += sp->get_recv_costs(1);
				}
				else
				{
					cons[c2][c1] += sp->get_send_costs(1);
					cons[c1][c2] += sp->get_recv_costs(1);
				}
			}
		}
	std::multimap<long long int,std::pair<stype,stype> > order;
	for(stype i=0;i<cons.size();++i)
	{
		for(stype j=i+1;j<cons.size();++j)
		{
			if(cons[i][j]+cons[j][i] != 0)
				order.insert(std::pair<long long int,std::pair<stype,stype> >((cons[i][j]+cons[j][i]),std::pair<stype,stype>(i,j)));
		}
	}

	std::multimap<stype,stype> merge;
	long long int num_cluster = in.size();

	bool flag = true;
	auto it = order.rbegin();
	while(num_cluster>size && flag)
	{
		if(it->first == 0)
		{
			flag = false;
			break;
		}
		--num_cluster;
		merge.insert(std::pair<stype,stype>(it->second.first,it->second.second));
	}
	std::vector<stype> merged = std::vector<stype>(in.size(),std::numeric_limits<stype>::max());
	std::vector<stype> cluster(in.size(),std::numeric_limits<stype>::max());
	stype c_count = 0;
	for(auto p : merge)
	{
		if(cluster[p.first] == std::numeric_limits<stype>::max())
		{
			c1 = p.first;
			cluster[p.first] = c_count++;
			merged[p.first] = p.first;
		}
		else
		{
			c1 = merged[p.first];
		}
		merged[p.second] = c1;
		cluster[p.second] = cluster[c1];
	}

	UnorderedSchedule res(c_count);
	std::list<stype> not_assigned;
	for(stype i=0;i<merged.size();++i)
	{
		if(merged[i]!=std::numeric_limits<stype>::max())
		{
			res[cluster[i]].insert(in[i]);
		}
		else
			not_assigned.push_back(i);
	}

	stype place = 0;
	std::list<stype>::iterator c = not_assigned.begin();
	for(;c!=not_assigned.end();++c)
	{
		while(place < size && !res[place].empty())
			++place;
		if(place >= res.size())
		{
			break;
		}
		res[place].insert(in[*c]);
		++place;
	}
	for(;c!=not_assigned.end();++c)
		res.push_back(in[*c]);
	if(res.size()>size)
		return get_load_balance(size,res);
	else
		return res;

}



UnorderedSchedule ClusterScheduler::get_extend_ics(stype size,UnorderedSchedule & in)
{
	//TODO buggy
	UnorderedSchedule res;
	std::vector<stype>  task_map = in.get_task_map();
	std::vector<std::vector<MCNode::cost_type> > cons = std::vector<std::vector<MCNode::cost_type> >(in.size(),std::vector<MCNode::cost_type>(in.size(),0));
	std::vector<MCNode::cost_type> recv_costs(in.size(),0),send_costs(in.size(),0);
	std::vector<bool> clustered(in.size(),false);
	std::vector<std::set<stype> > recvs_from(in.size(),std::set<stype>()), sends_to(in.size(),std::set<stype>());
	stype c1, c2;
	for(stype i=0;i<in.size();++i)
		for(const MCNode * n : in[i])
		{
			for(auto p : *n)
			{
				if(n->get_costs()!=0 && p.second->get_costs() != 0)
				{
					c1 = task_map[n->get_id()];
					c2 = task_map[p.first];
					if(c1 != c2) {
						recvs_from[c2].insert(c1);
						sends_to[c1].insert(c2);
					}
				}
			}
		}

	std::list<const MCNode *> temp;
	std::list<stype> added;
	MCNode::cost_type t_costs, max_costs = in.get_costs()/size + 1;
	stype i = 0;
	bool flag = false;
	while(i<in.size())
	{
		if(flag)
			i = 0;
		flag = false;
		if(!clustered[i])
		{
			added.clear();
			added.push_back(i);
			temp = in[i];
			t_costs = in[i].get_costs();
			//is idc
			std::set<stype>::iterator c = sends_to[i].begin();
			while(c!=sends_to[i].end())
			{
				if(recvs_from[*c].size() == 1 && t_costs < max_costs)
				{
					//merge
					flag = true;
					clustered[*c] = true;
					temp.insert(temp.end(),in[*c].begin(),in[*c].end());
					t_costs += in[*c].get_costs();
					stype buffer = *c;
					sends_to[i].erase(*c);
					sends_to[i].insert(sends_to[buffer].begin(),sends_to[buffer].end());
					if(sends_to[buffer].size()>0)
						added.push_back(buffer);
					sends_to[buffer].clear();
					recvs_from[buffer].clear();
					c = sends_to[i].begin();
				}
				else
				{
					++c;
				}
			}

			if(flag)
			{
				clustered[i] = true;
				res.push_back(temp);

				for(auto c : sends_to[i])
					for(auto old : added)
					{
						recvs_from[c].erase(old);
					}
			}
		}
		if(!flag)
			++i;
	}

	for(stype i=0;i<in.size();++i)
		if(!clustered[i])
			res.push_back(in[i]);
	//std::cout << "ics reduced " << in.size() << " cluster to " << res.size() << " clusters\n";
	return this->get_comm_min(size,res);
}


UnorderedSchedule ClusterScheduler::get_mcp_order(UnorderedSchedule const & in)
{
	struct Prio
	{
		std::vector<MCNode::cost_type> alap;

		bool operator()(const MCNode * lhs, const MCNode * rhs) const
		{
			return alap[lhs->get_id()] < alap[rhs->get_id()];
		}
	};

	SingleLinkGraph sg(g);

	std::vector<stype> recvs(sg.size()), tmap = in.get_task_map();

	for(stype i=0;i<in.size();++i)
	{
		for(const MCNode * n : in[i])
		{
			stype count_sends = 0;
			for(auto p : *n)
			{
				MCNode::cost_type temp = 0;
				if(tmap[n->get_id()] != tmap[p.first])
				{
					temp = sp->get_latency();
					++count_sends;
				}
				sg.at(n->get_id())->set_comm_costs(p.first,temp);
			}
			sg.at(n->get_id())->set_costs(n->get_costs()+sp->get_send_costs(count_sends)+sp->get_recv_costs(recvs[n->get_id()]));
		}
	}
	UnorderedSchedule res;
	std::list<const MCNode *> temp;
	Prio p;
	p.alap = calculate_alap(&sg);


	for(unsigned i=0;i<in.size();++i)
	{
		temp.clear();
		temp = in[i];
		temp.sort(p);
		res.push_back(temp);
	}
	return res;
}


UnorderedSchedule ClusterScheduler::get_send_first_order(UnorderedSchedule const & in)
{
	UnorderedSchedule res;

	struct Prio
	{
		std::vector<stype> lvls;
		std::vector<stype> sends;
		std::vector<stype> recvs;

		bool operator()(stype l, stype r) const
		{
			return lvls[l]<lvls[r]
			       || (lvls[l]==lvls[r] && (sends[l]>sends[r]
			       || (sends[l]==sends[r] && recvs[l]<recvs[r])));
		}
	};

	Prio pr;
	pr.lvls = get_lvls(g);
	pr.sends = std::vector<stype>(in.get_num_tasks(),0);
	pr.recvs = std::vector<stype>(in.get_num_tasks(),0);
	std::vector<stype> task_map = in.get_task_map();
	for(stype i=0;i<in.size();++i)
	{
		for(auto n : in[i])
		{
			for(auto p: *n)
			{
				if(task_map[p.first]!=task_map[n->get_id()])
				{
					++pr.sends[n->get_id()];
					++pr.recvs[p.first];
				}
			}
		}
	}
	for(stype i=0;i<in.size();++i)
	{
		std::multiset<stype,Prio> ord(pr);
		for(auto n : in[i])
		{
			ord.insert(n->get_id());
		}
		std::list<const MCNode *> temp;
		for(auto n : ord)
		{
			temp.push_back(g->at(n));
		}
		res.push_back(temp);
	}


	return res;
}

UnorderedSchedule ClusterScheduler::get_dyn_send_first_order(UnorderedSchedule const & in)
{
	UnorderedSchedule res;
	struct Prio
	{
		std::vector<stype> lvls;
		std::vector<stype> sends;
		std::vector<stype> recvs;

		bool operator()(stype l, stype r) const
		{
			//receiver immer als letztes
			if(recvs[l] > recvs[r])
				return false;
			else if(recvs[r] > recvs[l])
				return true;
			//sender immer als erstes
			if(sends[l] > sends[r])
				return true;
			else if(sends[r] > sends[l])
				return false;
			// kleines lvl ist wichtiger
			if(lvls[l] > lvls[r])
				return false;
			else
				return true;
		}
	};
	Prio pr;

	pr.lvls = get_lvls(g);
	pr.sends = std::vector<stype>(in.get_num_tasks(),0);
	pr.recvs = std::vector<stype>(in.get_num_tasks(),0);
	std::vector<stype> task_map = in.get_task_map();
	std::vector<stype> deps(in.get_num_tasks(),0);
	for(stype i=0;i<in.size();++i)
	{
		for(auto n : in[i])
		{
			for(auto p: *n)
			{
				if(task_map[p.first]!= i )
				{
					++pr.sends[n->get_id()];
					++pr.recvs[p.first];
				}
				else
				{
					//std::cout << p.first << " inserted \n";
					++deps[p.first];
				}
			}
		}
	}
	std::list<const MCNode *> temp;
	std::set<stype,Prio> order(pr);
	for(stype i=0;i<in.size();++i)
	{

		temp.clear();
		order.clear();

		for(const MCNode * n : in[i])
		{
			if(deps[n->get_id()]!=0)
				break;

			order.insert(n->get_id());
		}

		while(!order.empty())
		{
			stype add = *order.begin();
			order.erase(order.begin());
			temp.push_back(g->at(add));

			for(auto p : *(g->at(add)))
			{
				if(i == task_map[p.first])
				{
					--deps[p.first];
					if(deps[p.first] == 0)
					{
						order.insert(p.first);

					}
				}
			}
		}
		res.push_back(temp);
	}


	return res;
}

