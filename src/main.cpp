/*
 * main.cpp
 *
 *  Created on: 20.06.2014
 *      Author: marc
 */

#include <test_om_graphs.hpp>
#include <test_heat.hpp>
#include <test_sched_time.hpp>

#include <iostream>

int main(int argc, char* argv[])
{
	std::cout << "start test\n";
	//test_sched_time();

	if (2 != argc)
	    std::cout << "Error: Pass a graphml file to SynTAT. A sample file can be found in 'graphs' folder.\n";
	else
	    test_om(std::string(argv[1]));

	std::cout << "end test\n";

	return 0;
}
