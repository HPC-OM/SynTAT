/*
X * MCTaskSimulation.cpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

#include <Simulation/MCTaskSimulation.hpp>

MCTaskSimulation::MCTaskSimulation(UnorderedSchedule const * in) : sched(in) {
	tc = WAIT;
	init();
}

MCTaskSimulation::~MCTaskSimulation() {

}


void MCTaskSimulation::set_task_type(TaskType tc)
{
	this->tc = tc;
}

const UnorderedSchedule * MCTaskSimulation::get_sched()
{
	return sched;
}

MCTaskSimulation::TaskType MCTaskSimulation::get_task_type()
{
	return tc;
}

std::string MCTaskSimulation::task(const MCNode * n)
{
	std::stringstream res;
	if(measure_task)
		res << "measure_task_start = rdtsc();\n";
	switch(tc)
	{
	case(WAIT):
		res << tsc_wait_task(n);
		break;
	case(VECTOR):
		res << vector_task(n);
		break;
	default:
		break;
	}
	if(measure_task)
	{
		res << "measure_task_end = rdtsc();\n";
		res << "measure_task["<< n->get_id() << "].second += measure_task_end - measure_task_start;\n";
		res << "++measure_task["<< n->get_id() << "].first;\n";
	}

	return res.str();
}

std::string MCTaskSimulation::functions()
{
	std::stringstream res("");

	switch(tc)
	{
	case(WAIT):
		res << get_rdtsc_func_dec();
		break;
	default:
		break;
	}
	return res.str();
}

std::string MCTaskSimulation::get_rdtsc_func_dec()
{
	std::stringstream res("");
	res << "long long unsigned rdtsc() const;\n";
	res << "void waste_time()\n";
	return res.str();
}

std::string MCTaskSimulation::get_rdtsc_func_imp()
{
	std::stringstream res("");
	res << "double time_waster;\nunsigned step;\n\n";
	res << "inline long long unsigned rdtsc() {\n" <<
				"	unsigned long long hi, lo;\n" <<
				"	asm volatile (\"rdtsc\" : \"=a\"(lo), \"=d\"(hi));\n" <<
				"	return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );\n" <<
				"}\n\n";

	res << "void waste_time() {\n";
	res << "time_waster = (2ull + time_waster) + (((double)step++) * 5u);\n";
	res << "}\n\n";
	return res.str();
}

std::string MCTaskSimulation::declarations()
{
	return get_rdtsc_func_imp() + equation_vars();
}

std::string MCTaskSimulation::initializations()
{
	return equation_vars_init();
}

std::string MCTaskSimulation::deinitializations()
{
	std::stringstream res("");

	if(measure_task)
	{
		res << "std::ofstream ous;\n";
		res << "int num;\n";
		res << "unsigned long long sum;\n";
		res << "ous.open(\"" << task_file << ".csv\", std::ofstream::out);\n";
		res << "ous << \" node_id,cycles \\n\";\n";
		res << "for(unsigned i=0;i<measure_task.size();++i) ous << i << \",\" << task_t_array[i] << \"\\n\";\n";
		res << "ous.close();\n";
		res << "delete[] task_t_array;\n\n";
	}
	return equation_vars_destr();
}

void MCTaskSimulation::set_measure_task(bool in,std::string recv_name)
{
	measure_task = in;
	task_file = recv_name;
}


void MCTaskSimulation::init()
{
	this->measure_task = false;
	cycles_per_add = 10.0;
	for(unsigned i=0;i<sched->size();++i)
	{
		for(const MCNode * n : sched->get_schedule(i))
		{
			eq_vars.push_back(std::pair<unsigned,unsigned>(n->get_id(),n->get_costs()));
		}
	}
}

std::string MCTaskSimulation::tsc_wait_task(const MCNode * n)
{
	std::stringstream res("");
	if(n->get_costs() == 0)
		return "\n//Empty Task\n\n";
	res << "// Task " << n->get_id() << "\n";
	if(n->get_costs() >= 40 )
	{
		res << "end_t[" << n->get_id() << "] = " << n->get_costs()-25 << ";\n" <<
			   "end_t[" << n->get_id() << "] += rdtsc();\n" <<
			   "while(rdtsc() < end_t[" << n->get_id() << "]) { }\n";
	}
	else
	{
		if(n->get_costs() >= 30)
		{
			res << "waste_time();\n";
		}
		if(n->get_costs() >= 10)
		{
			res << "waste_time();\n";
		}
		res << "waste_time();\n";
	}

	return res.str();
}

std::string MCTaskSimulation::vector_task(const MCNode * n)
{
	unsigned id = n->get_id();
	std::stringstream res("");
	res << "for(unsigned i=0;i<va_" << id << ".size();++i)\n{\n" <<
		   "	vc_" << id << "[i] = va_" << id << "[i] + vb_" << id << "[i];\n}\n";

	return res.str();
}

std::string MCTaskSimulation::tsc_wait_equation_vars()
{
	std::stringstream res("");
	res << "unsigned long long * end_t;\n";
	return res.str();
}

std::string MCTaskSimulation::vector_equation_vars()
{
	std::stringstream res("");
	for(unsigned i=0;i<eq_vars.size();++i)
	{
		unsigned id = eq_vars[i].first;
		res << "double * va_" << id << ", * vb_" << id << ", * vc;\n";
	}
	return res.str();
}

std::string MCTaskSimulation::tsc_wait_equation_vars_init()
{
	std::stringstream res("");
	res << "end_t = new unsigned long long[" << eq_vars.size()+1 << "];\n";
	return res.str();
}

std::string MCTaskSimulation::tsc_wait_equation_vars_destr()
{
	std::stringstream res("");
	res << "delete[] end_t;\n";
	return res.str();
}

std::string MCTaskSimulation::vector_equation_vars_init()
{
	std::stringstream res("");
	for(unsigned i=0;i<eq_vars.size();++i)
	{
		unsigned id = eq_vars[i].first, cy = eq_vars[i].second;
		res << "va_" << id << " = new double[" << cy << "];\n";
		res << "vb_" << id << " = new double[" << cy << "];\n";
		res << "vc_" << id << " = new double[" << cy << "];\n";
		res << "for(unsigned i=0;i<" << cy << ";++i)\n{\n" <<
			   "	va[i] = (i+1)%100 * 1.94579546;\n" <<
			   "	vb[i] = (i+50)%100 * 1.48740245;\n" <<
			   "	vc[i] = 0.0;\n}\n";
	}
	return res.str();
}

std::string MCTaskSimulation::vector_equation_vars_destr()
{
	std::stringstream res("");
	for(unsigned i=0;i<eq_vars.size();++i)
	{
		unsigned id = eq_vars[i].first;
		res << "delete[] va_" << id << ";\n";
		res << "delete[] vb_" << id << ";\n";
		res << "delete[] vc_" << id << ";\n";
	}
	return res.str();
}

std::string MCTaskSimulation::equation_vars()
{
	std::stringstream res;

	if(measure_task)
	{
		res << "unsigned long long measure_task_start, measure_task_end;\n";
		res << "std::vector<std::pair<unsigned,unsigned long long> > measure_task;\n";
	}

	switch(tc)
	{
	case(WAIT):
		res << tsc_wait_equation_vars();
		break;
	case(VECTOR):
		res << vector_equation_vars();
		break;
	default:
		break;
	}
	return res.str();
}

std::string MCTaskSimulation::equation_vars_init()
{
	std::stringstream res;
	switch(tc)
	{
	case(WAIT):
		res << tsc_wait_equation_vars_init();
		break;
	case(VECTOR):
		res << vector_equation_vars_init();
		break;
	default:
		break;
	}
	if(measure_task)
			res << "measure_task = std::vector<std::pair<unsigned,unsigned long long> >(" << sched->get_num_tasks() << ",std::pair<unsigned,unsigned long long>(0u,0ull));\n";

	return res.str();
}

std::string MCTaskSimulation::equation_vars_destr()
{
	std::string res;
	switch(tc)
	{
	case(WAIT):
		res = tsc_wait_equation_vars_destr();
		break;
	case(VECTOR):
		res = vector_equation_vars_destr();
		break;
	default:
		break;
	}
	return res;
}
