/*
 * PTHClassSimulation.cpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

#include <Simulation/PTHClassSimulation.hpp>

#include <fstream>

PTHClassSimulation::PTHClassSimulation(UnorderedSchedule const * in, std::string const & name) :  MCClassSimulation(in,name) {
	MCSimulation::add_includeable(Includeables::IOSTREAM);
	type = PTH;
	comm = new PTHCommunicationSimulation(in);
	add_includeable(Includeables::VECTOR);
	add_includeable(Includeables::PTH_LIB);
	add_includeable(Includeables::FSTREAM);
	add_includeable(Includeables::UTILITY);
}

PTHClassSimulation::~PTHClassSimulation() {
	delete comm;
}

std::string PTHClassSimulation::get_string()
{
	std::stringstream res("");

	res << includes();

	res << comm->functions();

	res << generate_class();

	res << main_function();

	return res.str();
}

std::string PTHClassSimulation::main_function()
{
	std::stringstream res("");
	res << "int main(int argc, char *argv[]) {\n";
	res << "unsigned long long start_t1, start_t2, end_t,sum = 0;\n";
	res << "int  id;\n";
	res << "start_t1 = rdtsc();\n";
	res << name << " inst(id);\n";
	res << "for(unsigned i=0;i<"<< repeat_count <<";++i) {\n";
	res << "start_t2 = rdtsc();\n";
	res << "inst.execute();\n";
	res << "end_t = rdtsc();\n";
	res << "sum += end_t-start_t2;\n";
	res << "}\n";
	res << "std::cout << \"needed \" << sum/" << repeat_count << " << \" cycles for execution\\n\";\n";
	res << "return 0;\n}";
	return res.str();
}

std::string PTHClassSimulation::cpp_core_function(unsigned core_num)
{
	std::stringstream res("");

	res << "void * " << name << "::core_cast" << core_num << "(void * obj) {\n";
	res << "return reinterpret_cast<" << name << " *>(obj)->core" << core_num << "();\n";
	res << "}\n\n";

	res << "void * " << name << "::core" << core_num << "() { \n";
	if(core_num !=0)
	{
		res << "long long unsigned measure_recv_start,measure_recv_end;\n";
		res << "long long unsigned measure_send_start,measure_send_end;\n";
		res << "while(true) {\n";
		res << "pthread_spin_lock(&(core_locks_s[" << core_num-1 << "]));\n";
		res << "if(finished) pthread_exit(0);\n";
	}
	for(const MCNode * n : get_sched()->get_schedule(core_num))
	{
		res << comm->task(n) << "\n";
	}
	if(core_num !=0)
		res << "pthread_spin_unlock(&(core_locks_e[" << core_num-1 << "]));\n";
	if(core_num!=0)
		res << "}\n";
	res << "}\n\n";
	return res.str();
}

std::string PTHClassSimulation::core_function(unsigned core_num)
{
	std::stringstream res("");
	res << "static void * core_cast" << core_num <<"(void * obj);\n\n";

	res << "void * core" << core_num << "();\n\n";
	return res.str();
}

std::string PTHClassSimulation::execute_function()
{
	std::stringstream res("");
	res << "void execute();\n\n";
	return res.str();
}

std::string PTHClassSimulation::cpp_execute_function()
{
	std::stringstream res("");
	res << "void " << name << "::execute() {\n";

	for(unsigned i=0;i<get_sched()->size()-1 && comm->max_comms>0;++i)
	{
		res << "pthread_spin_unlock(&(core_locks_s[" << i << "]));\n";
	}
	res << "core0();\n";
	for(unsigned i=0;i<get_sched()->size()-1 && comm->max_comms>0;++i)
	{
		res << "pthread_spin_lock(&(core_locks_e[" << i << "]));\n";
	}
	res << "}\n";
	return res.str();
}
