/*
 * OMClassSimulation.cpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

#include <Simulation/OMClassSimulation.hpp>

#include <fstream>

OMClassSimulation::OMClassSimulation(UnorderedSchedule const * in, std::string const & name) :  MCClassSimulation(in,name) {
	MCSimulation::add_includeable(Includeables::IOSTREAM);
	type = OM;

	omcomm = new OMCommunicationSimulation(in);
	comm = omcomm;
}

OMClassSimulation::~OMClassSimulation() {
	delete comm;
}

std::string OMClassSimulation::get_string()
{
	std::stringstream res("");

	res << includes();

	res << comm->functions();

	res << generate_class();

	res << main_function();

	return res.str();
}

std::string OMClassSimulation::main_function()
{
	std::stringstream res("");
	res << "int main(int argc, char *argv[]) {\n";
	res << "unsigned long long start_t1, start_t2, end_t,sum = 0;\n";
	res << "int  id;\n";
	res << "start_t1 = rdtsc();\n";
	res << name << " inst(id);\n";
	res << "for(unsigned i=0;i<"<< repeat_count <<";++i) {\n";
	res << "start_t2 = rdtsc();\n";
	res << "inst.execute();\n";
	res << "end_t = rdtsc();\n";
	res << "sum += end_t-start_t2;\n";
	res << "}\n";
	res << "std::cout << \"needed \" << sum/" << repeat_count << " << \" cycles for execution\\n\";\n";
	res << "return 0;\n}";
	return res.str();
}

std::string OMClassSimulation::core_function(unsigned core_num)
{
	std::stringstream res("");

	return res.str();
}

std::string OMClassSimulation::cpp_core_function(unsigned core_num)
{
	std::stringstream res("");

	return res.str();
}

std::string OMClassSimulation::execute_function()
{
	std::stringstream res("");
	res << "void execute();\n\n";
	return res.str();
}

std::string OMClassSimulation::cpp_execute_function()
{
	std::stringstream res("");
	res << "void " << name << "::execute() {\n";
	res << "#pragma omp parallel num_threads(" << get_sched()->size() << ")\n";
	res << "{\n";
	for(stype i=0;i<omcomm->free_in_lvl.size();++i)
	{
		res << "#pragma omp sections\n";
		res << "{\n";
		for(auto l : omcomm->free_in_lvl[i])
		{
			for(auto n : l)
			{
				res << "#pragma omp section\n";
				res << "{\n";
				res << omcomm->task(n);
				res << "}\n";
			}
		}
		res << "}\n";
	}
	res << "}\n";
	res << "}\n";
	return res.str();
}

