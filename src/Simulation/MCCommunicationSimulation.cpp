/*
 * MCCommunicationSimulation.cpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#include <Simulation/MCCommunicationSimulation.hpp>
#include <sstream>
#include <map>

bool MCCommunicationSimulation::reduction = false;

MCCommunicationSimulation::MCCommunicationSimulation(UnorderedSchedule const * in) : MCTaskSimulation(in) {
	init();
}

MCCommunicationSimulation::~MCCommunicationSimulation() {
}

void MCCommunicationSimulation::set_measure_recv(bool in,std::string recv_name)
{
	measure_recv = in;
	recv_file = recv_name;
}

void MCCommunicationSimulation::set_measure_send(bool in,std::string send_name)
{
	measure_send = in;
	send_file = send_name;
}


std::string MCCommunicationSimulation::declarations()
{
	std::stringstream res("");
	if(measure_recv && num_comms>0)
	{
		res << "long long unsigned measure_recv_start,measure_recv_end;\n";
		res << "std::vector<std::pair<unsigned,unsigned long long> > measure_recv;\n";
	}
	if(measure_send && num_comms>0)
	{
		res << "long long unsigned measure_send_start,measure_send_end;\n";
		res << "std::vector<std::pair<unsigned,unsigned long long> > measure_send;\n";
	}
	res << MCTaskSimulation::declarations();
	return res.str();
}

std::string MCCommunicationSimulation::initializations()
{
	std::stringstream res("");
	if(measure_recv && num_comms>0)
		res << "measure_recv = std::vector<std::pair<unsigned,unsigned long long> >(" << sched->get_num_tasks()
			<< ",std::pair<unsigned,unsigned long long>(0,0));\n";

	if(measure_send && num_comms>0)
		res << "measure_send = std::vector<std::pair<unsigned,unsigned long long> >(" << sched->get_num_tasks()
			<< ",std::pair<unsigned,unsigned long long>(0,0));\n";
	res << MCTaskSimulation::initializations();
	return res.str();
}

std::string MCCommunicationSimulation::deinitializations()
{
	std::stringstream res("");
	res << MCTaskSimulation::deinitializations();

	if(num_comms>0)
	{
		if(measure_recv || measure_send)
		{
			res << "std::ofstream ous;\n";
			res << "int num;\n";
			res << "unsigned long long sum = 0;\n";
		}
		if(measure_recv)
		{
			res << "for(unsigned i=0;i<measure_recv.size();++i)\n";
			res << "	if(measure_recv[i].first>0)\n";
			res << "		sum += (double)measure_recv[i].second/measure_recv[i].first;\n";
			res << "ous.open(\"" << recv_file << ".csv\", std::ofstream::out);\n";
			res << "ous << \" node_id,cycles (sum_overhead=\"<< sum << \")\\n\";\n";
			res << "for(unsigned i=0;i<measure_recv.size();++i)\n";
			res << "	if(measure_recv[i].first>0)\n";
			res << "		ous << i << \",\" << (double)measure_recv[i].second/measure_recv[i].first << \"\\n\";\n";
			res << "	else\n";
			res << " 		ous << i << \",\" << 0 << \"\\n\";\n";
			res << "ous.close();\n\n";
		}
		if(measure_send)
		{
			res << "sum = 0;\n";
			res << "for(unsigned i=0;i<measure_send.size();++i)\n";
			res << "	if(measure_send[i].first>0)\n";
			res << "		sum += (double)measure_send[i].second/measure_send[i].first;\n";
			res << "ous.open(\"" << send_file << ".csv\", std::ofstream::out);\n";
			res << "ous << \" node_id,cycles (sum_overhead=\"<< sum << \")\\n\";\n";
			res << "ous << \" node_id,cycles \\n\";\n";
			res << "for(unsigned i=0;i<measure_send.size();++i)\n";
			res << "	if(measure_send[i].first>0)\n";
			res << "		ous << i << \",\" << (double)measure_send[i].second/measure_send[i].first << \"\\n\";\n";
			res << "	else\n";
			res << " 		ous << i << \",\" << 0 << \"\\n\";\n";
			res << "ous.close();\n";

		}
	}

	return res.str();
}

std::string MCCommunicationSimulation::task(const MCNode * n)
{
	std::stringstream res("");
	if(measure_recv && comms_in[n->get_id()].size() >0)
	{
		res << "measure_recv_start= rdtsc();\n";
	}
	res << task_pre_comm(n);
	if(measure_recv && comms_in[n->get_id()].size() >0)
	{
		res << "measure_recv[" << n->get_id() << "].second += rdtsc() - measure_recv_start;\n";
		res << "++measure_recv[" << n->get_id() << "].first;\n";
	}
	res << MCTaskSimulation::task(n);
	if(measure_send && comms_out[n->get_id()].size() >0)
	{
		res << "measure_send_start= rdtsc();\n";
	}
	res << task_post_comm(n);
	if(measure_send && comms_out[n->get_id()].size() > 0)
	{
		res << "measure_send[" << n->get_id() << "].second += rdtsc() - measure_send_start;\n";
		res << "++measure_send[" << n->get_id() << "].first;\n";
	}

	return res.str();
}


void MCCommunicationSimulation::init()
{
	measure_recv = false;
	measure_send = false;
	num_sends = std::vector<stype>(sched->size(),0);
	acc_num_sends = std::vector<stype>(sched->size(),0);
	find_communications();
}


void MCCommunicationSimulation::set_comm_reduction(bool in)
{
	reduction = in;
}

void MCCommunicationSimulation::find_communications()
{
	std::vector<stype> task_map = sched->get_task_map();
	cores_to_comms = std::vector<std::list<Communication> >(sched->size(),std::list<Communication>());
	comms_in = std::vector<std::list<Communication> >(task_map.size(),std::list<Communication>());
	comms_out = std::vector<std::list<Communication> >(task_map.size(),std::list<Communication>());
	unsigned ts, tt;
	Communication temp;
	num_comms = 0;
	for(unsigned i=0;i<sched->size();++i)
	{
		for(const MCNode * n : sched->get_schedule(i))
		{
			for(auto nit : *n)
			{
				tt = task_map[n->get_id()];
				ts = task_map[nit.second->get_id()];

				temp = {nit.second->get_id(),n->get_comm_costs(nit.second->get_id()),DISTRIBUTED,num_comms,tt,ts,n->get_comm_bool(nit.first),n->get_comm_float(nit.first),n->get_comm_int(nit.first)};

				if(tt==ts) continue;
				++num_comms;
				++num_sends[tt];
				cores_to_comms[temp.core].push_back(temp);
				cores_to_comms[temp.partner_core].push_back(temp);
				comms_out[n->get_id()].push_back(temp);


				temp.partner = n->get_id();
				temp.core = ts;
				temp.partner_core = tt;
				comms_in[nit.second->get_id()].push_back(temp);
			}
		}
	}
	if(reduction)
	{
		//ordnung der Tasks auf dem selben knoten erstellen
		std::vector<stype> n_orders(sched->get_num_tasks(),0);
		for(stype i=0;i<sched->size();++i)
		{
			stype j = 0;
			for(auto n : (*sched)[i])
				n_orders[n->get_id()] = j++;
		}

		// die mergen wo der target knoten nachrichtEN vom selben sender bekommt
		for(stype i=0;i<comms_in.size();++i)
		{
			std::map<stype,std::list<Communication> > cluster;
			for(auto c : comms_in[i])
			{
				cluster[c.partner_core].push_back(c);
			}
			comms_in[i].clear();
			for(auto c : cluster)
			{
				if(c.second.size() > 1)
				{
					std::list<Communication> temp = c.second;
					c.second.clear();
					std::list<Communication>::iterator it = temp.begin();
					Communication c_new = *it;
					++it;
					for(;it!=temp.end();++it)
					{
						c_new.byte += it->byte;
						c_new.cbool += it->cbool;
						c_new.cfloat += it->cfloat;
						c_new.cint += it->cint;
						if(n_orders[c_new.partner]<n_orders[it->partner])
							c_new.partner = it->partner;
					}
					comms_in[i].push_back(c_new);
				}
				else
					comms_in[i].push_back(c.second.back());
			}


		}
		comms_out.clear();
		cores_to_comms.clear();
		cores_to_comms = std::vector<std::list<Communication> >(sched->size(),std::list<Communication>());
		comms_out = std::vector<std::list<Communication> >(task_map.size(),std::list<Communication>());
		num_sends = std::vector<stype>(sched->size(),0);
		num_comms = 0;
		for(unsigned i=0;i<comms_in.size();++i)
		{
			Communication temp;
			for(std::list<Communication>::iterator it=comms_in[i].begin();it!=comms_in[i].end();++it)
			{
				++num_sends[it->partner_core];
				temp = *it;
				temp.partner = i;
				temp.partner_core = it->core;
				temp.core = it->partner_core;
				temp.comm_id = num_comms;
				it->comm_id = num_comms;
				comms_out[it->partner].push_back(temp);
				cores_to_comms[temp.core].push_back(temp);
				cores_to_comms[temp.partner_core].push_back(temp);
				++num_comms;
			}
		}

		// Reduzierung der Sends auf den Selben core zu dem Task der am ehesten ist

		// die mergen wo der target knoten nachrichtEN vom selben sender bekommt
		for(stype i=0;i<comms_out.size();++i)
		{
			std::map<stype,std::list<Communication> > cluster;
			for(auto c : comms_out[i])
			{
				cluster[c.partner_core].push_back(c);
			}
			comms_out[i].clear();
			for(auto c : cluster)
			{
				if(c.second.size() > 1)
				{
					std::list<Communication> temp = c.second;
					c.second.clear();
					std::list<Communication>::iterator it = temp.begin();
					Communication c_new = *it;
					++it;
					for(;it!=temp.end();++it)
					{
						c_new.byte += it->byte;
						c_new.cbool += it->cbool;
						c_new.cfloat += it->cfloat;
						c_new.cint += it->cint;
						if(n_orders[c_new.partner]>n_orders[it->partner])
							c_new.partner = it->partner;
					}
					comms_out[i].push_back(c_new);
				}
				else
					comms_out[i].push_back(c.second.back());
			}


		}
		comms_in.clear();
		cores_to_comms.clear();
		cores_to_comms = std::vector<std::list<Communication> >(sched->size(),std::list<Communication>());
		comms_in = std::vector<std::list<Communication> >(task_map.size(),std::list<Communication>());
		num_sends = std::vector<stype>(sched->size(),0);
		num_comms = 0;
		for(unsigned i=0;i<comms_out.size();++i)
		{
			Communication temp;
			for(std::list<Communication>::iterator it=comms_out[i].begin();it!=comms_out[i].end();++it)
			{
				++num_sends[it->partner_core];
				temp = *it;
				temp.comm_id = num_comms;
				it->comm_id = num_comms;
				cores_to_comms[temp.core].push_back(temp);
				cores_to_comms[temp.partner_core].push_back(temp);
				temp.partner = i;
				temp.partner_core = it->core;
				temp.core = it->partner_core;
				comms_in[it->partner].push_back(temp);
				++num_comms;
			}
		}
	}
	max_comms = 0;
	for(unsigned i=0;i<comms_in.size();++i)
	{
		if(comms_in[i].size()>max_comms) max_comms = comms_in[i].size();
	}
}

std::ostream & operator<<(std::ostream & in, MCCommunicationSimulation::Communication const & c)
{
	in << "Comm " << c.comm_id << " from " << c.core << " to " << c.partner_core;
	return in;
}


