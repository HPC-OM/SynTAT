/*
 * OMCommunicationSimulation.cpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#include <Simulation/OMCommunicationSimulation.hpp>
#include <sstream>

OMCommunicationSimulation::OMCommunicationSimulation(UnorderedSchedule const * in) : MCCommunicationSimulation(in) {
	init();
}

OMCommunicationSimulation::~OMCommunicationSimulation() {
}

void OMCommunicationSimulation::init()
{
	add_includeable(Includeables::VECTOR);
	add_includeable(Includeables::OM_LIB);
	add_includeable(Includeables::FSTREAM);
	add_includeable(Includeables::UTILITY);

	std::vector<bool> scheded(sched->get_num_tasks(),false);
	std::vector<stype> deps(sched->get_num_tasks(),0);
	for(stype i=0;i<sched->size();++i)
		for(const MCNode * n : (*(sched))[i])
		{
			if(n->get_id()!=0 && n->get_costs()!=0)
				for(auto p : *n)
					++deps[p.first];
		}
	bool flag = true;
	while(flag)
	{
		flag = false;
		std::list<std::list<const MCNode *> > temp;
		for(stype i=0;i<sched->size();++i)
		{
			temp.push_back(std::list<const MCNode *>());
			for(const MCNode * n : (*(sched))[i])
			{
				if(scheded[n->get_id()])
					continue;
				if(deps[n->get_id()]!=0)
					break;
				temp.back().push_back(n);
				flag = true;
				scheded[n->get_id()] = true;
			}
		}
		for(auto l : temp)
			for(auto n : l)
				for(auto p : *n)
					if(n->get_id()!=0 && n->get_costs()!=0)
						--deps[p.first];
		if(flag)free_in_lvl.push_back(temp);
	}

}

std::string OMCommunicationSimulation::task_pre_comm(const MCNode * n)
{
	return "";
}

std::string OMCommunicationSimulation::task_post_comm(const MCNode * n)
{
	return "";
}
