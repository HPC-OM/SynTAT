#include <Simulation/MCClassSimulation.hpp>



MCClassSimulation::MCClassSimulation(const UnorderedSchedule * in, const std::string & name) : ExecutionSimulation(name) {
	comm = NULL;
}


std::string MCClassSimulation::generate_cpp()
{

	std::stringstream res("");

	res << "#include \"" << name << ".h\"\n\n\n";

	res << name << "::" << name << "(int num) {\n";
	res << initializations() << "\n";
	res << "}\n";

	res << name << "::~" << name << "() {\n";
	res << deinitializations() << "\n";
	res << "}\n";
	for(unsigned i=0;i<get_sched()->size();++i)
	{
		res << cpp_core_function(i);
	}

	res << cpp_execute_function();

	return res.str();
}

std::string MCClassSimulation::generate_class()
{
	std::stringstream res("");

	res << "class " << name << "{\n";

	res << "private:\n";

	res << declarations();

	for(unsigned i=0;i<get_sched()->size();++i)
	{
		res << core_function(i);
	}

	res << "public:\n";

	res << name << "(int num);\n\n";

	res << "~" << name << "();\n\n";


	res << execute_function();

	res << "};\n";

	return res.str();
}

const UnorderedSchedule * MCClassSimulation::get_sched()
{
	return comm->get_sched();
}

void MCClassSimulation::set_measure_recv(bool in,std::string recv_name)
{
	comm->set_measure_recv(in, recv_name);
}

void MCClassSimulation::set_measure_send(bool in,std::string send_name)
{
	comm->set_measure_send(in,send_name);
}

void MCClassSimulation::set_measure_task(bool in,std::string task_name)
{
	comm->set_measure_task(in, task_name);
}

std::string MCClassSimulation::declarations()
{
	return comm->declarations();
}

std::string MCClassSimulation::initializations()
{
	return comm ->initializations();
}

std::string MCClassSimulation::deinitializations()
{
	return comm->deinitializations();
}
