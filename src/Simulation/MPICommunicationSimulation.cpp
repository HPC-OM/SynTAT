/*
 * MPICommunicationSimulation.cpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#include <Simulation/MPICommunicationSimulation.hpp>
#include <sstream>

MPICommunicationSimulation::MPICommunicationSimulation(UnorderedSchedule const * in) : MCCommunicationSimulation(in) {
	init();
}

MPICommunicationSimulation::~MPICommunicationSimulation() {
}


std::string MPICommunicationSimulation::declarations()
{
	std::stringstream res("");
	res << MCCommunicationSimulation::declarations();
	if(num_comms>0)
	{
		res << "class Comm {\n";
		res << "public:\n";
		res << "char * data;\n";
		res << "unsigned size;\n\n";
		res << "short * b;\n";
		res << "double * f;\n";
		res << "int * i;\n";

		res << "Comm() {\n";
		res << "size = 0;\n";
		res << "data = NULL;\n";
		res << "}\n\n";

		res << "void alloc(int b_size, int f_size, int i_size) {\n";
		res << "size = (b_size * sizeof(short) + f_size * sizeof(double) + i_size * sizeof(int))/sizeof(char);\n";
		res << "data = new char[size];\n";
		res << "\n";
		res << "b = (short *)&(data[0]);\n";
		res << "f = (double *)&(b[b_size]);\n";
		res << "i = (int *)&(f[f_size]);\n";
		res << "}\n\n";

		res << "~Comm() {\n";
		res << "if(data!=NULL)\n";
		res << "	delete[] data;\n";
		res << "}\n;";

		res << "};\n";

		res << "std::vector<MPI_Request> in_requ;\n";
		res << "std::vector<MPI_Request> out_requ;\n";
		res << "std::vector<Comm> data;\n";
	}


	res << "unsigned id;\n";
	return res.str();
}


std::string MPICommunicationSimulation::initializations()
{
	std::stringstream res("");
	res << MCCommunicationSimulation::initializations();
	res << "\n";
	res << "id = num;\n";

	if(num_comms>0)
	{
		res << "in_requ = std::vector<MPI_Request>(" << num_comms << ",MPI_Request());\n";
		res << "out_requ = std::vector<MPI_Request>(" << num_comms << ",MPI_Request());\n";
		res << "data = std::vector<Comm>(" << num_comms << ",Comm());\n\n";
		for(unsigned i=0;i<cores_to_comms.size();++i)
		{
			res << "if(id==" <<i << ") {\n";
			for(auto c : cores_to_comms[i])
			{
				res << "data["<< c.comm_id << "].alloc(" << c.cbool << "," << c.cfloat << "," << c.cint <<  ");\n";
			}
			res << "}\n";
		}
	}
	return res.str();
}






std::string MPICommunicationSimulation::deinitializations()
{
	std::stringstream res("");

	if(measure_recv || measure_send || measure_task)
	{
		res << "std::ofstream ous;\n";
		res << "int num;\n";
		res << "unsigned long long sum;\n";
		res << "float * buff;\n";
	}

	if(num_comms>0)
	{
		if(measure_recv)
		{
			res << "float * recv_t_array = new float[measure_recv.size()];\n";
			res << "for(unsigned i=0;i<measure_recv.size();++i) {\n";
			res << "if(measure_recv[i].first != 0)\n";
			res << "	recv_t_array[i] = (float)measure_recv[i].second/measure_recv[i].first;\n";
			res << "else\n";
			res << "	recv_t_array[i] = 0;\n";
			res << "}\n";
			if(num_comms>0)
			{
				res << "MPI_Comm_size(MPI_COMM_WORLD,&num);\n";
				res << "if(id == 0) \n";
				res << "	buff = new float[measure_recv.size()*num];\n";
				res << "MPI_Gather(recv_t_array,measure_recv.size(),MPI_FLOAT,buff,measure_recv.size(),MPI_FLOAT,0,MPI_COMM_WORLD);\n";
				res << "if(id == 0) {\n";
				res << "float max;\n";
				res << "for(unsigned i=0;i<measure_recv.size();++i) {\n";
				res << "max = buff[i];\n";
				res << "for(unsigned j=1;j<num;++j) {\n";
				res << "if(max < buff[i+j*measure_recv.size()])\n";
				res << "	max = buff[i+j*measure_recv.size()];\n";
				res << "}\n";
				res << "recv_t_array[i] = max;\n";
				res << "}\n";
				res << "delete[] buff;\n";
				res << "sum = 0;\n";
				res << "for(unsigned i=0;i<measure_recv.size();++i)\n";
				res << "	sum += recv_t_array[i];\n";
				res << "}\n";
			}
			res << "if(id == 0) {\n";
			res << "ous.open(\"" << recv_file << ".csv\", std::ofstream::out);\n";
			res << "ous << \" node_id,cycles (sum_overhead=\"<< sum << \")\\n\";\n";
			res << "for(unsigned i=0;i<measure_recv.size();++i) ous << i << \",\" << recv_t_array[i] << \"\\n\";\n";
			res << "delete[] recv_t_array;\n";
			res << "ous.close();\n";
			res << "}\n";



		}
		if(measure_send)
		{
			res << "float * send_t_array = new float[measure_send.size()];\n";
			res << "for(unsigned i=0;i<measure_send.size();++i) {\n";
			res << "if(measure_send[i].first != 0)\n";
			res << "	send_t_array[i] = (float)measure_send[i].second/measure_send[i].first;\n";
			res << "else\n";
			res << "	send_t_array[i] = 0;\n";
			res << "}\n";
			if(num_comms>0)
			{
				res << "MPI_Comm_size(MPI_COMM_WORLD,&num);\n";
				res << "if(id == 0) \n";
				res << "	buff = new float[measure_send.size()*num];\n";
				res << "MPI_Gather(send_t_array,measure_send.size(),MPI_FLOAT,buff,measure_send.size(),MPI_FLOAT,0,MPI_COMM_WORLD);\n";
				res << "if(id == 0) {\n";
				res << "float max;\n";
				res << "for(unsigned i=0;i<measure_send.size();++i) {\n";
				res << "max = buff[i];\n";
				res << "for(unsigned j=1;j<num;++j) {\n";
				res << "if(max < buff[i+j*measure_send.size()])\n";
				res << "	max = buff[i+j*measure_send.size()];\n";
				res << "}\n";
				res << "send_t_array[i] = max;\n";
				res << "}\n";
				res << "delete[] buff;\n";
				res << "}\n";
			}
			res << "if(id == 0) {\n";
			res << "ous.open(\"" << send_file << ".csv\", std::ofstream::out);\n";
			res << "ous << \" node_id,cycles \\n\";\n";
			res << "for(unsigned i=0;i<measure_send.size();++i) ous << i << \",\" << send_t_array[i] << \"\\n\";\n";
			res << "ous.close();\n";
			res << "delete[] send_t_array;\n";
			res << "}\n";

		}
	}

	if(measure_task)
	{
		res << "float * task_t_array = new float[measure_task.size()];\n";
		res << "for(unsigned i=0;i<measure_task.size();++i) {\n";
		res << "if(measure_task[i].first != 0)\n";
		res << "	task_t_array[i] = (float)measure_task[i].second/measure_task[i].first;\n";
		res << "else\n";
		res << "	task_t_array[i] = 0;\n";
		res << "}\n";
		if(num_comms>0)
		{
			res << "MPI_Comm_size(MPI_COMM_WORLD,&num);\n";
			res << "if(id == 0) \n";
			res << "	buff = new float[measure_task.size()*num];\n";
			res << "MPI_Gather(task_t_array,measure_task.size(),MPI_FLOAT,buff,measure_task.size(),MPI_FLOAT,0,MPI_COMM_WORLD);\n";
			res << "if(id == 0) {\n";
			res << "float max;\n";
			res << "for(unsigned i=0;i<measure_task.size();++i) {\n";
			res << "max = buff[i];\n";
			res << "for(unsigned j=1;j<num;++j) {\n";
			res << "if(max < buff[i+j*measure_task.size()])\n";
			res << "	max = buff[i+j*measure_task.size()];\n";
			res << "}\n";
			res << "task_t_array[i] = max;\n";
			res << "}\n";
			res << "delete[] buff;\n";
			res << "}\n";
		}
		res << "if(id == 0) {\n";
		res << "ous.open(\"" << task_file << ".csv\", std::ofstream::out);\n";
		res << "ous << \" node_id,cycles \\n\";\n";
		res << "for(unsigned i=0;i<measure_task.size();++i) ous << i << \",\" << task_t_array[i] << \"\\n\";\n";
		res << "ous.close();\n";
		res << "delete[] task_t_array;\n";
		res << "}\n";
	}

	res << MCTaskSimulation::deinitializations();

	return res.str();
}


std::string MPICommunicationSimulation::task_pre_comm(const MCNode * n)
{
	std::stringstream res("");
	unsigned id = n->get_id(), counter;
	//Abhängigkeiten empfangen
	if(comms_in[id].size()>0 )
	{
		counter = 0;

		for(std::list<Communication>::const_iterator it = comms_in[id].begin();it!=comms_in[id].end();++it)
		{
			res << "MPI_Irecv(data[" << it->comm_id << "].data"<< ",data[" << it->comm_id<< "].size,MPI_BYTE," << it->partner_core << "," << it->comm_id << ",MPI_COMM_WORLD, &(in_requ[" << counter++ << "]));\n";

		}
		res << "MPI_Waitall(" << counter << ",&(in_requ[0]),MPI_STATUSES_IGNORE);\n";

	}


	return res.str();
}

std::string MPICommunicationSimulation::task_post_comm(const MCNode * n)
{

	std::stringstream res("");

	// Abhängigkeiten schicken
	unsigned id = n->get_id();

	if(comms_out[id].size()>0)
	{
		for(std::list<Communication>::const_iterator it = comms_out[id].begin();it!=comms_out[id].end();++it)
		{
			res << "MPI_Isend(data[" << it->comm_id << "].data"<< ",data[" << it->comm_id << "].size,MPI_BYTE," << it->partner_core << "," << it->comm_id << ",MPI_COMM_WORLD, &(out_requ[" << acc_num_sends[it->core]++ << "]));\n";
		}

	}
	return res.str();
}


void MPICommunicationSimulation::init()
{


}

std::string MPICommunicationSimulation::get_mpi_type(float)
{
	return "MPI_FLOAT";
}

std::string MPICommunicationSimulation::get_mpi_type(double)
{
	return "MPI_DOUBLE";
}


std::string MPICommunicationSimulation::get_mpi_type(char)
{
	return "MPI_CHAR";
}

