/*
 * ComparisonSimulation.cpp
 *
 *  Created on: 22.09.2014
 *      Author: marc
 */

#include <Simulation/ComparisonSimulation.hpp>
#include <Simulation/MCTaskSimulation.hpp>
#include <fstream>
#include <cmath>

ComparisonSimulation::ComparisonSimulation(const std::string & name) : MCSimulation() ,ExecutionSimulation(name) {
	this->add_includeable(Includeables::VECTOR);
	this->add_includeable(Includeables::FSTREAM);
	mpi_needed = false;
	mpi_boost_needed = false;
	tbb_needed = false;
	pth_needed = false;
	om_needed = false;
	max_cores = 0;
	estimated_time = 0;
}

ComparisonSimulation::~ComparisonSimulation() {
}

std::string ComparisonSimulation::generate_bash_venus()
{
	std::set<stype> nodes;
	stype max_cores = this->max_cores;
	if(max_cores%8!=0) max_cores += 8-(max_cores%8);

	std::stringstream res(""), flags("");
	std::string sb = "#BSUB ";
	res << "#!/bin/bash\n";
	res << sb << "-n " << max_cores << "\n";
	res << sb << "-R span[hosts=" << max_nodes.size() << "]\n";
	res << sb << "-P zihforschung\n";
	res << sb << "-u marc.hartung@tu-dresden.de\n";
	res << "\n";

	res << "make --file=" << name << ".makefile -j" << max_cores << "\n";

	res << " echo \"runing\"\n";
	if(mpi_needed || mpi_boost_needed)
		res << "mpirun -np " << max_cores << " ";
	res << "~/"<< name <<"/" << name << "\n";

	return res.str();
}

std::string ComparisonSimulation::generate_make()
{
	std::stringstream res("");
	if(mpi_needed || mpi_boost_needed)
		res << "CC=mpicxx\n";
	else
		res << "CC=g++\n";
	res << "CFLAGS=-c\n";

	res << "LDFLAGS=";
	if(tbb_needed)
		res << "-I$(HOME)/programs/include -L$(HOME)/programs/lib/tbb-gcc4.9.1 -ltbb";
	if(pth_needed)
		res << " -pthread";
	if(om_needed)
		res << " -fopenmp";
	if(mpi_boost_needed)
		res << "-lboost_serialization -lboost_mpi";
	res << "\n";

	res << "SOURCES=";
	for(auto s : sims)
		res << s->get_name() << ".cpp ";
	res << name << ".cpp\n";

	res << "OBJECTS=$(SOURCES:.cpp=.o)\n";
	res << "EXECUTABLE=" << name << "\n\n";

	res << "all: $(SOURCES) $(EXECUTABLE)\n\n";

	res << "$(EXECUTABLE): $(OBJECTS)\n";
	res << "	$(CC) $(OBJECTS) -o $@ -I$(INC_LIBRARY_PATH) -L$(LD_LIBRARY_PATH) $(LDFLAGS)\n\n";

	res << ".cpp.o:\n";
	res << "	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)\n\n";

	res << "clean:\n";
	res << "	rm -rf *~ \\#_* *.o\n\n";

	return res.str();
}

std::string ComparisonSimulation::generate_bash_home()
{
	std::stringstream res(""), flags("");

	if(tbb_needed)
		flags << " -ltbb";
	if(pth_needed)
		flags << " -pthread";
	if(om_needed)
		flags << " -fopenmp";

	res << " echo \"compiling\"\n";

	res << "make --file=" << name << ".makefile -j" << max_cores << "\n";

	res << " echo \"runing\"\n";
	if(mpi_needed || mpi_boost_needed)
		res << "mpirun -np " << max_cores << " ";
	res << "~/"<< name <<"/" << name << "\n";

	return res.str();
}

std::string ComparisonSimulation::generate_bash_taurus()
{
	std::set<stype> nodes;
	std::stringstream ss("");
	if(std::round(estimated_time/60.0)<10)
		ss << "0";
	ss << std::round(estimated_time/60.0) << ":";
	if(((int)estimated_time%60)<10)
		ss << "0";
	ss << ((int)estimated_time%60) << ":00";

	std::stringstream res(""), flags("");
	std::string sb = "#SBATCH ";
	res << "#!/bin/bash\n";
	res << sb << "--ntasks=" << max_cores << "\n";
	res << sb << "--cpus-per-task=1\n";
	res << sb << "--nodes=1\n";
	//res << sb << "--exclusive\n";
	res << sb << "-A zihforschung\n";
	res << sb << "--mail-type ALL\n";
	if(max_cores>16)
		res << sb << "--partition smp\n";
	else
		res << sb << "--partition sandy\n";
	res << sb << "--time=" << ss.str() << "\n";
	res << sb << "--mem-per-cpu=1900\n";
	res << "\n";


	if(mpi_boost_needed)
		res << "module load boost/1.55.0-gnu4.8\n";
	else if(mpi_needed)
		res << "module load openmpi\n";
	else if(tbb_needed)
	{
		res << "module load gcc/4.9.1\n";
		res << "export LD_LIBRARY_PATH=\"$HOME/programs/lib/tbb-gcc4.9.1:$LD_LIBRARY_PATH\"\n";
		res << "export INC_LIBRARY_PATH=\"$HOME/programs/include:$INC_LIBRARY_PATH\"\n";
	}

	if(tbb_needed)
		flags << " -I$HOME/programs/include/ -L$HOME/programs/lib/tbb-gcc4.9.1 -ltbb";

	if(pth_needed)
		flags << " -pthread";

	if(om_needed)
		flags << " -fopenmp";

	res << "echo \"compiling\"\n";

	res << "make --file=" << name << ".makefile -j" << max_cores << "\n";

	res << " echo \"running\"\n";
	if(mpi_needed || mpi_boost_needed)
		res << "srun ";
	res << "~/"<< "test" <<"/" << name << "\n";

	return res.str();
}

std::string ComparisonSimulation::generate_main()
{
	std::stringstream res("");
	stype count = 0;
	if(om_needed)
		res << "#define OMP_WAIT_POLICY ACTIVE\n";
	res << includes();
	for(auto s : sims)
	{
		res << "#include \"" << s->get_name() << ".h\"\n";
	}
	res << "\n";
	res << MCTaskSimulation::get_rdtsc_func_imp();
	res << "\n";
	res << "int main(int argc, char *argv[]) {\n";
	res << "int num_procs = 1, id = 0;\n";
	res << "std::vector<unsigned long long> run_t(" << sims.size() << ",0), init_t(" << sims.size() << ",0);\n";
	res << "unsigned sim_counter = 0;\n";
	res << "unsigned long long start_t, end_t;\n\n";
	if(mpi_boost_needed)
	{
		res << "boost::mpi::environment env(argc,argv);\n";
		res << "boost::mpi::communicator world;\n";
		res << "num_procs = world.size();\n";
		res << "id = world.rank();\n\n";
	}
	else if(mpi_needed)
	{
		res << "MPI_Init(&argc,&argv);\n";
		res << "MPI_Comm_size(MPI_COMM_WORLD,&num_procs);\n";
		res << "MPI_Comm_rank(MPI_COMM_WORLD,&id);\n";
	}
	for(auto s : sims)
	{
		res << "start_t = rdtsc();\n";
		res << s->get_name() << "* obj_" << count << " = new " << s->get_name() <<"(id";
		if(s->get_sim_type() == MCSimulation::MPI_BOOST)
			res << ",world);\n";
		else
			res << ");\n";
		res << "end_t = rdtsc();\n";
		res << "init_t[" << count << "]=end_t-start_t;\n";
		res << "for(unsigned i=0;i<" << repeat_count << ";++i) {\n";
		if(mpi_boost_needed)
			res << "world.barrier();\n";
		else if(mpi_needed)
		{
			res << "MPI_Barrier(MPI_COMM_WORLD);\n";
		}
		res << "start_t = rdtsc();\n";
		res << "obj_" << count << "->execute(";
		if(s->get_sim_type() == MCSimulation::MPI_BOOST)
			res << "world);\n";
		else
			res << ");\n";

		res << "end_t = rdtsc();\n";
		//res << "std::cout << \"cycles: \" << end_t-start_t << \"\\n\";\n";
		res << "run_t[" << count << "] += end_t-start_t;\n";
		res << "}\n";
		res << "run_t[" << count << "] /= " << repeat_count << ";\n";
		if(mpi_boost_needed)
			res << "world.barrier();\n";
		else if(mpi_needed)
		{
			res << "MPI_Barrier(MPI_COMM_WORLD);\n";
		}
		res << "delete obj_" << count << ";\n\n";


		res << "if(id == 0) std::cout << \" Simmulation "<< count << "/" << sims.size() << " (" << s->get_name() <<  ") finished\\n\";\n";

		if(mpi_boost_needed)
		{
			res << "world.barrier();\n";
		}
		else if(mpi_needed)
		{
			res << "MPI_Barrier(MPI_COMM_WORLD);\n";
		}
		++count;
	}

	// Gather Information:
	res << "unsigned long long * end_init, * end_run;\n";
	if(mpi_boost_needed || mpi_needed)
	{
		res << "if(id == 0) {\n";
		res << "std::vector<std::vector<unsigned long long> > all_i, all_r;\n";
		res << "end_init = new unsigned long long[" << sims.size() << "];\n";
		res << "end_run = new unsigned long long[" << sims.size() << "];\n";
	}
	if(mpi_boost_needed)
	{
		res << "boost::mpi::gather(world,init_t,all_i,0);\n";
		res << "boost::mpi::gather(world,run_t,all_r,0);\n";
	}
	else if(mpi_needed)
	{
		res << "unsigned long long * temp_i, * temp_r;\n";
		res << "temp_i = new unsigned long long[" << sims.size() << "*num_procs ];\n";
		res << "temp_r = new unsigned long long[" << sims.size() << "*num_procs ];\n";

		res << "MPI_Gather(&(init_t[0])," << sims.size() << ",MPI_UNSIGNED_LONG_LONG,temp_i,"<< sims.size() << ",MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);\n";
		res << "MPI_Gather(&(run_t[0])," << sims.size() << ",MPI_UNSIGNED_LONG_LONG,temp_r,"<< sims.size() << ",MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);\n";

		res << "for(unsigned i=0;i<num_procs;++i) {\n";
		res << "all_i.push_back(std::vector<unsigned long long>());\n";
		res << "all_r.push_back(std::vector<unsigned long long>());\n";
		res << "for(unsigned j=0;j<"<<sims.size()<<";++j) {\n";
		res << "all_i[i].push_back(temp_i[i*" << sims.size() << "+j]);\n";
		res << "all_r[i].push_back(temp_r[i*" << sims.size() << "+j]);\n";
		res << "}\n}\n";
		res << "delete[] temp_i;\ndelete[] temp_r;\n";
	}
	if(mpi_boost_needed || mpi_needed) {
		res << "unsigned long long max_i,max_r;\n";
		res << "for(unsigned i=0;i<" << sims.size() << ";++i) {\n";
			res << "max_i = 0; max_r = 0;\n";
			res << "for(unsigned j=0;j<all_i.size();++j) {\n";
				res << "if(all_i[j][i]>max_i) max_i = all_i[j][i];\n";
				res << "if(all_r[j][i]>max_r) max_r = all_r[j][i];\n";
			res << "}\n";
			res << "end_init[i] = max_i; end_run[i] = max_r;\n";
		res << "}\n";
		res << "}\n";
		res << "else {\n";
	}
	if(mpi_boost_needed) {
		res << "boost::mpi::gather(world,init_t,0); \n";
		res << "boost::mpi::gather(world,run_t,0);\n";
		res << "}\n";
	}
	else if(mpi_needed)
	{
		res << "MPI_Gather(&(init_t[0])," << sims.size() << ",MPI_UNSIGNED_LONG_LONG,NULL,"<< sims.size() << ",MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);\n";
		res << "MPI_Gather(&(run_t[0])," << sims.size() << ",MPI_UNSIGNED_LONG_LONG,NULL,"<< sims.size() << ",MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);\n";
		res << "}\n";

	}
	else
		res << "end_init = &(init_t[0]); end_run = &(run_t[0]);\n";

	//write to file:
	if(mpi_needed || mpi_boost_needed)
		res << "if(id == 0) {\n";

	res << "std::ofstream ous;\n";
	res << "ous.open(\"" << name << ".csv\");\n";
	res << "ous << \"sim_name,num_cores,cycles_init,cycles_run,seq_cycles,speedup\\n\";\n";
	count = 0;
	for(auto s : sims)
	{
		res << "ous << \"" << s->get_name() << "," << s->get_sched()->get_num_cores()
			<< ",\" << end_init[" << count << "] << \",\" << end_run["<< count << "] << \"," << s->get_sched()->get_costs() << "\\n\";\n";
		res << "std::cout << \"" << s->get_name() << "," << s->get_sched()->get_num_cores()
					<< ",\" << end_init[" << count << "] << \",\" << end_run["<< count << "] << \"," << s->get_sched()->get_costs() << ",\" << (float)" << s->get_sched()->get_costs() << "/end_run["<<count<<"]"  << " << \"\\n\";\n";
		++count;
	}
	res << "ous.close();\n";
	if(mpi_needed || mpi_boost_needed)
		res << "}\n";
	if(mpi_needed)
		res << "MPI_Finalize();\n";
	if(pth_needed)
		res << "pthread_exit(0);\n";
	res << "return 0;\n}\n";
	return MCSimulation::ansi_parse(res.str());
}

void ComparisonSimulation::add_class_simulation(MCClassSimulation * in)
{
	if(in->get_sim_type()==TBB)
		tbb_needed = true;
	if(in->get_sim_type()==PTH)
			pth_needed = true;
	if(in->get_sim_type()==OM)
		om_needed = true;
	else if(in->get_sim_type()==MPI)
		mpi_needed = true;
	else if(in->get_sim_type()==MPI_BOOST)
		mpi_boost_needed = true;
	if(mpi_boost_needed && mpi_needed)
		mpi_needed = false;
	sims.push_back(in);
	if(mpi_needed)
		this->add_includeable(MCSimulation::MPI_LIB);
	else if(mpi_boost_needed)
		this->add_includeable(MCSimulation::BOOST_MPI_LIB);
	// Getting: how many nodes are needed and how many cores on each node
	if(in->get_sched()->size()>max_cores)
		max_cores = in->get_sched()->size();

}

std::string ComparisonSimulation::get_string()
{
	return "";
}

void ComparisonSimulation::toFile()
{
	toFile(name);
}

void ComparisonSimulation::toFile(std::string file)
{
	std::ofstream ous;
	unsigned long long sum = 0;
	for(auto s : sims)
	{
		ous.open((s->get_name()+std::string(".h")).c_str());
		ous << s->includes();


		ous <<  MCSimulation::ansi_parse(s->generate_class());

		ous.close();

		ous.open((s->get_name()+std::string(".cpp")).c_str());

		ous <<  MCSimulation::ansi_parse(s->generate_cpp());

		ous.close();

		sum += s->get_sched()->get_costs();
	}
	estimated_time = (double)sum/2.e9 * ((double)repeat_count/60.0) * 1.5 + 0.5*sims.size();

	ous.open((name+std::string("_taurus.sh")).c_str());
	ous << this->generate_bash_taurus();
	ous.close();

	ous.open((name+std::string(".makefile")).c_str());
	ous << this->generate_make();
	ous.close();



	ous.open((name+std::string("_venus.sh")).c_str());
	ous << this->generate_bash_venus();
	ous.close();

	ous.open((name+std::string(".sh")).c_str());
	ous << this->generate_bash_home();
	ous.close();

	ous.open((name+std::string(".cpp")).c_str());
	ous << this->generate_main();
	ous.close();
}


std::string ComparisonSimulation::declarations()
{
	return std::string();
}

std::string ComparisonSimulation::initializations()
{
	return std::string();
}

std::string ComparisonSimulation::deinitializations()
{
	return std::string();
}
