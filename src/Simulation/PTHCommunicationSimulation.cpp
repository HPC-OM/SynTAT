/*
 * PTHCommunicationSimulation.cpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#include <Simulation/PTHCommunicationSimulation.hpp>
#include <sstream>

PTHCommunicationSimulation::PTHCommunicationSimulation(UnorderedSchedule const * in) : MCCommunicationSimulation(in) {
	init();
}

PTHCommunicationSimulation::~PTHCommunicationSimulation() {
}


std::string PTHCommunicationSimulation::declarations()
{
	std::stringstream res("");
	res << MCCommunicationSimulation::declarations();
	if(num_comms>0)
	{
		res << "bool finished;\n";
		res << "pthread_spinlock_t locks[" << num_comms << "];\n";
		res << "pthread_spinlock_t core_locks_s[" << sched->size()-1 << "];\n";
		res << "pthread_spinlock_t core_locks_e[" << sched->size()-1 << "];\n";
		res << "pthread_t threads[" << sched->size()-1 << "];\n";
	}


	res << "unsigned id;\n";
	return res.str();
}


std::string PTHCommunicationSimulation::initializations()
{
	std::stringstream res("");
	res << MCCommunicationSimulation::initializations();
	res << "int err;\n";
	res << "\n";

	if(num_comms>0)
	{
		res << "finished = false;\n";
		res << "for(unsigned i=0;i<" << num_comms << ";++i) {\n";
		res << "pthread_spin_init(&(locks[i]),PTHREAD_PROCESS_SHARED);\n";
		res << "pthread_spin_lock(&(locks[i]));\n";
		res << "}\n";

		res << "for(unsigned i=0;i<" << sched->size()-1 << ";++i) {\n";
		res << "pthread_spin_init(&(core_locks_s[i]),PTHREAD_PROCESS_SHARED);\n";
		res << "pthread_spin_lock(&(core_locks_s[i]));\n";
		res << "pthread_spin_init(&(core_locks_e[i]),PTHREAD_PROCESS_SHARED);\n";
		res << "pthread_spin_lock(&(core_locks_e[i]));\n";
		res << "}\n";

		for(unsigned i=0;i<sched->size()-1;++i)
		{
			res << "err = pthread_create(&(threads["<<i<<"]),NULL,core_cast"<< i+1 << ",(void *)this);\n";
			res << "if(err!=0)\n";
			res << "	std::cout << \"error code: \" << err << \" (creating thread " << i << ")\\n\";\n";
		}

	}
	return res.str();
}



std::string PTHCommunicationSimulation::deinitializations()
{
	std::stringstream res("");

	res << MCCommunicationSimulation::deinitializations();
	if(max_comms>0)
	{
		res << "finished = true;\n";
		res << "for(unsigned i=0;i<" << num_comms << ";++i)\n";
		res << "	pthread_spin_unlock(&(locks[i]));\n";

		res << "for(unsigned i=0;i<" << sched->size()-1 << ";++i) {\n";
		res << "pthread_spin_unlock(&(core_locks_s[i]));\n";
		res << "pthread_spin_unlock(&(core_locks_e[i]));\n";
		res << "}\n";
		for(unsigned i=0;i<sched->size()-1;++i)
			res << "pthread_join((threads[" << i << "]),NULL);\n";
	}

	return res.str();
}

std::string PTHCommunicationSimulation::task_pre_comm(const MCNode * n)
{
	std::stringstream res("");
	unsigned id = n->get_id();
	//Abhängigkeiten empfangen
	if(comms_in[id].size()>0 )
	{

		for(std::list<Communication>::const_iterator it = comms_in[id].begin();it!=comms_in[id].end();++it)
		{

			res << "pthread_spin_lock(&(locks["<<it->comm_id<<"]));\n";
		}
	}

	return res.str();
}

std::string PTHCommunicationSimulation::task_post_comm(const MCNode * n)
{
	std::stringstream res("");
	unsigned id = n->get_id();

	// Abhängigkeiten schicken

		if(comms_out[id].size()>0)
		{
			for(std::list<Communication>::const_iterator it = comms_out[id].begin();it!=comms_out[id].end();++it)
			{

				res << "pthread_spin_unlock(&(locks[" << it->comm_id << "]));\n";
			}
		}


	return res.str();
}



void PTHCommunicationSimulation::init()
{


}


