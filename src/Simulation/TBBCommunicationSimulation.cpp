/*
 * TBBTaskCommunication.cpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */

#include <Simulation/TBBCommunicationSimulation.hpp>

#include <sstream>

TBBCommunicationSimulation::TBBCommunicationSimulation(UnorderedSchedule const * in) : MCCommunicationSimulation(in) {
	init();
}

TBBCommunicationSimulation::~TBBCommunicationSimulation() {
	//delete cluster;
}

std::string TBBCommunicationSimulation::declarations()
{
	std::stringstream res("");
	res << "struct TBB_Body {\n";
	res << "boost::function<void(void)> void_function;\n";
	res << "TBB_Body(boost::function<void(void)> void_function) : void_function(void_function) { }\n";
	res << "void operator()( tbb::flow::continue_msg ) const {\n";
	res << "void_function();\n";
	res << "}\n";
	res << "};\n\n";
	res << "tbb::flow::graph tbb_graph;\n\n";
	res << "tbb::flow::broadcast_node<tbb::flow::continue_msg> * start;\n";
	res << "std::vector<tbb::flow::continue_node<tbb::flow::continue_msg> *> tbb_n;\n\n";
	res << "unsigned id;\n";
	res << MCTaskSimulation::declarations();
	return res.str();
}

std::string TBBCommunicationSimulation::task_pre_comm(const MCNode * n)
{
	return "";
}

std::string TBBCommunicationSimulation::task_post_comm(const MCNode * n)
{
	return "";
}

std::string TBBCommunicationSimulation::initializations()
{
	std::stringstream res("");
	res << "id = num;\n";


	res << MCTaskSimulation::initializations();
	return res.str();
}

std::string TBBCommunicationSimulation::deinitializations()
{
	std::stringstream res("");
	res << "delete start;\n";
	for(stype i=0;i<cluster->size();++i)
	{
		res << "delete (tbb_n[" << i << "]);\n";
		}
	res << MCCommunicationSimulation::deinitializations();
	return res.str();
}

void TBBCommunicationSimulation::init()
{


	find_task_cluster();

}

void TBBCommunicationSimulation::find_task_cluster()
{
	UnorderedSchedule all;
	std::list<const MCNode *> temp;
	for(stype i=0;i<sched->size();++i)
	{
		temp.clear();
		for(const MCNode * n : sched->get_schedule(i))
		{
			if(comms_in[n->get_id()].size() > 0 && temp.size()>0)
			{
				all.push_back(temp);
				temp = std::list<const MCNode *>();
			}
			temp.push_back(n);
			if(comms_out[n->get_id()].size()>0)
			{
				all.push_back(temp);
				temp = std::list<const MCNode *>();
			}
		}
		if(temp.size()>0) all.push_back(temp);
	}
	cluster = new ClusterGraph(all);
}
