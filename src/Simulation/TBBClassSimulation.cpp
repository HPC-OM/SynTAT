/*
 * TBBClassSimulation.cpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */

#include <Simulation/TBBClassSimulation.hpp>


TBBClassSimulation::TBBClassSimulation(UnorderedSchedule const * in, std::string const & name) : MCClassSimulation(in,name) {
	MCSimulation::add_includeable(Includeables::IOSTREAM);
	type = TBB;
	tbbcomm = new TBBCommunicationSimulation(in);
	comm = tbbcomm;
	add_includeable(Includeables::VECTOR);
	add_includeable(Includeables::TBB_LIB);
	add_includeable(Includeables::TBB_GRAPH);
	add_includeable(Includeables::BOOST_BIND);
	add_includeable(Includeables::BOOST_FUNCTION);
}

TBBClassSimulation::~TBBClassSimulation() {
	delete comm;
}

std::string TBBClassSimulation::get_string()
{
	std::stringstream res("");

	res << includes();

	res << generate_class();

	res << main_function();

	return res.str();
}

std::string TBBClassSimulation::main_function()
{
	std::stringstream res("");
	res << "int main(int argc, char *argv[]) {\n";
	res << "unsigned long long start_t1, start_t2, end_t,sum = 0;\n";
	res << "int num_procs = 1, id = 0;\n";
	res << this->name << " inst;\n";
	res << "for(unsigned i=0;i<"<< repeat_count <<";++i) {\n";
	res << "start_t2 = rdtsc();\n";
	res << "inst.execute();\n";
	res << "end_t = rdtsc();\n";
	res << "sum += end_t-start_t2;\n";
	res << "}\n";
	res << "std::cout << \"needed \" << sum/" << repeat_count << " << \" cycles for execution\\n\";\n";
	res << "return 0;\n}\n";
	return res.str();
}

std::string TBBClassSimulation::core_function(unsigned core_num)
{
	std::stringstream res("");
	// no use for TBB
	return res.str();
}

std::string TBBClassSimulation::cpp_core_function(unsigned core_num)
{
	std::stringstream res("");
	// no use for TBB
	return res.str();
}

//TODO only use num cores
std::string TBBClassSimulation::cpp_execute_function()
{
	std::stringstream res("");
	std::vector<bool> is_source(tbbcomm->cluster->size(),true);
	res << "void " << name << "::execute(int id) {\n";

	res << "start->try_put(tbb::flow::continue_msg());\n";
	res << "tbb_graph.wait_for_all();\n";

	res << "}\n\n";

	res << cpp_task_funcs();
	return res.str();
}

std::string TBBClassSimulation::execute_function()
{
	std::stringstream res("");
	std::vector<bool> is_source(tbbcomm->cluster->size(),true);
	res << "void execute(int id = 0);\n\n";

	res << task_funcs();
	return res.str();
}


std::string TBBClassSimulation::task_funcs()
{
	std::stringstream res("");
	for(stype i = 0;i < tbbcomm->cluster->size();++i)
	{
		res << "void task_func" << (*(tbbcomm->cluster))[i].get_id() << "();\n\n";
	}

	return res.str();
}

std::string TBBClassSimulation::cpp_task_funcs()
{
	std::stringstream res("");
	for(stype i = 0;i < tbbcomm->cluster->size();++i)
	{
		res << "void "<< name << "::task_func" << (*(tbbcomm->cluster))[i].get_id() << "() {\n";
		std::list<const MCNode *> temp = (*(tbbcomm->cluster))[i].get_nodes();
		for(const MCNode * n : temp)
		{
			res << comm->task(n);
		}
		res << "}\n\n";
	}

	return res.str();
}

std::string TBBClassSimulation::initializations()
{
	std::stringstream res("");

	res << comm->initializations();

	res << "tbb::task_scheduler_init sched(" << get_sched()->size() << ");\n";
	res << "start = new tbb::flow::broadcast_node<tbb::flow::continue_msg>(tbb_graph);\n";
	res << "tbb_n = std::vector<tbb::flow::continue_node<tbb::flow::continue_msg> *>(" << tbbcomm->cluster->size() << ",NULL);\n";
	for(stype i=0;i<tbbcomm->cluster->size();++i)
	{
		res << "tbb_n[" << i << "] = new tbb::flow::continue_node<tbb::flow::continue_msg>"
			<< "(tbb_graph,TBB_Body(boost::bind<void>(&" << name << "::task_func" << i << ",this)));\n";
	}
	std::vector<bool> is_source(tbbcomm->cluster->size(),true);
	for(auto it = tbbcomm->cluster->link_begin();it!=tbbcomm->cluster->link_end();++it)
	{
		Link l = *it;
		if(l.get_direction())
		{
			res << "tbb::flow::make_edge(*(tbb_n[" << l.c1 << "]),*(tbb_n[" << l.c2 << "]));\n";
			is_source[l.c2] = false;
		}
		else
		{
			res << "tbb::flow::make_edge(*(tbb_n[" << l.c2 << "]),*(tbb_n[" << l.c1 << "]));\n";
			is_source[l.c1] = false;
		}
	}
	stype count = 0;
	for(stype i=0;i<is_source.size();++i)
		if(is_source[i])
		{
			++count;
			res << "tbb::flow::make_edge(*start,*(tbb_n["<<i<<"]));\n";
		}
	if(count == 0)
	{
		std::cout << "TBBClassSimulation says: couldn't assign source_node\n";
		exit(1);
	}

	return res.str();
}

