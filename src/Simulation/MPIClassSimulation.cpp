/*
 * MPIClassSimulation.cpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

#include <Simulation/MPIClassSimulation.hpp>

#include <fstream>

MPIClassSimulation::MPIClassSimulation(UnorderedSchedule const * in, std::string const & name) :  MCClassSimulation(in,name) {
	MCSimulation::add_includeable(Includeables::IOSTREAM);
	add_includeable(Includeables::VECTOR);
	add_includeable(Includeables::MPI_LIB);
	add_includeable(Includeables::FSTREAM);
	add_includeable(Includeables::UTILITY);
	type = MPI;
	comm = new MPICommunicationSimulation(in);
}

MPIClassSimulation::~MPIClassSimulation() {
	delete comm;
}

std::string MPIClassSimulation::get_string()
{
	std::stringstream res("");

	res << includes();

	res << comm->functions();

	res << generate_class();

	res << main_function();

	return res.str();
}

std::string MPIClassSimulation::main_function()
{
	std::stringstream res("");
	res << "int main(int argc, char *argv[]) {\n";
	res << "unsigned long long start_t1, start_t2, end_t,sum = 0;\n";
	res << "int  id;\n";
	res << "MPI_Init(&argc,&argv);\n";
	res << "MPI_Comm_rank(MPI_COMM_WORLD, &id);\n";
	res << "start_t1 = rdtsc();\n";
	res << name << " inst(id);\n";
	res << "for(unsigned i=0;i<"<< repeat_count <<";++i) {\n";
	res << "start_t2 = rdtsc();\n";
	res << "inst.execute();\n";
	res << "end_t = rdtsc();\n";
	res << "sum += end_t-start_t2;\n";
	res << "}\n";
	res << "std::cout << \"needed \" << sum/" << repeat_count << " << \" cycles for execution\\n\";\n";
	res << "MPI_Finalize();\n";
	res << "return 0;\n}";
	return res.str();
}

std::string MPIClassSimulation::core_function(unsigned core_num)
{
	std::stringstream res("");
	res << "void core" << core_num << "();\n\n";
	return res.str();
}

std::string MPIClassSimulation::cpp_core_function(unsigned core_num)
{
	std::stringstream res("");
	res << "void " << name << "::core" << core_num << "() { \n";
	for(const MCNode * n : get_sched()->get_schedule(core_num))
	{
		res << comm->task(n) << "\n";
	}
	if(comm->acc_num_sends[core_num]>0) res << "MPI_Waitall(" << comm->acc_num_sends[core_num] << ",&(out_requ[0]),MPI_STATUSES_IGNORE);\n";
	res << "}\n\n";
	return res.str();
}

std::string MPIClassSimulation::execute_function()
{
	std::stringstream res("");
	res << "void execute();\n\n";
	return res.str();
}

std::string MPIClassSimulation::cpp_execute_function()
{
	std::stringstream res("");
	res << "void " << name << "::execute() {\n";
	res << "switch(id) {\n";
	for(unsigned i=0;i<get_sched()->size();++i)
	{
		res << "case " << i << ":\n";
		res << "core" << i << "();\n";
		res << "break;\n";
	}
	res << "default:\n";
	res << "// nothing;\n";
	res << "break;\n";
	res << "}//switch\n";

	res << "}\n";
	return res.str();
}



