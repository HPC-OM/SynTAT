/*
 * MCSimulation.cpp
 *
 *  Created on: 21.08.2014
 *      Author: marc
 */

#include <Simulation/MCSimulation.hpp>

stype MCSimulation::repeat_count = 10;

MCSimulation::MCSimulation() {
	inc_libs = std::vector<bool>(COUNT,false);
	type = NO_TYPE;
}

MCSimulation::~MCSimulation() {

}

void MCSimulation::add_includeable(Includeables in)
{
	inc_libs[in] = true;
}

void MCSimulation::set_repeat_count(stype count)
{
	repeat_count = count;
}


MCSimulation::SIM_TYPE MCSimulation::get_sim_type() const
{
	return type;
}

std::string MCSimulation::includes()
{
	std::stringstream res("");


	if(inc_libs[MPI_LIB]) res << "#include <mpi.h>\n";
	if(inc_libs[BOOST_MPI_LIB]) res << "#include <boost/mpi.hpp>\n#include <boost/archive/text_oarchive.hpp>\n#include <boost/archive/text_iarchive.hpp>\n#include <boost/serialization/vector.hpp>\n#include <boost/serialization/shared_ptr.hpp>\n#include <boost/serialization/scoped_ptr.hpp>\n#include <boost/shared_ptr.hpp>\n";
	if(inc_libs[TBB_LIB]) res << "#include <tbb/tbb.h>\n#include <tbb/tbb_thread.h>\n";
	if(inc_libs[TBB_GRAPH]) res << "#include <tbb/flow_graph.h>\n";
	if(inc_libs[PTH_LIB]) res << "#include <pthread.h>\n";
	if(inc_libs[OM_LIB]) res << "#include <omp.h>\n";
	if(inc_libs[VECTOR]) res << "#include <vector>\n";
	if(inc_libs[IOSTREAM]) res << "#include <iostream>\n";
	if(inc_libs[FSTREAM]) res << "#include <fstream>\n";
	if(inc_libs[BOOST_BIND]) res << "#include <boost/bind.hpp>\n";
	if(inc_libs[BOOST_FUNCTION]) res << "#include <boost/function.hpp>\n";
	if(inc_libs[UTILITY]) res << "#include <utility>\n";

	return res.str();
}

std::string MCSimulation::ansi_parse(std::string in)
{
	std::stringstream work(in), res("");
	unsigned count = 0;
	char * buffer = new char[1024];
	std::string temp;
	bool k_open, k_close;
	while(work.good())
	{
		work.getline(buffer,1024);
		temp = buffer;
		k_open = temp.find('{') != std::string::npos;
		k_close = temp.find('}') != std::string::npos;

		for(unsigned i=0;i<count;++i)
		{
			res << 	"	";
		}
		res << temp << "\n";
		if(k_open) ++count;
		if(k_close) --count;
	}
	delete[] buffer;
	return res.str();
}
