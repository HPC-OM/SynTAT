/*
 * ExecutionSimulation.cpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */


#include <Simulation/ExecutionSimulation.hpp>

#include <fstream>

ExecutionSimulation::ExecutionSimulation(std::string const & name) : name(name)
{ }

ExecutionSimulation::~ExecutionSimulation() { }

void ExecutionSimulation::toFile(std::string file)
{
	std::ofstream ous;
	ous.open((file + std::string(".cpp")).c_str());
	ous << MCSimulation::ansi_parse(get_string());
	ous.close();
}

void ExecutionSimulation::toFile()
{
	toFile(name);
}


std::string ExecutionSimulation::get_name() const
{
	return name;
}
