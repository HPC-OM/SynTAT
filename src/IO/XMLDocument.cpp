/*
 * XMLDocument.cpp
 *
 *  Created on: 01.08.2014
 *      Author: marc
 */

#include <fstream>
#include <list>

#include <IO/XMLDocument.hpp>

enum  xml_element_type
{
	START, END, NOCONTENT, CONTENT, EMPTY
};

struct xml_element_read
{
	xml_element_type t;
	std::string content;
};

xml_element_read get_single_content(std::ifstream & ins)
{
	char c = ' ';
	xml_element_read res;
	res.t = EMPTY;
	std::string content;

	while(ins.good() && ( c==' ' || c=='\n' ))
	{
		c = ins.get();
	}
	if(!ins.good()) return res;
	content += c;
	if(c!='<')
	{
		res.t = CONTENT;
		while(ins.good() && ins.peek()!= '<')
		{
			content += ins.get();

		}
		if(content.size()>0 && content[0] == ' ') content.erase(0,1);
		if(content.size() > 0 && content[content.size()-1] == ' ') content.erase(content.size()-1,1);
	}
	else
	{
		while(ins.good() && c!='>')
		{
			c = ins.get();
			content += c;
		}
		if(content.size() > 2 && content[2] == '[' && content[1] == '!') // catch <![CDATA[]]>
		{
			res.t = CONTENT;
			size_t pos = content.find("<![CDATA[");
			if(pos != std::string::npos) content.erase(pos,9);
			pos = content.find("]]>");
			if(pos != std::string::npos) content.erase(pos,3);
			if(content.size()>0 && content[0] == ' ') content.erase(0,1);
			if(content[content.size()-1] == ' ') content.erase(content.size()-1,1);
		}
		else if(content[1] == '/')
		{
			res.t = END;

		}
		else if(content[content.size()-2]=='/')
		{
			res.t = NOCONTENT;
		}
		else
		{
			res.t = START;
		}


	}
	res.content = content;
	return res;
}

void XMLDocument::read(std::string const filename)
{
	std::ifstream ins;
	ins.open(filename.c_str());
	if(!ins.good())
	{
		std::cout << "File is not okay ('" << filename << "')\n";
		ins.close();
		return;
	}
	std::list<XMLElement *> open_elems;
	xml_element_read temp;
	temp = get_single_content(ins);
	while(ins.good())
	{
		temp = get_single_content(ins);
		if(temp.content.find("<!--") != std::string::npos) continue;
		switch(temp.t)
		{
		case START:
			elems.push_back(XMLElement(temp.content));
			if(open_elems.size()>0)
			{
				open_elems.back()->add_element(&(elems.back()));
			}
			open_elems.push_back(&(elems.back()));
			break;
		case END:
			open_elems.pop_back();
			break;
		case NOCONTENT:
			elems.push_back(XMLElement(temp.content));
			if(open_elems.size()>0)
			{
				open_elems.back()->add_element(&(elems.back()));
			}
			break;
		case CONTENT:
			open_elems.back()->set_content(temp.content);
			break;
		case EMPTY:
			break;
		}
	}
	root = &(elems.front());
	ins.close();
}

void XMLDocument::recursive_write(std::ofstream & ous, XMLElement * in)
{
	ous << "<" << in->get_name();
	for(auto it = in->attr_begin();it!=in->attr_end();++it)
	{
		ous << " " << it->first << "=\"" << it->second << "\"";
	}
	if(in->element_begin() != in->element_end()) {

		ous << ">\n";
		for(auto it = in->element_begin();it!=in->element_end();++it)
		{
			recursive_write(ous,*it);
		}
		ous << "</" << in->get_name() << ">\n";
	}
	else if(in->get_content() != "")
	{
		ous << ">";
		ous << in->get_content();
		ous << "</" << in->get_name() << ">\n";
	}
	else
	{
		ous << "/>\n";
	}

}

void XMLDocument::write(std::string const filename)
{
	if(root==NULL)
	{
		std::cout << "Couldn't find root element\n";
		return;
	}

	std::ofstream ous;
	ous.open(filename.c_str());
	ous << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
	recursive_write(ous,root);
	ous.close();
}

XMLElement * XMLDocument::get_root()
{
	return root;
}

void XMLDocument::set_root(XMLElement * in)
{
	root = in;
}

unsigned XMLDocument::size() const
{
	return elems.size();
}

std::ostream & operator<<(std::ostream & os, XMLDocument const & xd)
{
	os << *(xd.root);
	return os;
}

std::list<XMLElement>::const_iterator XMLDocument::elements_begin() const
{
	return elems.begin();
}

std::list<XMLElement>::const_iterator XMLDocument::elements_end() const
{
	return elems.end();
}

XMLElement * XMLDocument::add_element(XMLElement const & in)
{
	elems.push_back(in);
	return &(elems.back());
}
