/*
 * XMLElement.cpp
 *
 *  Created on: 01.08.2014
 *      Author: marc
 */

#include <IO/XMLElement.hpp>



std::string XMLElement::get_content() const
{
	return content;
}

std::string XMLElement::get_header() const
{
	return header;
}

void XMLElement::set_content(std::string const & in)
{
	content = in;
}

std::string XMLElement::get_name() const
{
	return name;
}

void XMLElement::set_name(std::string const & in)
{
	name = in;
}

std::string XMLElement::get_attr(std::string const & name) const
{
	std::map<std::string,std::string>::const_iterator it = attr.find(name);
	if(it == attr.end()) return std::string("");
	else return it->second;
}

void XMLElement::add_element(XMLElement * in)
{
	elems.push_back(in);
}

void XMLElement::add_attr(std::string name, std::string val)
{
	attr.insert(std::pair<std::string,std::string>(name,val));
}

bool XMLElement::operator==(XMLElement const & in) const
{
	return false;
}

std::ostream & operator<<(std::ostream & os, XMLElement const & xe)
{
	os << "elem_name: [" << xe.name << "] Atrr: ";
	for(std::map<std::string,std::string>::const_iterator it = xe.attr.begin();it!=xe.attr.end();++it)
	{
		os << "	" << it->first << "=" << it->second << "\n";
	}
	os << " cont: " << xe.content << "\n";
	for(std::list<XMLElement *>::const_iterator it = xe.elems.begin();it!=xe.elems.end();++it)
	{
		os << *(*it);
	}
	return os;
}

void XMLElement::init()
{
	unsigned i;
	for(i=1;i<header.size();++i)
	{
		if(header[i]==' ' || header[i]=='>')
		{
			break;
		}
	}
	name = header.substr(1,i-1);
	unsigned offset = ++i;
	bool value = false;
	bool attname = false;
	bool flag = true;
	std::string kstr = "";
	for(;i<header.size() && flag;++i)
	{
		switch(header[i])
		{
		case '>':
			flag = false;
			break;
		case ' ':

			break;
		case '=':
			attname = false;
			kstr = header.substr(offset,i-offset);
			break;
		case '\"':
			if(value)
			{
				value = false;
				attr.insert(std::pair<std::string,std::string>(kstr,header.substr(offset,i-offset)));
				kstr = "";
			}
			else
			{
				value = true;
				offset = i+1;
			}
			break;
		default:
			if(attname == false && value == false)
			{
				attname = true;
				offset = i;
			}
		}
	}
}

std::map<std::string,std::string>::const_iterator XMLElement::attr_begin() const
{
	return attr.begin();
}

std::map<std::string,std::string>::const_iterator XMLElement::attr_end() const
{
	return attr.end();
}

std::list<XMLElement *>::const_iterator XMLElement::element_begin() const
{
	return elems.begin();
}
std::list<XMLElement *>::const_iterator XMLElement::element_end() const
{
	return elems.end();
}
