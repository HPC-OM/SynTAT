/*
 * GraphMLReader.cpp
 *
 *  Created on: 01.08.2014
 *      Author: marc
 */

#include <IO/GraphMLReader.hpp>

#include <boost/lexical_cast.hpp>
#include <string>
#include <sstream>

void GraphMLReader::set_node_cost_name(std::string const st)
{
	node_cost_name = st;
}

void GraphMLReader::set_node_id_name(std::string const st)
{
	node_id_name = st;
}

SingleLinkGraph GraphMLReader::read(std::string const filename)  const
{
	SingleLinkGraph res;

	std::string node_cost_name = this->node_cost_name;
	std::string node_id_name = this->node_id_name;
	std::string edge_node_ref_name = this->edge_node_ref_name;
	std::string edge_comm_float = this->edge_comm_float;
	std::string edge_comm_int = this->edge_comm_int;
	std::string edge_comm_bool = this->edge_comm_bool;
	std::string node_id_prefix = this->node_id_prefix;

	unsigned node_name_length = 1;

	XMLDocument xmlD;
	xmlD.read(filename);
	unsigned count_nodes = 0;
	// get key mapping
	for(auto it = xmlD.elements_begin();it!=xmlD.elements_end();++it)
	{
		if(it->get_name()=="key")
		{
			if(it->get_attr("for") == "node")
			{
				std::string temp = it->get_attr("attr.name");
				if(temp == node_id_name)
				{
					node_id_name = it->get_attr("id");
				}
				else if(temp == node_cost_name)
				{
					node_cost_name = it->get_attr("id");
				}
			}
			if(it->get_attr("for") == "edge")
			{
				std::string temp = it->get_attr("attr.name");
				if(temp == edge_comm_bool)
				{
					edge_comm_bool = it->get_attr("id");
				}
				else if(temp == edge_comm_float)
				{
					edge_comm_float = it->get_attr("id");
				}
				else if(temp == edge_comm_int)
				{
					edge_comm_int = it->get_attr("id");
				}
			}
		}
		if(it->get_name()=="node")
				++count_nodes;
	}
	// get nodes

	res.new_nodes(count_nodes+1,0);
	unsigned id = 0;
	MCGraph::cost_type costs = 0;
	bool flag = true;
	for(auto it = xmlD.elements_begin();it!=xmlD.elements_end();++it)
	{
		if(it->get_name()=="node")
		{
			if(flag)
			{
				std::string id_string =it->get_attr("id");
				if(id_string[0] == 'n')
					node_name_length = 1;
				else
					node_name_length = 4;
				flag = false;
			}
			for(auto eit = it->element_begin();eit!=it->element_end();++eit)
			{
				if((*eit)->get_name() != "data") continue;

				if((*eit)->get_attr("key") == node_cost_name)
				{
					costs = (*eit)->get<MCGraph::cost_type>();
				}
				else if((*eit)->get_attr("key") == node_id_name)
				{
					id = (*eit)->get<unsigned>();
				}
			}
			res[id]->set_costs(costs);
		}
	}
	std::string str;
	unsigned tar;
	std::vector<bool> is_child(res.size(),false);
	for(auto it = xmlD.elements_begin();it!=xmlD.elements_end();++it)
	{
		if(it->get_name()=="edge")
		{
			str = it->get_attr("source");
			str = str.substr(node_name_length,str.size()-node_name_length);
			id = boost::lexical_cast<unsigned>(str);

			str = it->get_attr("target");
			str = str.substr(node_name_length,str.size()-node_name_length);
			tar = boost::lexical_cast<unsigned>(str);
			res.make_edge(id,tar,32);
			is_child[tar] = true; // needed for finding the source node
			for(auto eit = it->element_begin();eit != it->element_end();++eit)
			{
				if((*eit)->get_name() != "data") continue;

				if((*eit)->get_attr("key") == edge_comm_int)
				{
					res[id]->set_comm_int(tar,(*eit)->get<unsigned>());
				}
				else if((*eit)->get_attr("key") == edge_comm_float)
				{
					res[id]->set_comm_float(tar,(*eit)->get<unsigned>());
				}
				else if((*eit)->get_attr("key") == edge_comm_bool)
				{
					res[id]->set_comm_bool(tar,(*eit)->get<unsigned>());
				}
			}
		}
	}
	//finding source nodes
	std::list<unsigned> sources;
	for(unsigned i=0;i<is_child.size();++i)
	{
		if(!is_child[i])
		{
			sources.push_back(i);
		}
	}
	if(sources.size()==1)
	{
		res.set_first(sources.back());
	}
	else if(sources.size()==0)
	{
		std::cout << "GraphMLReader says: Couldn't set a source node\n";
	}
	else
	{
		unsigned source_node;
		if(!is_child[0] && res[0]->size() == 0)
			source_node = 0;
		else
		{
			source_node = res.new_node(0);
		}
		for(auto it = sources.begin();it!=sources.end();++it)
		{
			if(source_node != *it)
			{
				res.make_edge(source_node,*it,400);
			}
		}
		res.set_first(source_node);
	}
	return res;
}

void GraphMLReader::write(std::string const filename, MCGraph const * g) const
{
	XMLDocument xmlD;
	xmlD.set_root(xmlD.add_element(get_graphml_root()));
	//write keys:
	XMLElement temp;
	temp = get_grapml_key("node","double",node_cost_name,"nat1");
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key("node","int",node_id_name,"nat2");
	xmlD.get_root()->add_element(xmlD.add_element(temp));

	//write graph
	XMLElement * graph = xmlD.add_element(get_graphml_graph());
	xmlD.get_root()->add_element(graph);
	//write nodes:
	XMLElement * node;
	for(unsigned i=0;i<g->size();++i)
	{
		if(g->at(i)->get_costs()==0)
			continue;
		node = xmlD.add_element(get_graphml_node(i));
		graph->add_element(node);

		node->add_element(xmlD.add_element(get_graphml_data<MCNode::cost_type>("nat1",g->at(i)->get_costs())));
		node->add_element(xmlD.add_element(get_graphml_data<unsigned>("nat2",g->at(i)->get_id())));
	}

	//write edges
	for(unsigned i=0;i<g->size();++i)
	{
		if(g->at(i)->get_costs()==0)
			continue;
		for(auto it=g->at(i)->begin();it!=g->at(i)->end();++it)
		{
			graph->add_element(xmlD.add_element(get_graphml_edge(g->at(i),it->first,xmlD)));
		}
	}

	xmlD.write(filename);
}

std::string GraphMLReader::int_to_hex_str(unsigned in)
{
	unsigned i= in%16;
	if(i>9)
	{
		switch(i)
		{
		case(10):
			return "A";
		case(11):
			return "B";
		case(12):
			return "C";
		case(13):
			return "D";
		case(14):
			return "E";
		default:
			return "F";
		}
	}
	else
	{
		std::stringstream ss("");
		ss << in;
		return ss.str();
	}
}

std::string GraphMLReader::get_color(unsigned num, unsigned num_all)
{
	std::stringstream res("");
	unsigned c[3];
	c[0] = 0;
	c[1] = 0;
	c[2] = 0;
	switch(num%7)
	{
	case(0):
			c[0] = 255 - 200 * num/num_all;
			break;
	case(1):
			c[2] = 255 - 200 * num/num_all;
			break;
	case(2):
			c[1] = 255 - 200 * num/num_all;
			break;
	case(3):
			c[0] = 255 - 200 * num/num_all;
			c[2] = 255 - 200 * num/num_all;;
			break;
	case(4):
			c[2] = 255 - 200 * num/num_all;
			c[1] = 255 - 200 * num/num_all;
			break;
	case(5):
			c[0] = 255 - 200 * num/num_all;
			c[1] = 255 - 200 * num/num_all;
			break;
	default:
		c[0] = 255 - 200 * num/num_all;
		c[2] = 255 - 200 * num/num_all;
		c[1] = 255 - 200 * num/num_all;

	}
	res << "#";
	for(unsigned i=0;i<3;++i)
	{
		res << int_to_hex_str(c[i]/16) << int_to_hex_str(c[i]%16);
	}
	return res.str();
}

void GraphMLReader::write(std::string const filename, MCGraph const * g, UnorderedSchedule const & sched) const
{
	XMLDocument xmlD;
	std::vector<stype> task_map = sched.get_task_map();
	xmlD.set_root(xmlD.add_element(get_graphml_root()));
	//write keys:
	XMLElement temp;
	temp = get_grapml_key("node","double",node_cost_name,"cust3");
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key("node","int",node_id_name,"cust4");
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key_yed();
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key("edge","int","CommVarsInt","cust11");
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key("edge","int","CommVarsFloat","cust12");
	xmlD.get_root()->add_element(xmlD.add_element(temp));
	temp = get_grapml_key("edge","int","CommVarsBool","cust13");
	xmlD.get_root()->add_element(xmlD.add_element(temp));

	//write graph
	XMLElement * graph = xmlD.add_element(get_graphml_graph());
	xmlD.get_root()->add_element(graph);

	//write nodes:
	XMLElement * node;
	for(unsigned i=0;i<g->size();++i)
	{
		if(g->at(i)->get_costs()==0)
			continue;
		node = xmlD.add_element(get_graphml_node(i));
		graph->add_element(node);
		node->add_element(xmlD.add_element(get_graphml_data<MCNode::cost_type>("cust3",g->at(i)->get_costs())));
		node->add_element(xmlD.add_element(get_graphml_data<unsigned>("cust4",g->at(i)->get_id())));
		node->add_element(xmlD.add_element(get_graphml_data_yed("gi1",i,task_map[i],sched.size(),g->at(i)->get_costs(),xmlD)));
	}

	//write edges
	for(unsigned i=0;i<g->size();++i)
	{
		if(g->at(i)->get_costs()==0)
			continue;
		for(auto it=g->at(i)->begin();it!=g->at(i)->end();++it)
		{
			graph->add_element(xmlD.add_element(get_graphml_edge(g->at(i),it->first,xmlD)));
		}
	}

	xmlD.write(filename);
}

XMLElement GraphMLReader::get_graphml_root()
{
	XMLElement res;
	res.set_name(std::string("graphml"));
	res.add_attr(std::string("xmlns"),std::string("http://graphml.graphdrawing.org/xmlns"));
	res.add_attr(std::string("xmlns:xsi"),std::string("http://www.w3.org/2001/XMLSchema-instance"));
	res.add_attr(std::string("xmlns:y"),std::string("http://www.yworks.com/xml/graphml"));
	res.add_attr(std::string("xmlns:yed"),std::string("http://www.yworks.com/xml/yed/3"));
	res.add_attr(std::string("xsi:schemaLocation"),std::string("http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd"));
	return res;
}

XMLElement GraphMLReader::get_graphml_graph()
{
	XMLElement res;
	res.set_name("graph");
	res.add_attr("edgedefault", "directed");
	res.add_attr("id","TaskGraph");
	return res;
}

XMLElement GraphMLReader::get_grapml_key(std::string const _for, std::string const _type, std::string const _name, std::string const _id)
{
	XMLElement res;
	res.set_name("key");
	res.add_attr("for",_for);
	res.add_attr("attr.type",_type);
	res.add_attr("attr.name",_name);
	res.add_attr("id",_id);
	return res;
}

XMLElement GraphMLReader::get_grapml_key_yed()
{
	XMLElement res;
	res.set_name("key");
	res.add_attr("for","node");
	res.add_attr("id","gi1");
	res.add_attr("yfiles.type","nodegraphics");
	return res;
}

template<typename value_type>
XMLElement GraphMLReader::get_graphml_data(std::string const _key, value_type val)
{
	XMLElement res;
	std::stringstream ss;
	ss << val;
	res.set_name("data");
	res.add_attr("key",_key);
	res.set_content(ss.str());
	return res;
}


XMLElement GraphMLReader::get_graphml_data_yed(std::string const _key, unsigned node_num, unsigned cluster, unsigned num_clusters, MCNode::cost_type costs, XMLDocument & xmlD)
{
	XMLElement res,yedS, yborder,yfill,ylabel,ylabel2,ylabel3,yshape;
	std::stringstream ss("");
	res.set_name("data");
	res.add_attr("key",_key);
	yedS.set_name("y:ShapeNode");

	yfill.set_name("y:Fill");
	yfill.add_attr("color",get_color(cluster,num_clusters));
	yfill.add_attr("transparent","false");

	yborder.set_name("y:BorderStyle");
	yborder.add_attr("color", "#000000");
	yborder.add_attr("type","line");
	yborder.add_attr("width","1.0");

	ylabel.set_name("y:NodeLabel");
	ylabel.add_attr("alignment","center");
	ylabel.add_attr("autoSizePolicy","content");
	ylabel.add_attr("fontFamily","Dialog");
	ylabel.add_attr("fontSize","12");
	ylabel.add_attr("fontStyle","plain");
	ylabel.add_attr("hasLineColor","false");
	ylabel.add_attr("modelName","internal");
	ylabel.add_attr("modelPosition","c");
	ylabel.add_attr("textColor","#000000");
	ylabel.add_attr("visible","true");
	ss << node_num;
	ylabel.set_content(ss.str());
	ss.str("");

	ylabel2.set_name("y:NodeLabel");
	ylabel2.add_attr("alignment","center");
	ylabel2.add_attr("backgroundColor","#46BED8");
	ylabel2.add_attr("autoSizePolicy","content");
	ylabel2.add_attr("fontFamily","Dialog");
	ylabel2.add_attr("fontSize","12");
	ylabel2.add_attr("fontStyle","bold");
	ylabel2.add_attr("hasLineColor","false");
	ylabel2.add_attr("modelName","corners");
	ylabel2.add_attr("modelPosition","nw");
	ylabel2.add_attr("textColor","#000000");
	ylabel2.add_attr("visible","true");
	ss << cluster;
	ylabel2.set_content(ss.str());
	ss.str("");

	ylabel3.set_name("y:NodeLabel");
	ylabel3.add_attr("alignment","center");
	ylabel3.add_attr("backgroundColor","#FFFF00");
	ylabel3.add_attr("autoSizePolicy","content");
	ylabel3.add_attr("fontFamily","Dialog");
	ylabel3.add_attr("fontSize","12");
	ylabel3.add_attr("fontStyle","bold");
	ylabel3.add_attr("hasLineColor","false");
	ylabel3.add_attr("modelName","corners");
	ylabel3.add_attr("modelPosition","se");
	ylabel3.add_attr("textColor","#000000");
	ylabel3.add_attr("visible","true");
	ss << costs;
	ylabel3.set_content(ss.str());
	ss.str("");


	yshape.set_name("y:Shape");
	yshape.add_attr("type","rectangle");

	yedS.add_element(xmlD.add_element(yfill));
	yedS.add_element(xmlD.add_element(yborder));
	yedS.add_element(xmlD.add_element(ylabel));
	yedS.add_element(xmlD.add_element(ylabel2));
	yedS.add_element(xmlD.add_element(ylabel3));
	yedS.add_element(xmlD.add_element(yshape));

	res.add_element(xmlD.add_element(yedS));

	return res;
}

XMLElement GraphMLReader::get_graphml_node(unsigned id)
{
	XMLElement res;
	std::stringstream ss;
	ss << id;
	res.set_name("node");
	res.add_attr("id", ss.str());
	return res;
}

XMLElement GraphMLReader::get_graphml_edge(const MCNode * n, stype target,XMLDocument & xmlD)
{
	XMLElement res,bdata,fdata,idata;
	std::stringstream ss;
	ss.str("");
	ss << "Edge" << n->get_id() << target;
	res.set_name("edge");
	res.add_attr("id",ss.str());
	ss.str("");
	ss << n->get_id();
	res.add_attr("source",ss.str());
	ss.str("");
	ss << target;
	res.add_attr("target",ss.str());

	idata.set_name("data");
	idata.add_attr("key","cust11");
	ss.str("");
	ss << n->get_comm_int(target);
	idata.set_content(ss.str());

	fdata.set_name("data");
	fdata.add_attr("key","cust12");
	ss.str("");
	ss << n->get_comm_float(target);
	fdata.set_content(ss.str());

	bdata.set_name("data");
	bdata.add_attr("key","cust13");
	ss.str("");
	ss << n->get_comm_bool(target);
	bdata.set_content(ss.str());

	res.add_element(xmlD.add_element(bdata));
	res.add_element(xmlD.add_element(idata));
	res.add_element(xmlD.add_element(fdata));

	return res;
}

