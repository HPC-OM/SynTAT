/*
 * MCGraphAnalyser.cpp
 *
 *  Created on: 03.08.2014
 *      Author: marc
 */




#include <Graph/MCGraphAnalyser.hpp>

#include <set>
#include <map>
#include <vector>
#include <limits>


void MCGraphAnalyser::rec_lvl_calc(stype n, const MCNode * first, std::vector<lvl_type> & res) const
{
	if(n > res[first->get_id()])
		res[first->get_id()] = n;
	else
		return;

	for(auto it = first->begin();it != first->end();++it)
	{
		rec_lvl_calc(n+1,it->second,res);
	}
}

std::vector<MCGraphAnalyser::lvl_type> MCGraphAnalyser::get_lvls() const
{
	std::vector<lvl_type> res(g->size(),0);
	rec_lvl_calc(1,g->get_first(),res);
	return res;
}

void MCGraphAnalyser::rec_toplvl_calc(lvl_type n, const MCNode *first, std::vector<lvl_type> & res) const
{
	if(res[first->get_id()] < n)
		res[first->get_id()] = n;
	for(auto it = first->begin();it != first->end();++it)
	{
		rec_toplvl_calc(n+first->get_costs()+first->get_comm_costs(it->first),it->second,res);
	}
}

std::vector<MCGraphAnalyser::lvl_type> MCGraphAnalyser::get_top_lvls() const
{
	std::vector<lvl_type> res(g->size(),0);
	rec_toplvl_calc(0,g->get_first(),res);
	return res;
}

MCGraphAnalyser::lvl_type MCGraphAnalyser::rec_botlvl_calc(const MCNode * first, std::vector<lvl_type> & res) const
{
	lvl_type tlvl = 0, temp;
	if(res[first->get_id()]!=0)
		return res[first->get_id()];
	for(auto it = first->begin();it != first->end();++it)
	{
		temp = rec_botlvl_calc(it->second,res) + first->get_comm_costs(it->first);
		if(tlvl<temp) tlvl = temp;
 	}
	res[first->get_id()] = tlvl +first->get_costs();
	return res[first->get_id()];
}

std::vector<MCGraphAnalyser::lvl_type> MCGraphAnalyser::get_bot_lvls() const
{
	std::vector<lvl_type> res(g->size(),0);
	rec_botlvl_calc(g->get_first(), res);
	return res;
}


std::vector<MCGraphAnalyser::lvl_type> MCGraphAnalyser::get_comp_costs() const
{
	std::vector<lvl_type> res(g->size(),0);
	rec_botlvl_calc(g->get_first(), res);
	for(stype i=0;i<res.size();++i)
		res[i] += g->at(i)->get_costs();
	return res;
}

std::vector<MCGraphAnalyser::lvl_type> MCGraphAnalyser::get_alaps() const
{
	std::vector<lvl_type> res(g->size(),0);
	rec_botlvl_calc(g->get_first(), res);
	lvl_type cp = crit_path_costs(true);
	for(stype i=0;i<res.size();++i) res[i] = cp - res[i];
	return res;
}

std::vector<double> MCGraphAnalyser::get_parallelity() const
{
	struct mark
	{
		double lvl;
		bool start;

		bool operator<(mark const & in) const
		{
			return lvl<in.lvl || (lvl == in.lvl && start);
		}
	};
	std::vector<double> res(g->size(),0);
	std::vector<lvl_type> tlvls = get_top_lvls();
	std::set<mark> marks;
	for(stype i=0;i<tlvls.size();++i)
	{
		marks.insert({(double)tlvls[i],true});
		marks.insert({(double)g->at(i)->get_costs()+(double)tlvls[i],false});
	}
	std::map<double,int> para_scale;
	int val = 0.0;
	for(auto it = marks.begin();it!=marks.end();++it)
	{
		if(it->start) val += 1;
		else val -= 1;
		para_scale.insert(para_scale.end(),std::pair<double,int>(it->lvl,val));
	}
	double para, temp;
	double start, ende;
	for(stype i=0;i<g->size();++i)
	{
		para = 0.0;
		auto it = para_scale.find(tlvls[i]);
		if(it == para_scale.end()) continue;
		while(true)
		{
			start = it->first;
			temp = it->second;
			++it;
			ende = it->first;
			para += (ende-start)*temp;
			if(it == para_scale.end() || ende >= (double)g->at(i)->get_costs() + (double)tlvls[i])
				break;
		}
		res[i] = para/(double)g->at(i)->get_costs();
	}

	return res;
}

std::set<stype> MCGraphAnalyser::rec_succs_count(const MCNode * first, std::vector<stype> & out) const
{
	std::set<stype> temp_deps,deps;
	if(first->begin()!=first->end())
	{
		for(auto p : *first)
		{
			temp_deps = rec_succs_count(p.second,out);
			temp_deps.insert(p.first);
			deps.insert(temp_deps.begin(),temp_deps.end());
		}

	}
	out[first->get_id()] = deps.size();
	return deps;
}

std::vector<stype> MCGraphAnalyser::transitive_succs_count() const
{
	std::vector<stype> res(g->size(),0);
	rec_succs_count(g->get_first(),res);
	return res;
}

std::set<stype> MCGraphAnalyser::rec_succs(const MCNode * first, std::vector<std::set<stype> > & out) const
{
	std::set<stype> temp_deps,deps;
	if(first->begin()!=first->end())
	{
		for(auto p : *first)
		{
			temp_deps = rec_succs(p.second,out);
			temp_deps.insert(p.first);
			deps.insert(temp_deps.begin(),temp_deps.end());
		}

	}
	out[first->get_id()] = deps;
	return deps;
}

std::vector<std::set<stype> > MCGraphAnalyser::transitive_succs() const
{
	std::vector<std::set<stype> > res(g->size(),std::set<stype>());
	rec_succs(g->get_first(),res);
	return res;
}


MCNode::cost_type MCGraphAnalyser::rec_crit_path(const MCNode * first, std::vector<std::pair<std::list<const MCNode *>,MCGraph::cost_type> > & temp) const
{
	MCNode::cost_type costs = 0, temp_costs;
	const MCNode * max_node = NULL;

	if(temp[first->get_id()].second == 0)
	{

		for(auto p : *first)
		{
			temp_costs = rec_crit_path(p.second,temp)+first->get_comm_costs(p.first);
			if(temp_costs>costs)
			{
				max_node = p.second;
				costs = temp_costs;
			}
		}
		if(max_node != NULL)
		{
			temp[first->get_id()].first = temp[max_node->get_id()].first;
			temp[first->get_id()].first.push_back(max_node);
			temp[first->get_id()].second = temp[max_node->get_id()].second;
		}
		temp[first->get_id()].second += first->get_costs();
	}

	return temp[first->get_id()].second;
}
/*
std::pair<std::list<const MCNode *>,MCGraph::cost_type> MCGraphAnalyser::rec_crit_path(const MCNode * first) const
{
	std::pair<std::list<const MCNode *>,MCGraph::cost_type > r, temp;
	if(first->begin()==first->end())
	{
		r.first.push_back(first);
		r.second = first->get_costs();
		return r;
	}
	r.second = 0;
	for(auto it=first->begin();it!=first->end();++it)
	{
		temp = rec_crit_path(it->second);
		if(temp.second>r.second)
			r = temp;
	}
	r.first.push_back(first);
	r.second += first->get_costs();
	return r;
}*/

std::list<const MCNode *> MCGraphAnalyser::get_crit_path() const
{
	std::pair<std::list<const MCNode *>,MCNode::cost_type> ini(std::list<const MCNode *>(),0);
	std::vector<std::pair<std::list<const MCNode *>,MCGraph::cost_type> > temp(g->size(),ini);
	rec_crit_path(g->get_first(),temp);
	return temp[0].first;
}

MCGraph::cost_type MCGraphAnalyser::crit_path_costs(bool comm) const
{
	MCGraph::cost_type res = 0;
	std::list<const MCNode *> cp = get_crit_path();
	const MCNode * n;
	for(auto it = cp.begin();it != cp.end();)
	{
		n = *it;
		++it;
		res += n->get_costs();
		if(comm)
			res += n->get_comm_costs((*(it))->get_id());
		else
			++it;
	}
	return res;
}

MCNode::cost_type MCGraphAnalyser::get_sum_costs() const
{
	MCNode::cost_type res = 0;
	for(stype i=0;i<g->size();++i)
	{
		res += g->at(i)->get_costs();
	}
	return res;
}

std::vector<AnalysedNode> &  MCGraphAnalyser::get_analysed_nodes(AnalysedNode::Analyse type)
{
	if(aNodes.size() == 0)
	{
		for(stype i=0;i<g->size();++i)
		{
			aNodes.push_back(AnalysedNode(g->at(i)));
		}
	}
	if(!analysed[type])
	{
		analysed[type] = true;
		switch(type)
		{
		case AnalysedNode::LVL:
			{
				std::vector<lvl_type> up = get_lvls();
				for(stype i=0;i<up.size();++i)
					aNodes[i].set_lvl(up[i]);
			}
			break;
		case AnalysedNode::BLVL:
			{
				std::vector<lvl_type> up = get_bot_lvls();
				for(stype i=0;i<up.size();++i)
					aNodes[i].set_blvl(up[i]);
			}
			break;
		case AnalysedNode::TLVL:
			{
				std::vector<lvl_type> up = get_top_lvls();
				for(stype i=0;i<up.size();++i)
					aNodes[i].set_tlvl(up[i]);
			}
			break;
		case AnalysedNode::ALAP:
			{
			std::vector<lvl_type> up = get_alaps();
			for(stype i=0;i<up.size();++i)
				aNodes[i].set_alap(up[i]);
			}
			break;
		case AnalysedNode::COMP_COSTS:
			{
				std::vector<lvl_type> up = get_comp_costs();
				for(stype i=0;i<up.size();++i)
					aNodes[i].set_comp_costs(up[i]);
			}
			break;
		case AnalysedNode::DEP_COUNT:
					{
						std::vector<lvl_type> up = this->transitive_succs_count();
						for(stype i=0;i<up.size();++i)
							aNodes[i].set(AnalysedNode::DEP_COUNT,up[i]);
					}
					break;
		default:
			break;
		}
	}
	return aNodes;
}

MCGraphAnalyser::Statistic MCGraphAnalyser::get_statistic() const
{
	Statistic res;

	res.num_nodes = g->size();
	res.sum_costs = 0;
	res.max_costs = 0;
	res.cp_costs = this->crit_path_costs();

	res.max_parallelity = 0;
	res.min_parallelity = std::numeric_limits<double>::infinity();
	res.average_parallelity = 0;

	res.num_edges = 0;
	res.max_comm_costs = 0;
	res.parallelity = get_parallelity();

	MCNode * node;
	for(stype i=0;i<res.num_nodes;++i)
	{
		node = g->at(i);

		res.sum_costs += node->get_costs();
		if(res.max_costs < node->get_costs())
		{
			res.max_costs = node->get_costs();
			res.max_costs_index = node->get_id();
		}

		res.average_parallelity += res.parallelity[i];
		if(res.parallelity[i] > res.max_parallelity)
			res.max_parallelity = res.parallelity[i];
		if(res.parallelity[i] < res.min_parallelity)
			res.min_parallelity = res.parallelity[i];

		res.num_edges += node->size();
		for(std::pair<stype,const MCNode *> n : *node)
		{
			res.sum_comm_costs += node->get_comm_costs(n.second->get_id());
			if(res.max_comm_costs < node->get_comm_costs(n.second->get_id())) res.max_comm_costs = node->get_comm_costs(n.second->get_id());
		}
	}
	res.max_speedup = (double)res.sum_costs/res.cp_costs;

	res.average_comm_costs = res.sum_comm_costs/res.num_edges;
	res.average_costs = res.sum_costs/res.num_nodes;
	res.average_edges_per_node = (double)res.num_edges/res.num_nodes;
	res.average_parallelity /= res.num_nodes;
	res.calc_comm_costs_ratio = res.sum_costs/res.sum_comm_costs;

	return res;
}

std::ostream & operator<<(std::ostream & in, MCGraphAnalyser::Statistic const & s)
{
			in << "num_nodes=" << s.num_nodes << "\nsum_costs" << s.sum_costs << "\n max_costs=" << s.max_costs
			   << "\n average_costs=" << s.average_costs << "\n num_edges=" << s.num_edges << "\n sum_comm_costs=" << s.sum_comm_costs
			   << "\n max_comm_costs=" << s.max_comm_costs << "\n average_comm_costs=" << s.average_comm_costs
			   << "\n average_edges_per_node=" << s.average_edges_per_node << "\n ccr=" << s.calc_comm_costs_ratio
			   << "\n max_parallelity=" << s.max_parallelity << "\n min_parallelity=" << s.min_parallelity
			   << "\n average_parallelity=" << s.average_parallelity << "\nCP_costs=" << s.cp_costs << "\n max_speedup=" << s.max_speedup << "\n";
			return in;
	}
