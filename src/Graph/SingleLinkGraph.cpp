/*
 * SingleLinkGraph.cpp
 *
 *  Created on: 20.06.2014
 *      Author: marc
 */

#include <Graph/SingleLinkGraph.hpp>



SingleLinkGraph::~SingleLinkGraph() {
	for(stype i=0;i<nodes.size();++i)
		delete nodes[i];
}

void SingleLinkGraph::init()
{
	first = 0;
	edge_counter = 0;
}

void SingleLinkGraph::delete_nodes()
{
	for(unsigned i=0;i<nodes.size();++i)
	{
		std::cout << "deleting node " << i << " of " << nodes.size() << "\n";
		delete nodes[i];
	}
}

void SingleLinkGraph::init(MCGraph const * in)
{
	init();
	new_nodes(in->size(),0);
	unsigned id;
	for(unsigned i=0;i<size();++i)
	{
		const MCNode * old = in->at(i);
		id = old->get_id();
		nodes[id]->set_costs(old->get_costs());
		for(std::pair<stype,const MCNode *> p : *old)
		{
			make_edge(i,p.first,in->at(i)->get_comm_costs(p.first));
			nodes[id]->set_comm_bool(p.first,in->at(id)->get_comm_bool(p.first));
			nodes[id]->set_comm_float(p.first,in->at(id)->get_comm_float(p.first));
			nodes[id]->set_comm_int(p.first,in->at(id)->get_comm_int(p.first));
		}
	}
	first = nodes[in->get_first()->get_id()];
}


bool SingleLinkGraph::make_edge(MCNode * from, MCNode * to, MCNode::communication_type costs)
{
	if(*from == *to)
	{
		//std::cout << "SingleLinkGraph: no edge to same node allowed\n";
		return false;
	}
	if(from->add_node(to, costs))
	{
		++edge_counter;
		return true;
	}
	return false;
}

bool SingleLinkGraph::remove_edge(MCNode * from, MCNode * to)
{
	if(*from == *to) return false;
	if(from->remove_node(to))
	{
		--edge_counter;
		return true;
	}
	return false;
}

void SingleLinkGraph::delete_zero_source_node()
{
	while(first->begin() != first->end())
		first->remove_node(first->begin()->second);
}

unsigned SingleLinkGraph::new_node(cost_type costs)
{
	unsigned res = nodes.size();
	nodes.push_back(new MCNode(res, costs));
	return res;
}

void SingleLinkGraph::new_nodes(unsigned num, cost_type costs)
{
	for(unsigned i=0;i<num;i++) new_node(costs);
}

MCGraph * SingleLinkGraph::copy(MCGraph const * in) const
{
	SingleLinkGraph * res = new SingleLinkGraph();

	return res;
}

SingleLinkGraph & SingleLinkGraph::operator=(SingleLinkGraph const & in)
{
	delete_nodes();
	init(&in);
	return *this;
}


SingleLinkGraph & SingleLinkGraph::operator *=(cost_type sk)
{
	for(unsigned i=0;i<nodes.size();++i)
	{
		nodes[i]->set_costs(nodes[i]->get_costs()*sk);
	}
	return *this;
}

MCGraph *  SingleLinkGraph::travers() const
{
	SingleLinkGraph * res = new SingleLinkGraph();
	res->new_nodes(size(),0);
	for(unsigned i=0;i<nodes.size();++i)
	{
		res->at(i)->set_costs(nodes[i]->get_costs());
		for(auto it : *(nodes[i]))
		{
			res->make_edge(it.first,i,nodes[i]->get_comm_costs(it.first));
		}
	}

	return res;
}

std::pair<stype,stype> SingleLinkGraph::merge(stype s1, stype t1, stype s2, stype t2)
{
	std::pair<stype,stype> res;
	res.first = s2;
	res.second = t1;
	if(!nodes[res.first]->has_edge_with(res.second))
		nodes[res.first]->add_node(nodes[res.second],nodes[s1]->get_comm_costs(t1));
	nodes[res.first]->set_comm_int(res.second,nodes[res.first]->get_comm_int(t2)+nodes[s1]->get_comm_int(t1));
	nodes[res.first]->set_comm_float(res.second,nodes[res.first]->get_comm_float(t2)+nodes[s1]->get_comm_float(t1));
	nodes[res.first]->set_comm_int(res.second,nodes[res.first]->get_comm_int(t2)+nodes[s1]->get_comm_int(t1));
	nodes[s1]->remove_node(nodes[t1]);
	if(t2 != t1)
		nodes[res.first]->remove_node(nodes[t2]);

	return res;
}
