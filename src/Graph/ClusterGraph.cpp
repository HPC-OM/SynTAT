/*
 * ClusterGraph.cpp
 *
 *  Created on: 10.09.2014
 *      Author: marc
 */

#include <Graph/ClusterGraph.hpp>

#include <map>


///////////////////////////////////
//////////////Link/////////////////
///////////////////////////////////

Link::Link(stype const & c1, stype const & c2,stype num, bool correct_direction = false)
{
	this->c1 = c1;
	this->c2 = c2;
	this->num = num;
	direction = correct_direction;
}

Link::Link(stype const & c1, stype const & c2)
{
	this->c1 = c1;
	this->c2 = c2;
	this->num = 0;
	direction = false;
}

stype Link::get_num() const
{
	return num;
}

bool Link::get_direction() const
{
	return direction;
}

void Link::set_num(stype const & in)
{
	num = in;
}

stype Link::operator[](stype index) const
{
	if(index==0) return c1;
	else return c2;
}


void Link::add_num(stype const & in)
{
	num += in;
}

stype & Link::operator[](stype index)
{
	if(index==0) return c1;
	else return c2;
}

///////////////////////////////////
////////////ClusterNode////////////
///////////////////////////////////

ClusterNode::ClusterNode(stype id,MCNode::cost_type costs, std::list<const MCNode *> const & l,std::list<Link>::iterator end, cluster_acc no_cluster) : no_cluster(no_cluster), no_link(end), id(id)
{
	this->costs = costs;
	mcnodes.insert(mcnodes.end(),l.begin(),l.end());
}

stype ClusterNode::get_id() const
{
	return id;
}

stype ClusterNode::size() const
{
	return mcnodes.size();
}

bool ClusterNode::add_link(ClusterNode::link_acc l, cluster_acc c1, cluster_acc self)
{
	if(((*l)[0] != c1->get_id() && (*l)[1] != c1->get_id()) || ((*l)[0] != get_id() && (*l)[1] != get_id())) return false;
	cnodes.insert(std::pair<stype,cluster_acc>(c1->get_id(),c1));
	c1->cnodes.insert(std::pair<stype,cluster_acc>(get_id(),self));

	c1->links.insert(std::pair<stype,link_acc>(get_id(),l));
	links.insert(std::pair<stype,link_acc>(c1->get_id(),l));
	return true;
}

ClusterNode::link_iterator ClusterNode::erase(ClusterNode::link_iterator it)
{
	cnodes[it->first]->private_erase(get_id());
	return private_erase(it);
}

void ClusterNode::erase(stype id)
{
	cnodes[id]->private_erase(get_id());
	private_erase(id);
}


void ClusterNode::private_erase(stype id)
{
	cnodes.erase(id);
	links.erase(id);
}

ClusterNode::link_iterator ClusterNode::private_erase(ClusterNode::link_iterator it)
{
	cnodes.erase(it->first);
	return links.erase((const_link_iterator)it);
}


MCNode::cost_type ClusterNode::get_costs() const
{
	return costs;
}

std::list<const MCNode *> & ClusterNode::get_nodes()
{
	return mcnodes;
}

void ClusterNode::set_costs(MCNode::cost_type in)
{
	costs = in;
}

void ClusterNode::add_nodes(std::list<const MCNode *> const & in)
{
	mcnodes.insert(mcnodes.end(),in.begin(),in.end());
}

ClusterNode::cluster_acc ClusterNode::operator[](stype index)
{
	if(cnodes.find(index)!=cnodes.end()) return cnodes[index];
	else return no_cluster;
}

ClusterNode::cluster_iterator ClusterNode::cluster_begin()
{
	return cnodes.begin();
}

ClusterNode::cluster_iterator ClusterNode::cluster_end()
{
	return cnodes.end();
}

ClusterNode::const_cluster_iterator ClusterNode::cluster_begin() const
{
	return cnodes.begin();
}

ClusterNode::const_cluster_iterator ClusterNode::cluster_end() const
{
	return cnodes.end();
}

ClusterNode::link_iterator ClusterNode::begin()
{
	return links.begin();
}

ClusterNode::const_link_iterator ClusterNode::begin() const
{
	return links.begin();
}

ClusterNode::link_iterator ClusterNode::end()
{
	return links.end();
}

ClusterNode::const_link_iterator ClusterNode::end() const
{
	return links.end();
}


ClusterNode::link_acc ClusterNode::get_link(stype id)
{
	if(links.find(id)!=links.end()) return links[id];
	else return no_link;
}

bool ClusterNode::operator==(const ClusterNode & in) const
{
	return id == in.id;
}

bool ClusterNode::operator<(const ClusterNode & in) const
{
	return get_costs()<in.get_costs() || (get_costs()==in.get_costs() && get_id() < in.get_id());
}

bool ClusterNode::operator>(const ClusterNode & in) const
{
	return get_costs()>in.get_costs() || (get_costs()==in.get_costs() && get_id() > in.get_id());
}

///////////////////////////////////
////////////ClusterGraph///////////
///////////////////////////////////


ClusterGraph::ClusterGraph(UnorderedSchedule const & in) {

	init(in);
}

ClusterGraph::~ClusterGraph() {
	std::cout << "CG destr\n";
}


void ClusterGraph::init(UnorderedSchedule const & in)
{

	for(stype i=0;i<in.size();++i)
	{
		std::list<ClusterNode>::iterator it = nodes.insert(nodes.end(),ClusterNode(nodes.size(),in[i].get_costs(), in[i],links.end(),nodes.end()));
		vnodes.push_back(it);
	}
	std::vector<std::vector<std::pair<stype,bool> > > adjmat = in.get_adj_matrix();

	for(stype i=0;i<adjmat.size();++i)
	{
		for(stype j=i+1;j<adjmat.size();++j)
		{
			if(adjmat[i][j].first==0) continue;
			 auto it = links.insert(links.end(),Link(i,j,adjmat[i][j].first,adjmat[i][j].second));
			 vnodes[i]->add_link(it,vnodes[j],vnodes[i]);
		}
	}
}

ClusterNode & ClusterGraph::operator[](stype index)
{
	return *(vnodes[index]);
}

ClusterNode ClusterGraph::operator[](stype index) const
{
	return *(vnodes[index]);
}

stype ClusterGraph::size() const
{
	return nodes.size();
}

double density(stype num_links, MCNode::cost_type size1, MCNode::cost_type size2)
{
	double size;
	if(size1>size2) size = size2;
	else size = size1;
	return (double)(num_links*700)/size;
}


std::list<Link>::iterator ClusterGraph::get_most_dense_link()
{
	double dense = 0,temp;
	std::map<stype,link_acc> conn_count;
	std::list<Link>::iterator res;
	std::vector<stype> conn = get_connected_clusters();
	for(link_acc it = links.begin();it!=links.end();++it)
	{
		if(conn_count.find((*it)[0]) == conn_count.end()) conn_count[(*it)[0]] = it;
		else conn_count[(*it)[0]] = links.end();
		if(conn_count.find((*it)[1]) == conn_count.end()) conn_count[(*it)[1]] = it;
		else conn_count[(*it)[1]] = links.end();

		MCNode::cost_type size1 = vnodes[(*it)[0]]->get_costs();
		MCNode::cost_type size2 = vnodes[(*it)[1]]->get_costs();
		stype nums = (*it).get_num();
		if((temp = density(nums,size1,size2))>dense)
		{
			dense = temp;
			res = it;
		}
	}
	for(std::pair<stype,link_acc> p : conn_count)
	{
		if(p.second != links.end())
		{
			res = p.second;
			break;
		}
	};
	return res;
}

stype ClusterGraph::num_links() const
{
	return links.size();
}

stype ClusterGraph::merge(std::list<Link>::iterator l)
{
	return merge((*l)[0],(*l)[1]);
}

stype ClusterGraph::merge(stype n1, stype n2)
{
	cluster_acc a = vnodes[n1], b = vnodes[n2];
	//merging costs
	a->set_costs(a->get_costs()+b->get_costs());
	a->add_nodes(b->get_nodes());
	// erase link between two clusters

	if(a->get_link(b->get_id()) != links.end())
	{
		links.erase(a->get_link(b->get_id()));
		b->erase(a->get_id());
	}

	for(auto it = b->begin();it!=b->end();)
	{
		if(a->get_link(it->first)!=links.end()) // zwei kanten müssen zusammengeführt werden
		{
			a->get_link(it->first)->add_num(it->second->get_num());
			links.erase(it->second);
			it = b->erase(it);
		}
		else //kante kann umgeschrieben werden
		{
			if((*(it->second))[0]==it->first) (*(it->second))[1] = a->get_id();
			else (*(it->second))[0] = a->get_id();
			a->add_link(it->second,(*b)[it->first],a);
			it = b->erase(it);
		}
	}
	vnodes[b->get_id()] = nodes.end();
	nodes.erase(b);
	return a->get_id();
}

UnorderedSchedule ClusterGraph::get_unordered_schedule() const
{
	UnorderedSchedule res(nodes.size());
	stype i=0;
	for(auto it : nodes)
	{
		res[i].insert(it.get_nodes());
		++i;
	}
	return res;
}


std::vector<stype> ClusterGraph::get_isolated_clusters() const
{
	std::vector<stype> res;
	for(auto it = nodes.begin();it!=nodes.end();++it)
		if(it->size()==0)
			res.push_back(it->get_id());
	return res;
}

std::vector<stype> ClusterGraph::get_connected_clusters() const
{
	std::vector<stype> res;
	for(auto it = nodes.begin();it!=nodes.end();++it)
		if(it->size()>0)
			res.push_back(it->get_id());
	return res;

}

std::vector<stype> ClusterGraph::get_cluster_nums() const
{
	std::vector<stype> res;
		for(auto it = nodes.begin();it!=nodes.end();++it)
				res.push_back(it->get_id());
		return res;
}

void ClusterGraph::sort_nodes()
{
	nodes.sort();
}

std::list<Link>::const_iterator ClusterGraph::link_begin() const
{
	return links.begin();
}

std::list<Link>::const_iterator ClusterGraph::link_end() const
{
	return links.end();
}

std::list<Link>::iterator ClusterGraph::link_begin()
{
	return links.begin();
}

std::list<Link>::iterator ClusterGraph::link_end()
{
	return links.end();
}

