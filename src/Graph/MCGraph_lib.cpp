/*
 * MCGraph_lib.cpp
 *
 *  Created on: 11.07.2014
 *      Author: marc
 */

#include <Graph/MCGraph_lib.hpp>
#include <Graph/MCGraphAnalyser.hpp>
#include <IO/GraphMLReader.hpp>
#include <limits>
#include <cmath>

/**
 * \brief Calculates makespan through adding comm-nodes
 */
MCNode::cost_type imp_makespan(MCGraph const * g, MPIParameter const & sp, UnorderedSchedule const & sched, bool recv)
{
	SingleLinkGraph sg;
	MCNode * newNode, * commLast, * commFirst;
	std::vector<stype> task_map = sched.get_task_map();
	sg.new_nodes(g->size(),0);
	sg.set_first(0u);
	//a list per node contain conn with p.first (p.second==true => incoming, ==false outgoing)
	std::vector<std::list<std::pair<unsigned,std::pair<bool, unsigned> > > > conns(g->size(),std::list<std::pair<unsigned,std::pair<bool, unsigned> > >());
	std::vector<unsigned> preds(g->size(),g->size());
	unsigned tmp_pred;
	unsigned edges = 0, in_comms;
	for(unsigned i=0; i < sched.size() ;++i)
	{
		tmp_pred = g->size();
		for(auto n : sched[i])
		{
			if( tmp_pred < g->size() )
				preds[n->get_id()] = tmp_pred;
			tmp_pred = n->get_id();
			for(auto p : *n)
			{
				if(task_map[p.first] != task_map[n->get_id()])
				{
					conns[p.first].push_back(std::pair<unsigned,std::pair<bool, unsigned> >(n->get_id(),std::pair<bool,unsigned>(true,edges)));
					conns[n->get_id()].push_back(std::pair<unsigned,std::pair<bool, unsigned> >(p.first,std::pair<bool,unsigned>(false,edges++)));
				}
			}
		}
	}

	std::vector<std::pair<unsigned,unsigned> > comm_nodes(edges,std::pair<unsigned,unsigned>(0,0)),
											   start_end_nodes(g->size(),std::pair<unsigned,unsigned>(0,0));

	for(unsigned i=0;i<g->size();++i)
	{
		newNode = sg.at(i);
		commFirst = newNode;
		commLast = newNode;
		in_comms = 0;


		for(auto p : conns[i])
		{
			if(comm_nodes[p.second.second].second==0)
			{
				comm_nodes[p.second.second] = std::pair<unsigned,unsigned>(sg.new_node(sp.get_send_costs(1)),sg.new_node(sp.get_recv_costs(1)));
				sg.at(comm_nodes[p.second.second].first)->add_node(sg.at(comm_nodes[p.second.second].second),sp.get_latency());
			}
			std::pair<unsigned,unsigned> cnodes = comm_nodes[p.second.second];
			if(p.second.first) //incoming
			{
				sg.at(cnodes.second)->add_node(commFirst);
				commFirst = sg.at(cnodes.second);
				++in_comms;
			}
			else //outgoing
			{
				commLast->add_node(sg.at(cnodes.first));
				commLast = sg.at(cnodes.first);
			}
		}
		newNode->set_costs(g->at(i)->get_costs()+sp.get_wait(in_comms));
		start_end_nodes[i].first = commFirst->get_id();
		start_end_nodes[i].second = commLast->get_id();

	}
	for(unsigned i=0;i<g->size();++i)
	{
		if(preds[i]<g->size())
			sg.at(start_end_nodes[preds[i]].second)->add_node(sg.at(start_end_nodes[i].first));
	}
	sg.set_first(0u);
    return get_crit_costs(&sg,true);

}

MCNode::cost_type find_crit_path(const MCNode * first, std::list<unsigned> & rlist,bool comm)
{
	MCGraph::cost_type res, temp;
	std::list<unsigned> list_temp;
	unsigned node_to_add;
	if(first->begin() == first->end())
	{
		res = first->get_costs();
		return res;
	}

	MCNode::node_list::const_iterator it = first->begin();
	res = find_crit_path(it->second,rlist);

	node_to_add = it->second->get_id();
	++it;
	for(;it!=first->end();++it)
	{
		temp = find_crit_path(it->second,list_temp);
		if(comm)
			temp += first->get_comm_costs(it->first);
		if(temp>res)
		{
			node_to_add = it->second->get_id();
			rlist = list_temp;
			res = temp;
		}
	}
	rlist.push_front(node_to_add);
	return res + first->get_costs();
}

MCNode::cost_type iter_crit_costs(MCGraph const * g, bool comm)
{
	MCNode::cost_type res = 0,temp;
	std::vector<MCNode::cost_type> vec(g->size(),0);
	std::list<const MCNode *> l = topological_order(g);

	for(auto n : l)
	{
		if(vec[n->get_id()]==0)
			vec[n->get_id()] += n->get_costs();
		for(auto p : *n)
		{
			temp = vec[n->get_id()];
			if(comm)
				temp += n->get_comm_costs(p.first);
			temp += p.second->get_costs();
			if(temp > vec[p.first])
			{
				vec[p.first] = temp;
				if(temp>res)
					res = temp;
			}
		}
	}

	return res;
}

std::list<const MCNode *> topological_order(MCGraph const * g)
{
	std::list<const MCNode *> res;
	res.push_back(g->get_first());
	auto it = res.begin();
	while(it!=res.end())
	{
		for(auto p : *(*it))
		{
			res.push_back(p.second);
		}
		++it;
	}
	return res;
}

MCNode::cost_type get_crit_costs(const MCGraph * g,bool comm)
{
	std::vector<MCNode::cost_type> temp = get_blvls(g,comm);
	MCNode::cost_type max= 0;
	for(stype i=0;i<temp.size();++i)
	{
		if(max < temp[i])
			max = temp[i];
	}
	return max;
}

MCNode::cost_type get_sum_costs(const MCGraph * g)
{
	MCNode::cost_type sum = 0;
	for(stype i=0;i<g->size();++i)
	{
		sum += g->at(i)->get_costs();
	}
	return sum;
}

MCNode::cost_type get_sum_comm_costs(const MCGraph * g)
{
	MCNode::cost_type sum = 0;
	for(stype i=0;i<g->size();++i)
	{
		for(auto p : *(g->at(i)))
			sum += g->at(i)->get_comm_costs(p.first);
	}
	return sum;
}

std::vector<MCGraph::cost_type> calculate_alap(const MCGraph * g)
{
	std::vector<MCGraph::cost_type> res(g->size(),0);
	rec_calculate_alap(g->get_first(),res);
	MCNode::cost_type max = res[0];
	for(stype i = 1; i< res.size();++i)
	{
		if(res[i] > max) max = res[i];
	}
	for(stype i = 0; i< res.size();++i)
		res[i] = max-res[i];
	return res;
}

MCGraph::cost_type rec_calculate_alap(const MCNode * n, std::vector<MCGraph::cost_type> & l)
{
	MCGraph::cost_type res = 0, temp;
	for(std::pair<stype,const MCNode *> p : *n)
	{
		temp = rec_calculate_alap(p.second,l)+n->get_comm_costs(p.first);
		if(temp>res) res = temp;
	}
	res += n->get_costs();
	l[n->get_id()] = res;
	return res;
}

std::vector<MCGraph::cost_type> calculate_asap(const MCGraph * g)
{
	std::vector<MCGraph::cost_type> res  = get_blvls(g);
	MCGraph::cost_type cp = get_crit_costs(g);
	for(stype i=0;i<g->size();++i)
		res[i] = cp-res[i];
	return res;
}

void rec_calculate_asap(const MCNode * n,std::vector<MCGraph::cost_type> & l, std::vector<stype> & deps)
{
	MCNode::cost_type ctemp, per_comm = 300;
	if(deps[n->get_id()] <= 1)
	{
		MCGraph::cost_type temp;
		for(auto p : *n)
		{
			ctemp = 0;
			if(n->get_comm_costs(p.first)!=0){
				if(n->get_comm_bool(p.first)>0) ctemp += per_comm;
				if(n->get_comm_float(p.first)>0) ctemp += per_comm;
				if(n->get_comm_int(p.first)>0) ctemp += per_comm;
			}
			temp = l[n->get_id()] + ctemp + n->get_costs();
			if(l[p.first]<temp)
				l[p.first] = temp;
		}
		for(auto p : *n)
			rec_calculate_asap(p.second,l,deps);
	}
	else
		--deps[n->get_id()];

}

std::vector<MCNode::cost_type> top_order_blvl(std::vector<const MCNode *> const & in)
{
	std::vector<MCNode::cost_type> res(in.size(),0);
	for(unsigned i=0;i<res.size();++i)
		for(auto p : *in[i])
			++res[p.first];
	return res;
}

MCNode::cost_type rec_blvl(const MCNode * n, std::vector<MCNode::cost_type> & in, bool comm)
{
	MCNode::cost_type res = 0,temp;
	for(auto p : *n)
	{
		if(in[p.first] == std::numeric_limits<MCNode::cost_type>::max())
		{
			temp = rec_blvl(p.second,in);
			if(comm)
				temp += n->get_comm_costs(p.first);
		}
		else
			temp = in[p.first];
		if(res < temp)
		{
			res = temp;
		}
	}
	in[n->get_id()] = res + n->get_costs();
	return in[n->get_id()];
}

std::vector<MCNode::cost_type> get_blvls(const MCGraph * g, bool comm)
{
	std::vector<MCNode::cost_type> res(g->size(),std::numeric_limits<MCNode::cost_type>::max());

	rec_blvl(g->get_first(),res,comm);
	return res;
}

void lvl_order(const MCNode * n,std::list<const MCNode *> & in, std::vector<bool> & vis)
{
	if(vis[n->get_id()])
		return;
	vis[n->get_id()] = true;
	for(auto p : *n)
	{
		in.push_back(p.second);
	}
	for(auto p : *n)
	{
		lvl_order(p.second,in,vis);
	}
}

std::vector<MCNode::cost_type> get_task_waits(MCGraph const * g, UnorderedSchedule const & sched,MPIParameter const * sp)
{

	std::vector<MCNode::cost_type> res(g->size(),0), st = res;

	if(sched.size()==1)
		return res;
	std::vector<std::vector<const MCNode *> > vec(sched.size(),std::vector<const MCNode *>());
	MCGraph * tg = g->travers();
	for(stype i=0;i<vec.size();++i)
	{
	//	vec[i] = std::vector<const MCNode *>(sched[i].size(),NULL);
		for(const MCNode * n : sched[i])
			vec[i].push_back(n);
	}

	std::vector<stype> index(sched.size(),0), deps = find_dependencies(g), tm = sched.get_task_map(), recvs = deps, sends(g->size(),0);
	stype cur = 0, last =0, counter = 0;
	MCNode::cost_type max_c,temp;
	const MCNode * n;
	if(g->at(0)->get_costs()==0)
	{
		for(auto p : *(g->at(0)))
		{
			recvs[p.first] = 0;
		}
	}
	while(true)
	{
		if(index[cur]<sched[cur].size() && deps[(vec[cur][index[cur]]->get_id())]==0)
		{
			n = vec[cur][(index[cur])];
			++counter;
			max_c = 0;
			for(auto p : *(tg->at(n->get_id())))
			{
				temp = st[p.first];
				if(tm[p.first]!=tm[n->get_id()])
					temp += sp->get_latency();
				else
					if(g->at(p.first)->get_costs() != 0)
						--recvs[n->get_id()];
				if(temp>max_c)
					max_c=temp;
			}

			for(auto p : *n)
			{
				--deps[p.first];
				//if(n->get_costs()==0)
					//--recvs[p.first];
				if(tm[p.first] != tm[n->get_id()] && n->get_costs()!=0)
					++sends[n->get_id()];
			}
			if(index[cur]!=0)
			{

				if(max_c>st[index[cur]-1])
				{
					res[n->get_id()] = max_c-st[index[cur]-1];
					st[n->get_id()] = max_c;
				}
				else
				{
					st[n->get_id()] = st[index[cur]-1];
				}
			}
			st[n->get_id()] += n->get_costs() + sp->get_in_costs(recvs[n->get_id()]) + sp->get_out_costs(sends[n->get_id()]);

			index[cur] += 1;
			last = cur;
		}
		else
		{
			bool flag = false;
			for(stype i=0;i<sched.size();++i)
				if(index[i]<sched[i].size())
				{
					flag = true;
					break;
				}
			if(cur==last && !flag)
			{
				flag = false;
				break;
			}
			cur = (cur+1)%sched.size();
		}
	}
	return res;
}

std::vector<MCNode::cost_type> get_tlvls(const MCGraph * g, bool comm)
{
	std::vector<MCNode::cost_type> res(g->size(),0);
	std::cout << "get_tlvls not impl\n";
	exit(1);
	return res;
}

std::vector<stype> get_lvls(MCGraph const * g)
{
	std::vector<stype> res(g->size(),0);
	rec_calculate_lvl(g->get_first(),res);
	return res;
}

void rec_calculate_lvl(const MCNode * n, std::vector<stype> & lvls)
{
	for(auto p : *n)
	{
		if(lvls[n->get_id()]+1 > lvls[p.first])
		{
			lvls[p.first] = lvls[n->get_id()]+1;
			rec_calculate_lvl(p.second, lvls);
		}
	}
}

std::vector<stype> get_travers_lvls(MCGraph const * g)
{
	std::vector<stype> res = get_lvls(g);
	stype maxi = 0;
	for(stype i=0;i<res.size();++i)
		if(res[i]>maxi)
			maxi = res[i];
	for(stype i=0;i<res.size();++i)
		if(res[i]<maxi && g->at(i)->size()>0)
			res[i] = std::numeric_limits<stype>::max();
	rec_calculate_travers_lvl(g->get_first(),res);
	return res;
}


stype rec_calculate_travers_lvl(const MCNode * n, std::vector<stype> & lvls)
{
	if(lvls[n->get_id()]==std::numeric_limits<stype>::max())
	{
		for(auto p : *n)
		{
			if(rec_calculate_travers_lvl(p.second,lvls)<=lvls[n->get_id()])
			{
				lvls[n->get_id()] = lvls[p.first]-1;
			}
		}
	}
	return lvls[n->get_id()];
}


void lvl_sched_ana(const MCGraph * g,stype cores)
{
	MCNode::cost_type overhead = 100;
	stype num_cores = cores;
	std::vector<stype> lvls(g->size(),0);

	rec_calculate_lvl(g->get_first(), lvls);
	stype max_lvl = 0;

	for(stype i=0;i<lvls.size();++i)
		if(lvls[i]>max_lvl)
		{
			max_lvl = lvls[i];
		}
	++max_lvl;
	std::vector<std::list<const MCNode *> > lvl_to_node(max_lvl,std::list<const MCNode *>());
	std::vector<MCNode::cost_type> lvl_costs(max_lvl,0);
	std::vector<MCNode::cost_type> max_task(max_lvl,0);
	for(unsigned i=0;i<lvls.size();++i)
	{
		lvl_to_node[lvls[i]].push_back(g->at(i));
		lvl_costs[lvls[i]] += g->at(i)->get_costs();
		if(max_task[lvls[i]] < g->at(i)->get_costs())
			max_task[lvls[i]] = g->at(i)->get_costs();
	}
	MCNode::cost_type sum = 0, ave_c, min_c = lvl_costs[1], max_c = lvl_costs[1], para_costs = 0, min_para = 0;
	for(stype i=1;i<lvl_costs.size();++i)
	{
		lvl_costs[i] += lvl_to_node[i].size() * overhead;
		sum += lvl_costs[i];
		std::cout << "lvl " << i << " costs: " << lvl_costs[i] << " num_tasks: " << lvl_to_node[i].size() << " max_task: " << max_task[i]  << "\n";
		if(lvl_costs[i]>max_c)
			max_c = lvl_costs[i];
		if(lvl_costs[i]<min_c)
			min_c =lvl_costs[i];
		para_costs += std::max(lvl_costs[i]/num_cores,max_task[i]) + overhead;
		min_para += max_task[i] + overhead;
	}
	ave_c = sum/(lvl_costs.size()-1);
	std::cout << "Level Scheduling Analyse (estimated overhead between level: " << overhead << "):\n" << "average lvlcost: " << ave_c << " \nmin lvlcost: " << min_c << "\nmax lvlcost: " << max_c
			<< "\npara_t " << num_cores << " cores: " << para_costs << " sp: " << (double)sum/para_costs
			<< "\nmin para_t: " << min_para << " sp: " << (double)sum/min_para << "\n\n";


}

double max_sp_lvl(MCGraph const * g)
{
	return max_sp_lvl(g,std::numeric_limits<MCNode::cost_type>::max());
}

double max_sp_lvl_fixed(MCGraph const * g)
{
	return max_sp_lvl_fixed(g,std::numeric_limits<MCNode::cost_type>::max());
}

double max_sp_lvl(MCGraph const * g, stype num_cores, MCNode::cost_type overhead)
{
	std::vector<stype> lvls = get_travers_lvls(g), trlvls = get_travers_lvls(g);
	//rec_calculate_lvl(g->get_first(), lvls);
	stype max_lvl = 0;

	for(stype i=0;i<lvls.size();++i)
		if(lvls[i]>max_lvl)
		{
			max_lvl = lvls[i];
		}
	++max_lvl;
	std::vector<std::list<const MCNode *> > lvl_to_node(max_lvl,std::list<const MCNode *>());
	std::vector<MCNode::cost_type> lvl_costs(max_lvl,0);
	std::vector<MCNode::cost_type> max_task(max_lvl,0);
	for(unsigned i=0;i<lvls.size();++i)
	{
		lvl_to_node[lvls[i]].push_back(g->at(i));
		lvl_costs[lvls[i]] += g->at(i)->get_costs();
		if(max_task[lvls[i]] < g->at(i)->get_costs())
			max_task[lvls[i]] = g->at(i)->get_costs();
	}
	std::vector<stype> fixed(max_lvl,0);
	for(unsigned i=0;i<lvls.size();++i)
	{
		if(trlvls[i]==lvls[i])
		{
			++fixed[lvls[i]];
		}
	}
	MCNode::cost_type sum = 0, min_c = lvl_costs[1], max_c = lvl_costs[1], para_costs = 0;
	for(stype i=1;i<lvl_costs.size();++i)
	{
		sum += lvl_costs[i];
		if(lvl_costs[i]>max_c)
			max_c = lvl_costs[i];
		if(lvl_costs[i]<min_c)
			min_c =lvl_costs[i];
		para_costs += std::max(lvl_costs[i]/num_cores,max_task[i]) + overhead;
	}
	return (double)sum/para_costs;
}

stype max_cores_lvl(MCGraph const * g)
{
	stype i=1;
	double sp = 1 ,sp_last = 0;
	while(i<g->size() && sp>sp_last)
	{
		++i;
		sp_last = sp;
		sp = max_sp_lvl(g,i,0);
	}
	return i;
}

double max_sp_lvl_fixed(MCGraph const * g, stype num_cores, MCNode::cost_type overhead)
{
	std::vector<stype> lvls = get_lvls(g), trlvls = get_travers_lvls(g);
	//rec_calculate_lvl(g->get_first(), lvls);
	stype max_lvl = 0;

	for(stype i=0;i<lvls.size();++i)
		if(lvls[i]>max_lvl)
		{
			max_lvl = lvls[i];
		}
	++max_lvl;
	std::vector<std::list<const MCNode *> > lvl_to_node(max_lvl,std::list<const MCNode *>());
	std::vector<MCNode::cost_type> lvl_costs(max_lvl,0);
	std::vector<MCNode::cost_type> max_task(max_lvl,0);
	for(unsigned i=0;i<lvls.size();++i)
	{
		lvl_to_node[lvls[i]].push_back(g->at(i));
		lvl_costs[lvls[i]] += g->at(i)->get_costs();
	}
	std::vector<stype> fixed(max_lvl,0);
	for(unsigned i=0;i<lvls.size();++i)
	{
		if(max_task[lvls[i]] < g->at(i)->get_costs() && lvls[i] == trlvls[i])
			max_task[lvls[i]] = g->at(i)->get_costs();
		if(trlvls[i]==lvls[i])
		{
			++fixed[lvls[i]];
		}
	}
	MCNode::cost_type sum = 0, min_c = lvl_costs[1], max_c = lvl_costs[1], para_costs = 0;
	for(stype i=1;i<lvl_costs.size();++i)
	{
		sum += lvl_costs[i];
		if(lvl_costs[i]>max_c)
			max_c = lvl_costs[i];
		if(lvl_costs[i]<min_c)
			min_c =lvl_costs[i];
		para_costs += std::max(lvl_costs[i]/num_cores,max_task[i]) + overhead;
	}
	return (double)sum/para_costs;
}

std::vector<stype> find_dependencies(const MCGraph * g)
{
	std::vector<stype> res(g->size(),0);
	for(stype i=0;i<res.size();++i)
	{
			for(auto p : *(g->at(i)))
			{
				++res[p.first];
			}
	}
	return res;
}



void paint_cp(MCGraph const * g, MPIParameter * sp, UnorderedSchedule const & sched)
{
	SingleLinkGraph sg(g);
	std::vector<stype> sends(g->size()), recvs(g->size()), tm = sched.get_task_map();
	for(stype i=0;i<sched.size();++i)
	{
		for(const MCNode * n : sched[i])
		{
			for(auto p : *n)
			{
				if(tm[p.first] == i)
					continue;
				++sends[n->get_id()];
				++recvs[p.first];
			}
		}
	}
	for(stype i=0;i<g->size();++i)
	{
		if(sg.at(i)->get_costs()==0)
			continue;
		sg.at(i)->set_costs(g->at(i)->get_costs() + sp->get_in_costs(recvs[i]) + sp->get_out_costs(sends[i]));
	}
}

MCNode::cost_type makespan(MCGraph const * g, MPIParameter const & sp, UnorderedSchedule const & sched)
{
	SingleLinkGraph sg(g);
	std::vector<stype> sends(g->size()), recvs(g->size()), tm = sched.get_task_map();
	std::list<std::pair<stype,stype> > rm_edge, mk_edge;
	const MCNode * last;
	for(stype i=0;i<sched.size();++i)
	{
		last = NULL;
		for(const MCNode * n : sched[i])
		{
			for(auto p : *n)
			{
				if(tm[p.first] == i)
				{
					rm_edge.push_back({n->get_id(),p.first});
				}
				else
				{
					++sends[n->get_id()];
					++recvs[p.first];
				}
			}

			if(last != NULL)
			{
				mk_edge.push_back({last->get_id(),n->get_id()});
			}
			last = n;
		}
	}
	for(auto p : rm_edge)
	{
		sg.remove_edge(p.first,p.second);
	}
	for(auto p : mk_edge)
	{
		sg.make_edge(p.first,p.second,0);
	}
	bool flag = false;
	for(stype i=0;i<sg.size();++i)
	{
		flag = false;
		MCNode * n = sg.at(i);

		if(n->get_costs() == 0)
			flag = true;
		if(!flag)
		{
			n->set_costs(g->at(i)->get_costs() + sp.get_in_costs(recvs[i]) + sp.get_out_costs(sends[i]));
		}
		for(auto p : *n)
		{
			if(tm[p.first] == tm[i] || flag)
			{
				sg.at(n->get_id())->set_comm_costs(p.first,0);
			}
			else
			{
				sg.at(n->get_id())->set_comm_costs(p.first,sp.get_latency());
			}
		}

	}
	GraphMLReader r;
	//r.write("mak.graphml",&sg,sched);
	//exit(0);
	MCGraphAnalyser ga(&sg,&sp);

	//return ga.crit_path_costs(true);
	return get_crit_costs(&sg,true);
}


std::pair<double,stype> edge_reduction_esti(MCGraph const * g, MPIParameter * sp, stype cores)
{
	std::pair<double,stype> res;

	res.first = 0;
	stype m = g->num_edges();
	stype n = g->size();
	MCNode::cost_type costs = 0;
	for(stype i=0;i<g->size();++i)
		costs += g->at(i)->get_costs();
	if(m==0 || n == 0)
	{
		res.first = std::numeric_limits<double>::infinity();
		return res;
	}

	double prod = 1;
	for(stype i=1;i<=g->size();++i)
	{
		prod *= (1.0-(1.0/((double)cores*(double)i)));
	}

	MCNode::cost_type com_costs = (prod*m)* (sp->get_out_costs(1) + sp->get_in_costs(1) + sp->get_wait(1));

	res.second = cores;

	//CP calcs
	std::list<stype> rlist;
	MCNode::cost_type cp = find_crit_path(g->get_first(),rlist);//get_crit_costs(g,false);
	stype num_cp_out_edges = 0, num_cp_in_edges = 0;
	MCGraph * tg = g->travers();
	for(stype i : rlist)
	{
		if(g->at(i)->get_costs() == 0)
			continue;
		num_cp_out_edges += g->at(i)->size()-1;
		num_cp_in_edges += tg->at(i)->size()-1;
	}
	cp += prod * num_cp_in_edges * sp->get_in_costs(1) + prod * num_cp_out_edges * (sp->get_out_costs(1) +sp->get_wait(1));
	delete tg;


	res.first = (double)costs/(std::max((double)cp,(double)costs/cores) + (double)com_costs/cores);
	//res.first = (double)costs/(((double)costs/max_sp_lvl(g,cores,0)) + (double)com_costs/cores);

	return res;
}

std::pair<double,stype> edge_reduction_lvl_esti(MCGraph const * g, MPIParameter * sp, stype cores)
{
	std::pair<double,stype> res;

	res.first = 0;
	stype m = g->num_edges();
	stype n = g->size();
	MCNode::cost_type costs = 0;
	for(stype i=0;i<g->size();++i)
		costs += g->at(i)->get_costs();
	if(m==0 || n == 0)
	{
		res.first = std::numeric_limits<double>::infinity();
		return res;
	}

	double prod = 1;
	for(stype i=1;i<=g->size();++i)
	{
		prod *= (1.0-(1.0/((double)cores*(double)i)));
	}

	MCNode::cost_type com_costs = (prod*m)* (sp->get_out_costs(1) + sp->get_in_costs(1) + sp->get_wait(1));

	res.second = cores;

	//CP calcs
	std::list<stype> rlist;
	MCNode::cost_type cp = find_crit_path(g->get_first(),rlist);//get_crit_costs(g,false);
	stype num_cp_out_edges = 0, num_cp_in_edges = 0;
	MCGraph * tg = g->travers();
	for(stype i : rlist)
	{
		if(g->at(i)->get_costs() == 0)
			continue;
		num_cp_out_edges += g->at(i)->size()-1;
		num_cp_in_edges += tg->at(i)->size()-1;
	}
	cp += prod * num_cp_in_edges * sp->get_in_costs(1) + prod * num_cp_out_edges * (sp->get_out_costs(1) +sp->get_wait(1));
	delete tg;


	res.first = (double)costs/(std::max((double)costs/max_sp_lvl_fixed(g,cores,0),(double)cp) + (double)com_costs/cores);

	return res;
}


std::pair<double,stype> edge_reduction_test(MCGraph const * g, MPIParameter * sp, stype max_cores)
{
	std::pair<double,stype> res, temp;
	res.first = 0;
	for(stype i=1;i<=16;i+=1)
	{
		temp = edge_reduction_esti(g,sp,i);
		std::cout << temp.first << "\n";

	}
	return res;
}

double get_mean_edges(stype num_v, stype num_e, stype num_c)
{
	long double res = 1;

	for(stype i=1;i<=num_v;++i)
	{
		res *= (1.0L-1.0L/(long double)(num_c*i));
	}

	return (1.0-res);
}

void print_mean_edges(stype num_v, stype num_e)
{
	for(stype i=1;i<=16;++i)
	{
		std::cout << i << "," << get_mean_edges(num_v,num_e,i) << "\n";
	}
}

std::array<std::list<const MCNode *>,3> get_bsa_classes(MCGraph const  * g)
{
	/**
	 * res[0] cpn
	 * res[1] ibn
	 * res[2] obn
	 */
	std::array<std::list<const MCNode *>,3> res;
	std::list<stype> cp;
	std::vector<stype> num(g->size(),2);
	find_crit_path(g->get_first(),cp,false);
	const MCGraph * tg = g->travers();
	std::list<const MCNode *> dl;
	dl.push_back( tg->at(cp.back()));
	auto it = dl.begin();
	while(it!=dl.end())
	{
		num[(*it)->get_id()] = 1;
		for(auto p : *(*it))
		{
			dl.push_back(p.second);
		}
		++it;
	}
	for(auto t : cp)
	{
		num[t] = 0;
	}
	for(stype i=0;i<num.size();++i)
		res[num[i]].push_back(g->at(i));
	return res;
}
