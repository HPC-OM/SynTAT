# SynTAT - Synthetic Testing and Analysation of Task Graphs

## Overview
SynTAT is a framework for synthetic testing and analysation of Task-Gaphs. It provides
  - analysis and evaluation of scheduling and clustering algorithms,
  - benchmarking different parallelization methods,
  - and prediction of runtime for (parallel) OpenModelica simulations and other traceable programs.

SynTAT is developed by Marc Hartung and Martin Flehmig at Technische Universität Dresden. If 
you have questions concerning SynTAT or trouble using it, feel free to contact us via 
marc.hartung[at]tu-dresden.de and martin.flehmig[at]tu-dresden.de.

SynTAT is open-source and released under 3-Clause BSD License. See the LICENSE file which comes 
with SynTAT or visit https://opensource.org/licenses/BSD-3-Clause for further information.


## Installation
We provide a CMake script to configure, build and install SynTAT.

```
~> mkdir build
~> cd build
~> cmake ../
~> make -j
```

### Usage

Use the provided example Task-Graph in graphs:

```
~>  ./SynTAT ../graphs/DynamicPipes.graphml
```
