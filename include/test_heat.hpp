/*
 * tgsim_test.hpp
 *
 *  Created on: 15.02.2015
 *      Author: marc
 */

#ifndef TGSIM_TEST_HPP_
#define TGSIM_TEST_HPP_

#include <iostream>
#include <unordered_map>

#include <Graph/TestGraphs.hpp>
#include <Graph/MCNode.hpp>
#include <Graph/MCGraph.hpp>
#include <Graph/MCGraph_lib.hpp>

#include <IO/GraphMLReader.hpp>
#include <Graph/MCGraphAnalyser.hpp>

#include <Scheduling/Schedule.hpp>
#include <Scheduling/MCPScheduler.hpp>
#include <Scheduling/ETFScheduler.hpp>
#include <Scheduling/TFScheduler.hpp>
#include <Scheduling/LCScheduler.hpp>
#include <Scheduling/DSCAlgorithm.hpp>
#include <Scheduling/DLSAlgorithm.hpp>
#include <Scheduling/LVLScheduler.hpp>

#include <Simulation/MPIClassSimulation.hpp>
#include <Simulation/TBBClassSimulation.hpp>
#include <Simulation/PTHClassSimulation.hpp>
#include <Simulation/OMClassSimulation.hpp>
#include <Simulation/ComparisonSimulation.hpp>
#include <omp.h>
#include <io_simple.hpp>
using namespace std;

void test_heat()
{
	typedef MPIClassSimulation sim_type;
	std::string sim_name = "DynamicPipes";
	std::string sim_prefix = "";
	MCSimulation::set_repeat_count(100000);
	MCCommunicationSimulation::set_comm_reduction(true);
	MPIParameter sp1 = MPIParameter::pthreads(), sp = MPIParameter::distributed();
	bool unbound = false, print_graphs = false;
	std::string folder = "/home/marc/hpcom/tgsim_graphs/";
	GraphMLReader gRead;



	stype num_cores = 1, max_num_cores = 2;
	stype num_barriers = 10;
	MCNode::cost_type costs = (6*500);
	MCNode::cost_type costs_per_step = costs;
	MCNode::cost_type mk1,mk2;
	std::list<SingleLinkGraph *> dags;
	std::list<UnorderedSchedule> scheds;
	ComparisonSimulation CS("para_dags");

	std::list<MCClassSimulation *> sims;

	//

	for(stype p=1;p<=2;p*=2)
	{

		stype multiply = 1;
		double sp_cur = 0, sp_last = 1;

		while(num_cores<=p && std::abs(sp_cur-sp_last) > 0.01)
		{
			std::stringstream ss("");

			SingleLinkGraph * dag, temp;

			UnorderedSchedule sched(p), * sord;
			dags.push_back(new SingleLinkGraph());
			dag = (dags.back());

// DEFINE HERE GRAPH AND SCHED:
			rand_wert_sim_hint(dags.back(), num_barriers,costs_per_step,p,sched);
			DSCAlgorithm dcp(dag,sp);
			gRead.write("acutal.graphml",dag,sched);
// DEFINE HERE GRAPH AND SCHED:

			LCScheduler lc(dag,sp);
			//scheds.push_back(lc.get_dyn_send_first_order(sched));
			scheds.push_back(sched);



			sord = &(scheds.back());

			ss << sim_prefix << "para_dags_" << costs_per_step << "_" << multiply*p << "_" << sord->size();

			sims.push_back(new sim_type(sord,ss.str()));
			sims.back()->set_measure_send(true,(ss.str() + std::string("_send")));
			sims.back()->set_measure_recv(true,(ss.str() + std::string("_recv")));

			mk1 = lc.makespan(*sord,dag);
			mk2 = makespan( dag,sp,*sord);
			sp_last = sp_cur;
			sp_cur = (double)get_sum_costs(dag)/mk1;


			std::cout << p << "," << sord->get_costs() << "," << mk1 << "\n";
			break;

			//std::cout << p << "," << num_cores << "," << sp_cur << "," << (double)get_sum_costs(dag)/mk2 /* << "," << (double)get_sum_costs(&(temp))/get_crit_costs(&(temp)) << "," << sched.get_num_tasks() << "," << costs_per_step/sched.get_num_tasks()+1 */<< "\n";
		}


	}
	for(auto s : sims)
		CS.add_class_simulation(s);
	CS.toFile();
}




#endif /* TGSIM_TEST_HPP_ */
