/*
 * structs.hpp
 *
 *  Created on: 19.06.2014
 *      Author: marc
 */

#ifndef STRUCTS_HPP_
#define STRUCTS_HPP_

struct lvl_node_struct
{
	unsigned lvl;
	unsigned node;

	bool operator<(lvl_node_struct const & in) const
	{
		return lvl < in.lvl;
	}
};

struct send_recv_struct
{
	unsigned id;
	unsigned send;
	unsigned recv;

	bool operator<(send_recv_struct const & in) const
	{
		return send < in.send || (send == in.send && recv < in.recv);
	}
};



#endif /* STRUCTS_HPP_ */
