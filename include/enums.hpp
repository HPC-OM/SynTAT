/*
 * enums.hpp
 *
 *  Created on: 19.06.2014
 *      Author: marc
 */

#ifndef ENUMS_HPP_
#define ENUMS_HPP_

enum SimulationMode
{
	MPIFLOP,  MPIWAIT, TBBWAIT, TBBFLOP
};



#endif /* ENUMS_HPP_ */
