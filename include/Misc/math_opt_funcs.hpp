/*
 * math_opt_funcs.hpp
 *
 *  Created on: 05.12.2014
 *      Author: marc
 */

#ifndef MATH_OPT_FUNCS_HPP_
#define MATH_OPT_FUNCS_HPP_

#include <vector>
#include <map>
#include <array>
#include <types.hpp>
#include <iostream>
/*
template<typename val_s, typename val_t>
std::array<double,2> mdkq_lin(std::map<val_s,val_t> const & in)
{
	std::array<double,2> res = {0,0};
	double sx = 0, sy = 0, pxx = 0, pxy = 0;
	stype n = in.size();
	for(auto & p : in)
	{
		sx +=p.first; sy += p.second;
		pxx +=p.first*p.first;
		pxy = p.first*p.second;
	}

	sx /= n;
	sy /= n;
	std::cout << "l: " << pxy << " r " << sx*sy*n << "\n";
	res[0] = (pxy - sx*sy*n)/(pxx-sx*sx*n);
	res[1] = sy - res[0]*sx;

	return res;
}
*/
template<typename val_s, typename val_t>
std::array<double,2> mdkq_lin(std::map<val_s,val_t> const & in)
{
	std::array<double,2> res = {0,0};
	double sx = 0, sy = 0, pxx = 0, pxy = 0;
	stype n = in.size();
	for(auto & p : in)
	{
		sx +=p.first;
		sy += p.second;
	}
	sx /= n;
	sy /= n;
	for(auto & p : in)
	{
		pxy += (p.first-sx)*(p.second-sy);
		pxx += (p.first-sx)*(p.first-sx);
	}
	res[0] = (double)pxy/pxx;
	res[1] = sy - res[0]*sx;
	return res;
}

template<typename val_s, typename val_t>
double average_div(std::map<val_s,val_t> const & in)
{
	double res = 0;
	for(auto p : in)
		res += (double)p.second/p.first;
	return res/in.size();
}


#endif /* MATH_OPT_FUNCS_HPP_ */
