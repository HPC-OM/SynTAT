/*
 * io_simple.hpp
 *
 *  Created on: 02.11.2014
 *      Author: marc
 */

#ifndef IO_SIMPLE_HPP_
#define IO_SIMPLE_HPP_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>

template<typename T>
std::vector<T> read_csv_2(std::string const & file, bool skip = true)
{
	std::vector<T> res;
	bool first_line = skip;
	std::ifstream ins;
	std::string temp;
	char buffer[1024];
	ins.open(file.c_str());
	while(ins.good())
	{
		ins.getline(buffer,1024);
		temp = buffer;
		if(first_line || temp.size()==0) {
					first_line = false;
					continue;
				}
		std::string::size_type pos1 = temp.find(',')+1, pos2 = temp.find(',',pos1+1);
		if(pos2 == std::string::npos)
			pos2 = temp.size();
		temp = temp.substr(pos1,pos2-pos1);
		res.push_back(boost::lexical_cast<T>(temp));
	}

	ins.close();
	return res;
}



#endif /* IO_SIMPLE_HPP_ */
