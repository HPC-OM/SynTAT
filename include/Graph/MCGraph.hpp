/*
 * Graph.hpp
 *
 *  Created on: 19.06.2014
 *      Author: marc
 */

#ifndef MCGRAPH_HPP_
#define MCGRAPH_HPP_

#include <vector>
#include <list>
#include <utility>

#include <Graph/MCNode.hpp>

class MCGraph
{
public:

	typedef MCNode::cost_type cost_type;

	typedef std::vector<std::list<std::pair<unsigned,bool> > > edge_list;

	virtual ~MCGraph() { }

	virtual MCNode * get_first() const = 0;

	virtual unsigned size() const = 0;

	virtual unsigned num_edges() const = 0;

	virtual MCNode * at(unsigned in) const = 0;

	virtual MCNode * operator[](unsigned in) = 0;

	virtual unsigned num_nodes() const = 0;

	virtual MCGraph * copy(MCGraph const * in) const = 0;

	virtual void delete_zero_source_node() = 0;

	virtual MCGraph * travers() const = 0;


	/**
	 * returns an vector, each entry is the id of a node with a list of all incoming (second=false) and outgoing (second=true)
	 * edges with the node pair.first
	 */
	edge_list get_edges() const
	{
		edge_list res(size(),std::list<std::pair<unsigned,bool> >());
		for(unsigned i=0;i<size();++i)
		{
			for(std::pair<unsigned,const MCNode *> p : *(at(i)))
			{
				res[i].insert(res[i].end(),std::pair<unsigned,bool>(p.second->get_id(),true));
				res[p.second->get_id()].insert(res[p.second->get_id()].end(),std::pair<unsigned,bool>(i,false));
			}
		}

		return res;
	}

protected:


};




#endif /* GRAPH_HPP_ */
