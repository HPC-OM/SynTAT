/*
 * AnalysedNode.hpp
 *
 *  Created on: 28.08.2014
 *      Author: marc
 */

#ifndef ANALYSEDNODE_HPP_
#define ANALYSEDNODE_HPP_

#include <Graph/MCNode.hpp>
#include <vector>

class AnalysedNode {
public:

	// Before editing type check the set functions for support

	typedef unsigned lvl_type;

	enum Analyse
	{
		LVL,BLVL,TLVL,ALAP,COMP_COSTS,DEP_COUNT, /*<<---insert here */COUNT
	};

	AnalysedNode(MCNode const * in);

	virtual ~AnalysedNode();

	const MCNode * get_node() const { return node;}

	lvl_type get_blvl() const { return anas[BLVL];}
	lvl_type get_tlvl() const { return anas[TLVL];}
	lvl_type get_lvl() const { return anas[LVL];}
	lvl_type get_alap() const { return anas[ALAP];}
	lvl_type get_comp_costs() const { return anas[COMP_COSTS];}
	lvl_type get_dep_count() const { return anas[DEP_COUNT];}

	lvl_type get(Analyse type) { return anas[type]; }

	lvl_type & operator[](Analyse type) { return anas[type]; }

	lvl_type operator[](Analyse type) const { return anas[type]; }


	MCNode::cost_type get_costs() const { return node->get_costs(); }

	void set_blvl(lvl_type const & in) { anas[BLVL] = in; }
	void set_tlvl(lvl_type const & in) { anas[TLVL] = in; }
	void set_lvl(lvl_type const & in) { anas[LVL] = in; }
	void set_alap(lvl_type const & in) { anas[ALAP] = in; }
	void set_comp_costs(lvl_type const & in) { anas[COMP_COSTS] = in; }

	void set(Analyse type, lvl_type const & value) { anas[type] = value; }


	static struct
	{
		bool operator()(AnalysedNode const & lhs, AnalysedNode const & rhs)
		{
			return lhs.get_lvl()<rhs.get_lvl() || (lhs.get_lvl()==rhs.get_lvl() && lhs.get_costs() < rhs.get_costs());
		}
	} lvl_comp;



	static struct
	{
		bool operator()(AnalysedNode const & lhs, AnalysedNode const & rhs)
		{
			if(lhs.get_tlvl()<rhs.get_tlvl())
				return true;
			else if(lhs.get_tlvl()!=rhs.get_tlvl())
				return false;
			if(lhs.get_node()->size() > rhs.get_node()->size())
				return true;
			return false;
		}
	} tlvl_comp;


	static struct
	{
		bool operator()(AnalysedNode const & lhs, AnalysedNode const & rhs)
		{
			return lhs.get_blvl()<rhs.get_blvl() || (lhs.get_blvl()==rhs.get_blvl() && lhs.get_node()->get_id() < rhs.get_node()->get_id());
		}
	} blvl_comp;

	struct alap_comp_struct
	{
		bool operator()(AnalysedNode const & lhs, AnalysedNode const & rhs)
		{
			return lhs.get_alap()<rhs.get_alap() || (lhs.get_alap()==rhs.get_alap() && lhs.get_node()->get_id() < rhs.get_node()->get_id());
		}
	};

	static alap_comp_struct alap_comp;


private:

	MCNode const * node;

	std::vector<lvl_type> anas;


};

#endif /* ANALYSEDNODE_HPP_ */
