/*
 * TestGraphs.hpp
 *
 *  Created on: 09.06.2014
 *      Author: marc
 */

#ifndef TESTGRAPHS_HPP_
#define TESTGRAPHS_HPP_

#include <Graph/SingleLinkGraph.hpp>
#include <IO/GraphMLReader.hpp>
#include <Scheduling/UnorderedSchedule.hpp>

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <cmath>

void lvl_connect_graph(SingleLinkGraph * g,stype num_nodes_per_lvl, stype num_edges_per_node, stype num_lvls, MCNode::cost_type costs)
{
	stype nlvl = num_lvls;
	stype nodes = num_nodes_per_lvl;
	stype edges = std::max(1u,std::min(num_edges_per_node,nodes));

	g->new_node(0ull);
	g->set_first(g->at(0));
	g->new_nodes(nlvl*nodes,costs);
	stype cur, tar;
	for(stype i=1;i<=nodes;++i)
		g->make_edge(0,i,0);


	for(stype i=0;i<nlvl-1;++i)
	{
		for(stype j=1;j<=nodes;++j)
		{
			cur = i*nodes+j;
			for(stype k=0;k<edges;++k)
			{
				tar = cur+nodes+k;
				if(tar>(i+2)*nodes)
					tar -= nodes;
				if(tar>=g->size())
					break;
				g->make_edge(cur,tar,0);
			}
		}
	}

}

void cross_graph(SingleLinkGraph * g, stype length, stype width,MCNode::cost_type costs_per_node)
{
	g->set_first(g->at(g->new_node(0)));
	stype first = g->new_node(costs_per_node);
	g->make_edge(g->get_first()->get_id(),first);
	stype pre,num;
	for(stype i=0;i<width;++i)
	{
		num = g->new_node(costs_per_node);
		g->make_edge(first,num,0);
		g->at(first)->set_comm_float(num,5);
	}
	if(length<3)
		return;
	pre = g->get_first()->get_id()+1;
	for(stype i=0;i<length-2;++i)
	{
		num = g->new_node(costs_per_node);
		g->make_edge(pre,num,0);
		g->at(pre)->set_comm_float(num,5);
		pre = num;
	}
}

void closed_cross_graph(SingleLinkGraph * g, stype length, stype width,MCNode::cost_type costs_per_node)
{
	g->set_first(g->new_node(0));
	stype num, pre = g->new_node(costs_per_node);
	stype first = g->new_node(costs_per_node);
	g->make_edge(g->get_first()->get_id(),first);
	for(stype i=0;i<width;++i)
	{
		num = g->new_node(costs_per_node);
		g->make_edge(first,num,0);
		g->make_edge(num,pre,0);

	}


	if(length<4)
		return;
	for(stype i=0;i<length-3;++i)
	{
		num = g->new_node(costs_per_node);
		g->make_edge(pre,num,0);
		pre = num;
	}
}

void rand_wert_sim(SingleLinkGraph * g, stype steps, MCNode::cost_type cost_per_step, stype parallelity)
{
	g->new_node(0ull);
	g->set_first(g->at(0));
	MCNode::cost_type per_node = cost_per_step/parallelity + 1;
	for(stype i=0;i<parallelity;++i)
	{
		g->make_edge(0,g->new_node(per_node));
	}
	for(stype i=1;i<steps-1;++i)
	{
		for(stype j=0;j<parallelity;++j)
		{
			stype num = g->new_node(per_node);
			g->make_edge(num-parallelity,num,5);
			g->at(num-parallelity)->set_comm_float(num,5);
			if(j!=0)
			{
				g->make_edge(num-parallelity-1,num);
				g->at(num-parallelity-1)->set_comm_float(num,5);
			}
			if(j!=parallelity-1)
			{
				g->make_edge(num-parallelity+1,num);
				g->at(num-parallelity+1)->set_comm_float(num,5);
			}

		}
	}
}

void rand_wert_sim_hint(SingleLinkGraph * g, stype steps, MCNode::cost_type cost_per_step, stype parallelity,UnorderedSchedule & sched)
{
	g->new_node(0);
	g->set_first(g->at(0));
	sched[0].insert(sched[0].end(),g->at(0));

	MCNode::cost_type per_node = cost_per_step/parallelity + 1;
	std::vector<stype> parting;
	stype core = 0;
	stype hint_sum = parallelity, hint_it = sched.size();
	while(hint_sum!=0)
	{
		parting.push_back(hint_sum/hint_it);
		hint_sum -= hint_sum/hint_it;
		--hint_it;
	}
	for(stype i=1;i<parting.size();++i)
	{
		parting[i] += parting[i-1];
	}
	stype num;
	for(stype i=0;i<parallelity;++i)
	{
		num = g->new_node(per_node);
		if(parting[core]<=i)
			++core;
		g->make_edge(0,num);
		sched[core].insert(sched[core].end(),g->at(num));
	}
	for(stype i=1;i<steps;++i)
	{
		core = 0;
		for(stype j=0;j<parallelity;++j)
		{
			if(parting[core]<=j)
				++core;
			stype num = g->new_node(per_node);
			g->make_edge(num-parallelity,num);
			if(j!=0)
			{
				g->make_edge(num-parallelity-1,num);
			}
			if(j!=parallelity-1)
			{
				g->make_edge(num-parallelity+1,num);
			}
			sched[core].insert(sched[core].end(),g->at(num));
		}
	}
}


void simple_sequ_graph(SingleLinkGraph * g, unsigned num_nodes, SingleLinkGraph::cost_type costs)
{
	g->new_nodes(num_nodes,costs);
	g->set_first((*g)[0]);
	for(unsigned i=1;i<num_nodes;++i)
	{
		g->make_edge(i-1,i);
	}
}

void simple_dual_graph(SingleLinkGraph * g, unsigned num_nodes, SingleLinkGraph::cost_type costs)
{
	g->new_nodes(num_nodes,costs);
	g->set_first((*g)[0]);
	g->make_edge(0,1);
	g->make_edge(0,2);
	for(unsigned i=1;i<num_nodes-2;++i)
	{
		g->make_edge(i,i+2);
	}
}

void small_graph(SingleLinkGraph * g)
{
	g->new_node(100); // 0
	g->new_node(50); // 1
	g->new_node(10); // 2
	g->new_node(10); // 3

	g->make_edge(0,1);
	g->make_edge(0,2);
	g->make_edge(2,3);
	g->make_edge(1,3);

	g->set_first(0u);
}


void random_graph(SingleLinkGraph * g, unsigned num_nodes, unsigned num_edges, unsigned max_edges,stype it)
{
	for(unsigned i=0;i<num_nodes;++i)
	{
		g->new_node(1ull);
	}

	std::vector<unsigned> is_target(num_nodes,0),is_source(num_nodes,0);

	unsigned target, source;
	for(unsigned i=0;i<num_edges;++i)
	{
		srand((unsigned)clock()%134857632290);
		target = (rand()%(num_nodes-1))+1;
		srand((unsigned)clock()%12421278980);
		source = (rand()%(num_nodes-1))+1;;
		if(target==source)
		{
			--i;
			continue;
		}
		if(target<source)
		{
			unsigned temp = source;
			source = target;
			target = temp;
		}
		if(is_target[target]>=max_edges)
		{
			--i;
			continue;
		}
		if(is_source[source]>=max_edges)
		{
			--i;
			continue;
		}
		if(!g->make_edge(source,target))
		{
			--i;
		}
		else
		{
			is_target[target]++;
			is_source[source]++;
		}
	}

	for(unsigned i=1;i<num_nodes-1;++i)
	{
		if(is_target[i]==0) g->make_edge(0,i);
		if(is_source[i]==0) g->make_edge(i,num_nodes-1);
	}
	g->set_first(0u);
}

void vec_vec_mul_graph(SingleLinkGraph * g, unsigned pow)
{
	unsigned num = 1;
	for(unsigned i=0;i<pow;++i) num*=2;

	g->new_node(0ull);
	g->set_first(0u);
	unsigned temp1;
	for(unsigned i=0;i<num;++i)
	{
		temp1 = g->new_node(6ull);
		g->make_edge(0,temp1);
	}

	unsigned iter = num, start = 1;
	for(unsigned k=0;k<pow;++k)
	{
		for(unsigned i=start;i<start+iter;i+=2)
		{
			temp1=g->new_node(6ull);
			g->make_edge(i,temp1);
			g->make_edge(i+1,temp1);
		}
		start +=iter;
		iter/=2;
	}
}

void graphml_file(SingleLinkGraph *g, std::string filename)
{
	GraphMLReader reader;
	*g = reader.read(filename);
}



#endif /* TESTGRAPHS_HPP_ */
