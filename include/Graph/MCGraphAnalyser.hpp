/*
 * MCGraphAnalyser.hpp
 *
 *  Created on: 03.08.2014
 *      Author: marc
 */

#ifndef MCGRAPHANALYSER_HPP_
#define MCGRAPHANALYSER_HPP_

#include <vector>
#include <list>

#include <Graph/MCGraph.hpp>
#include <Graph/AnalysedNode.hpp>

#include <Scheduling/MPIParameter.hpp>
#include <types.hpp>

class MCGraphAnalyser
{
public:
	typedef AnalysedNode::lvl_type lvl_type;

	struct Statistic
	{
		stype num_nodes;

		MCNode::cost_type sum_costs;
		MCNode::cost_type max_costs;
		stype max_costs_index;
		double average_costs;

		stype num_edges;
		double average_edges_per_node;
		double sum_comm_costs;
		double max_comm_costs;
		double average_comm_costs;

		double calc_comm_costs_ratio;

		std::vector<double> parallelity;
		double max_parallelity;
		double min_parallelity;
		double average_parallelity;

		MCNode::cost_type cp_costs;
		double max_speedup;

		friend std::ostream & operator<<(std::ostream & in, Statistic const & s);

	};

	MCGraphAnalyser(MCGraph const * g, const MPIParameter * sp) : sp(sp), analysed(std::vector<bool>(AnalysedNode::COUNT,false))
	{
		this->g = new SingleLinkGraph(sp->apply_costs(g));

	}
	~MCGraphAnalyser()
	{
		delete g;
	}

	std::vector<stype> transitive_succs_count() const;

	std::vector<std::set<stype> > transitive_succs() const;

	std::vector<double> get_parallelity() const;

	std::list<const MCNode *> get_crit_path() const;

	MCGraph::cost_type crit_path_costs(bool comm = false) const;

	std::vector<AnalysedNode> & get_analysed_nodes(AnalysedNode::Analyse type);

	Statistic get_statistic() const;

	MCNode::cost_type get_sum_costs() const;


private:
	SingleLinkGraph * g;
	const MPIParameter * sp;
	std::vector<bool> analysed;

	std::vector<AnalysedNode> aNodes;

	std::vector<lvl_type> get_lvls() const;

	std::vector<lvl_type> get_top_lvls() const;

	std::vector<lvl_type> get_bot_lvls() const;

	std::vector<lvl_type> get_alaps() const;

	std::vector<lvl_type> get_comp_costs() const;

	void rec_lvl_calc(stype n, const MCNode * first, std::vector<lvl_type> & res) const;

	std::set<stype> rec_succs_count(const MCNode * first, std::vector<stype> & out) const;

	std::set<stype> rec_succs(const MCNode * first, std::vector<std::set<stype> > & out) const;

	void rec_toplvl_calc(lvl_type n, const MCNode * first, std::vector<lvl_type> & res) const;

	lvl_type rec_botlvl_calc(const MCNode * first, std::vector<lvl_type> & res) const;

	MCNode::cost_type rec_crit_path(const MCNode * first, std::vector<std::pair<std::list<const MCNode *>,MCGraph::cost_type> > & temp) const;




};



#endif /* MCGRAPHANALYSER_HPP_ */
