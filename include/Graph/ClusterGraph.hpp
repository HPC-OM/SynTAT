/*
 * ClusterGraph.hpp
 *
 *  Created on: 10.09.2014
 *      Author: marc
 */

#ifndef CLUSTERGRAPH_HPP_
#define CLUSTERGRAPH_HPP_

#include <unordered_map>

#include <Scheduling/UnorderedSchedule.hpp>
#include <Graph/MCNode.hpp>
#include <types.hpp>


class Link
{
public:
	stype c1, c2;

	Link(stype const & c1, stype const & c2,stype num, bool correct_direction);

	Link(stype const & c1, stype const & c2);

	stype get_num() const;

	bool get_direction() const;

	void set_num(stype const & in);

	void add_num(stype const & in);

	stype operator[](stype index) const;

	stype & operator[](stype index);

private:
	stype num;
	bool direction;
};

class ClusterNode
{
public:
	typedef std::list<Link>::iterator link_acc;
	typedef std::list<ClusterNode>::iterator cluster_acc;
	typedef std::unordered_map<stype,cluster_acc> node_list;
	typedef std::unordered_map<stype,cluster_acc>::iterator cluster_iterator;
	typedef std::unordered_map<stype,cluster_acc>::const_iterator const_cluster_iterator;
	typedef std::unordered_map<stype,link_acc> link_list;
	typedef std::unordered_map<stype,link_acc>::iterator link_iterator;
	typedef std::unordered_map<stype,link_acc>::const_iterator const_link_iterator;

	ClusterNode(stype id,MCNode::cost_type costs, std::list<const MCNode *> const & l,link_acc end, cluster_acc no_cluster);

	stype get_id() const;

	bool add_link(link_acc l, cluster_acc c1, cluster_acc c2);

	link_acc get_link(stype id);

	link_iterator erase(link_iterator it);

	void erase(stype id);

	stype size() const;

	MCNode::cost_type get_costs() const;

	void set_costs(MCNode::cost_type in);

	void add_nodes(std::list<const MCNode *> const & in);

	std::list<const MCNode *> & get_nodes();

	cluster_acc operator[](stype index);

	cluster_iterator cluster_begin();

	cluster_iterator cluster_end();

	const_cluster_iterator cluster_begin() const;

	const_cluster_iterator cluster_end() const;

	link_iterator begin();

	const_link_iterator begin() const;

	link_iterator end();

	const_link_iterator end() const;

	bool operator==(const ClusterNode & in) const;

	bool operator<(const ClusterNode & in) const;

	bool operator>(const ClusterNode & in) const;


private:
	std::list<const MCNode *> mcnodes;
	cluster_acc no_cluster;
	link_acc no_link;

	node_list cnodes;
	link_list links;

	MCNode::cost_type costs;
	const stype id;

	void private_erase(stype id);

	link_iterator private_erase(link_iterator it);
};


class ClusterGraph {
public:
	typedef ClusterNode::link_acc link_acc;
	typedef std::list<ClusterNode>::iterator cluster_acc;

	ClusterGraph(UnorderedSchedule const & in);

	virtual ~ClusterGraph();

	void init(UnorderedSchedule const & in);

	void erase(stype index);

	ClusterNode & operator[](stype index);

	ClusterNode operator[](stype index) const;

	void checkLinks()
	{
		bool flag = false;
		for(Link l : links)
		{
			if(vnodes[l[0]] == nodes.end() || vnodes[l[1]]==nodes.end())
			{
				std::cout << "link " << l[0] << "-" << l[1] << " couldnt be resolved\n";
				flag = true;
			}
		}
		if(flag) exit(1);
	}

	stype size() const;

	link_acc get_most_dense_link();

	void sort_nodes();

	stype num_links() const;

	stype merge(std::list<Link>::iterator l);

	stype merge(stype n1, stype n2);

	UnorderedSchedule get_unordered_schedule() const;

	std::vector<unsigned> get_isolated_clusters() const;

	std::vector<stype> get_connected_clusters() const;

	std::vector<stype> get_cluster_nums() const;

	std::list<Link>::const_iterator link_begin() const;

	std::list<Link>::const_iterator link_end() const;

	std::list<Link>::iterator link_begin();

	std::list<Link>::iterator link_end();

	std::list<link_acc> get_inner_links(std::vector<stype> const & cluster_nums);



private:

	std::list<ClusterNode> nodes;
	std::vector<cluster_acc> vnodes;
	std::list<Link> links;
};

#endif /* CLUSTERGRAPH_HPP_ */
