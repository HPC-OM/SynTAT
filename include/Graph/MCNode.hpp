/*
 * MCNode.hpp
 *
 *  Created on: 19.06.2014
 *      Author: marc
 */

#ifndef MCNODE_HPP_
#define MCNODE_HPP_

#include <unordered_map>
#include <iostream>

#include <types.hpp>


class MCNode
{
public:
	typedef std::unordered_map<stype, const MCNode *> node_list;
	typedef unsigned cost_type;
	typedef stype communication_type;
	typedef std::unordered_map<stype, communication_type> comm_list_type;

	MCNode(stype id, cost_type costs)
	{
		this->id = id;
		this->costs = costs;
	}

	virtual ~MCNode() { }

	node_list::iterator begin() { return succs.begin(); }

	node_list::iterator end() { return succs.end(); }

	node_list::const_iterator begin() const { return succs.begin(); }

	node_list::const_iterator end() const { return succs.end(); }

	bool add_node(MCNode * in, communication_type costs = 0) {
		return succs.insert(std::pair<stype,MCNode *>(in->get_id(),in)).second && comm_costs.insert(std::pair<stype,communication_type>(in->get_id(),costs)).second; }

	bool remove_node(const MCNode * in) {
		comm_costs.erase(in->get_id());
		return 0 < succs.erase(in->get_id());
	}

	stype get_id() const { return id; }

	stype size() const { return succs.size(); }

	cost_type get_costs() const { return costs; }

	void set_costs(cost_type in) { this->costs = in; }

	bool operator==(MCNode const & in) const { return id == in.id; }

	communication_type get_comm_costs(stype node_id) const
	{
		communication_type res;
		if(comm_costs.find(node_id) == comm_costs.end())
		{
			res = 0;
		}
		else res = comm_costs.at(node_id);
		return res;
	}

	void set_comm_costs(stype node_id, cost_type in)
	{
		if(comm_costs.find(node_id) == comm_costs.end())
		{
			comm_costs.insert(std::pair<stype,cost_type>(node_id,in));
		}
		else
			comm_costs[node_id] = in;
	}

	bool has_edge_with(stype node_id) const
	{
		return succs.find(node_id) != succs.end();
	}

	void set_comm_float(stype node_id, stype num)
	{
		comm_vars_f[node_id] = num;
	}

	void set_comm_bool(stype node_id, stype num)
	{
		comm_vars_b[node_id] = num;
	}

	void set_comm_int(stype node_id, stype num)
	{
		comm_vars_i[node_id] = num;
	}

	stype get_comm_bool(stype node_id) const
	{
		stype res = 0;
		std::unordered_map<stype,stype>::const_iterator it =  comm_vars_b.find(node_id);
		if(it != comm_vars_b.end())
		{
			res = it->second;
		}
		return res;

	}

	stype get_comm_float(stype node_id) const
	{
		stype res = 0;
		std::unordered_map<stype,stype>::const_iterator it =  comm_vars_f.find(node_id);
		if(it != comm_vars_f.end())
		{
			res = it->second;
		}
		return res;

	}

	stype get_comm_int(stype node_id) const
	{
		stype res = 0;
		std::unordered_map<stype,stype>::const_iterator it =  comm_vars_i.find(node_id);
		if(it != comm_vars_i.end())
		{
			res = it->second;
		}
		return res;
	}

	void print_comm_vars(stype node_id) const
	{
		std::cout << "edge " << id << " to " << node_id << "\n";
		std::cout << "has " << get_comm_int(node_id) << " ints\n";
		std::cout << "has " << get_comm_bool(node_id) << " bools\n";
		std::cout << "has " << get_comm_float(node_id) << " floats\n";
	}


private:


	stype id;
	cost_type costs;

	node_list succs;
	comm_list_type comm_costs;
	std::unordered_map<stype,stype> comm_vars_f;
	std::unordered_map<stype,stype> comm_vars_b;
	std::unordered_map<stype,stype> comm_vars_i;



};


#endif /* NODE_HPP_ */
