#ifndef MCGRAPH_LIB
#define MCGRAPH_LIB

#include <vector>
#include <list>
#include <array>

#include <Graph/MCGraph.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <Scheduling/MPIParameter.hpp>

MCNode::cost_type find_crit_path(const MCNode * first, std::list<unsigned> & rlist,bool comm = true);

MCNode::cost_type get_crit_costs(const MCGraph * g,bool comm = false);

MCNode::cost_type get_sum_costs(const MCGraph * g);

MCNode::cost_type get_sum_comm_costs(const MCGraph * g);

std::vector<MCGraph::cost_type> calculate_alap(const MCGraph * g);

MCGraph::cost_type rec_calculate_alap(const MCNode * n, std::vector<MCGraph::cost_type> & l);

std::vector<MCGraph::cost_type> calculate_asap(const MCGraph * g);

void rec_calculate_asap(const MCNode * n,std::vector<MCGraph::cost_type> & l, std::vector<stype> & deps);

MCNode::cost_type rec_blvl(const MCNode * n, std::vector<MCNode::cost_type> & in,bool comm= true);

std::vector<MCNode::cost_type> get_blvls(const MCGraph * g,bool comm = true);

std::vector<MCNode::cost_type> get_tlvls(const MCGraph * g, bool comm = true);

std::vector<stype> get_lvls(MCGraph const * g);

std::vector<stype> get_travers_lvls(MCGraph const * g);

void rec_calculate_lvl(const MCNode * n, std::vector<stype> & lvls);

stype rec_calculate_travers_lvl(const MCNode * n, std::vector<stype> & lvls);

void lvl_sched_ana(const MCGraph * g,stype cores);

std::vector<stype> find_dependencies(const MCGraph * g);

void paint_cp(MCGraph const * g, MPIParameter * sp, UnorderedSchedule const & sched);

MCNode::cost_type makespan(MCGraph const * g, MPIParameter const & sp, UnorderedSchedule const & sched);

std::pair<double,stype> edge_reduction_lvl_esti(MCGraph const * g, MPIParameter * sp, stype cores);

std::pair<double,stype> edge_reduction_esti(MCGraph const * g, MPIParameter * sp, stype cores);

std::pair<double,stype> edge_reduction_test(MCGraph const * g, MPIParameter * sp, stype max_cores);

std::vector<MCNode::cost_type> get_task_waits(MCGraph const * g, UnorderedSchedule const & sched, MPIParameter const * sp);

double max_sp_lvl(MCGraph const * g);

double max_sp_lvl(MCGraph const * g, stype num_cores,MCNode::cost_type overhead = 0);

double max_sp_lvl_fixed(MCGraph const * g);

double max_sp_lvl_fixed(MCGraph const * g, stype num_cores, MCNode::cost_type overhead = 0);

stype max_cores_lvl(MCGraph const * g);

void print_mean_edges(stype num_v, stype num_e);

double get_mean_edges(stype num_v, stype num_e, stype num_c);

std::array<std::list<const MCNode *>,3> get_bsa_classes(MCGraph const  * g);

std::list<const MCNode *> topological_order(MCGraph const * g);

MCNode::cost_type imp_makespan(MCGraph const * g, MPIParameter const & sp, UnorderedSchedule const & sched, bool recv = false);

#endif
