/*
 * SingleLinkGraph.hpp
 *
 *  Created on: 20.06.2014
 *      Author: marc
 */

#ifndef SINGLELINKGRAPH_HPP_
#define SINGLELINKGRAPH_HPP_

#include <vector>
#include <boost/graph/adjacency_list.hpp>


#include <Graph/MCNode.hpp>
#include <Graph/MCGraph.hpp>



class SingleLinkGraph: public MCGraph {
public:

	typedef MCNode::cost_type cost_type;

	SingleLinkGraph()
	{
		first = 0;
		edge_counter = 0;
	}

	SingleLinkGraph(MCGraph const * in)
	{
		init(in);
	}

	SingleLinkGraph(SingleLinkGraph const & in)
	{
		init(&in);
	}

	~SingleLinkGraph();

	void init();

	void init(MCGraph const * in);

	SingleLinkGraph & operator=(SingleLinkGraph const & in);

	 MCNode * get_first() const { return first; }

	 void set_first(MCNode * first) { this->first = first; }

	 void set_first(unsigned first) { set_first(nodes[first]); }

	 unsigned size() const { return nodes.size(); }

	 unsigned num_edges() const {
		 if(get_first()->get_costs()==0)
			 return edge_counter-get_first()->size();
		 return edge_counter;
	 }

	 unsigned num_nodes() const {
		 if(get_first()->get_costs()==0)
			 return size()-1;
		 return size(); }

	 MCNode * at(unsigned in) const { return nodes[in]; }

	 MCNode * operator[](unsigned in){ return at(in); }

	 void delete_zero_source_node();

	 unsigned new_node(cost_type costs);

	 void new_nodes(unsigned num, cost_type costs);

	 bool make_edge(MCNode * from, MCNode * to, MCNode::communication_type costs);

	 bool make_edge(MCNode * from, MCNode * to) { return make_edge(from,to,0); }

	 bool make_edge(unsigned from, unsigned to, MCNode::communication_type costs) { return make_edge(nodes[from],nodes[to], costs); }

	 bool make_edge(unsigned from, unsigned to) { return make_edge(from,to,0); }

	 bool remove_edge(MCNode * from, MCNode * to);

	 bool remove_edge(unsigned from, unsigned to) { return remove_edge(nodes[from],nodes[to]); }

	 MCGraph * copy(MCGraph const * in) const;

	 SingleLinkGraph & operator *=(cost_type sk);

	 std::pair<stype,stype> merge(stype s1, stype t1, stype s2, stype t2);

	 MCGraph * travers() const;

private:

	 MCNode * first;
	 std::vector<MCNode *> nodes;

	 unsigned edge_counter;

	 void delete_nodes();

};


#endif /* SINGLELINKGRAPH_HPP_ */
