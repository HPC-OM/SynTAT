/*
 * MCGraph_lib.hpp
 *
 *  Created on: 11.07.2014
 *      Author: marc
 */

#ifndef MCGRAPH_LIB_HPP_
#define MCGRAPH_LIB_HPP_

#include <list>
#include <Graph/MCGraphAnalyser.hpp>
#include <Graph/AnalysedNode.hpp>

#include <Graph/MCGraph.hpp>

MCNode::cost_type find_crit_path(const MCNode * first, std::list<unsigned> & rlist);

std::vector<MCGraph::cost_type> calculate_alap(const MCGraph * g);

std::vector<MCGraph::cost_type> calculate_asap(const MCGraph * g);

MCNode::cost_type get_crit_costs(const MCGraph * g);

MCNode::cost_type get_sum_costs(const MCGraph * g);

void rec_calculate_lvl(const MCNode * n, std::vector<stype> & lvls);

std::vector<stype> get_lvls(MCGraph const * g);

MCGraph::cost_type rec_calculate_alap(const MCNode * n, std::vector<MCGraph::cost_type> & l);

void rec_calculate_asap(const MCNode * n,std::vector<MCGraph::cost_type> & l, std::vector<stype> & deps);

std::vector<MCNode::cost_type> get_tlvls(const MCGraph * g);

std::vector<MCNode::cost_type> get_blvls(const MCGraph * g);

void lvl_sched_ana(const MCGraph * g,stype cores);

std::vector<stype> find_dependencies(const MCGraph * g);


#endif /* MCGRAPH_LIB_HPP_ */
