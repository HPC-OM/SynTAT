/*
 * test_om_graphs.hpp
 *
 *  Created on: 15.02.2015
 *      Author: marc
 */

#ifndef TEST_OM_GRAPHS_HPP_
#define TEST_OM_GRAPHS_HPP_

#include <iostream>
#include <unordered_map>
#include <numeric>

#include <Graph/TestGraphs.hpp>
#include <Graph/MCNode.hpp>
#include <Graph/MCGraph.hpp>
#include <Graph/MCGraph_lib.hpp>

#include <IO/GraphMLReader.hpp>
#include <Graph/MCGraphAnalyser.hpp>

#include <Scheduling/Schedule.hpp>
#include <Scheduling/MCPScheduler.hpp>
#include <Scheduling/ETFScheduler.hpp>
#include <Scheduling/TFScheduler.hpp>
#include <Scheduling/LCScheduler.hpp>
#include <Scheduling/DSCAlgorithm.hpp>
#include <Scheduling/DLSAlgorithm.hpp>
#include <Scheduling/LVLScheduler.hpp>

#include <Simulation/MPIClassSimulation.hpp>
#include <Simulation/TBBClassSimulation.hpp>
#include <Simulation/PTHClassSimulation.hpp>
#include <Simulation/OMClassSimulation.hpp>
#include <Simulation/ComparisonSimulation.hpp>
#include <omp.h>
#include <io_simple.hpp>

//MF: ungenutzt
//void test_duplication(const MCGraph * g)
//{
//	typedef PTHClassSimulation sim_type;
//	MPIParameter sp = MPIParameter::pthreads();
//
//	MCGraph *tg = g->travers();
//	for(unsigned i=0;i<g->size();++i)
//	{
//
//	}
//
//
//	delete tg;
//}

void test_om(const std::string& infile)
{
	// s1-s5 mpi, s6 - pth
	typedef PTHClassSimulation sim_type;
	std::string para_suffix = "";
	std::string sim_name = std::string("test")+para_suffix;
	std::string sim_prefix = "";
	MCSimulation::set_repeat_count(10000);
	MCCommunicationSimulation::set_comm_reduction(false);
	MPIParameter sp = MPIParameter::pthreads(), sp2 = MPIParameter::distributed(), sp3 = MPIParameter::shared();

	bool unbound = false, print_graphs = false;
	//MF std::string folder = "/home/hartung/hpcom/tasksched/trunk/graphs/";
	//MF std::string folder = "/mnt/data/bu/Projekte/hpcom-tasksched/trunk/graphs/";
	GraphMLReader gRead;
	bool syn = false;
	std::vector<std::string> infiles;
	//infiles.push_back("spice3");
	//MF infiles.push_back("DynamicPipes");
	infiles.push_back(infile);
	//infiles.push_back("FourRobots");
	//infiles.push_back("CauerLow");
	//infiles.push_back("wire");
	//infiles.push_back("FourBitAdder");
	//infiles.push_back("CoilSpring");
	std::vector<std::string> infiles2(infiles.size(),"");


	std::list<stype> cores;

	for(unsigned i=1;i<=24 ;i+=1)
		cores.push_back(i);

	std::vector<SingleLinkGraph> graphs;
	for(unsigned i = 0;i<infiles.size();++i)
	{
		//MF SingleLinkGraph temp = gRead.read(folder + infiles[i] + std::string(".graphml"));
	    SingleLinkGraph temp = gRead.read(infiles[i]);
		//graphs.push_back(sp.apply_costs(&temp));
		graphs.push_back(temp);
	}

	for(stype i=0;i<infiles.size();++i)
		infiles2[i] = sim_prefix + infiles[i];
	std::cout << "Model,V,E,sumcosts,cp,speedup,spmin,lvlsp\n";

	for(unsigned i=0;i<graphs.size();++i)
	{
		std::cout << infiles[i] << ",";
	}
	std::cout << "\n";
	std::cout << "Modell,Komm-Kosten,Task-Kosten,CCR\n";
	for(unsigned i=0;i<graphs.size();++i)
	{
		MCGraphAnalyser ga(&(graphs[i]),&sp);
		SingleLinkGraph sg_t = sp.apply_costs(&graphs[i]);
		std::cout << infiles[i] << "\n";
		std::cout << graphs[i].num_nodes() << ",";
		std::cout << graphs[i].num_edges() << ",";
		std::cout << (double)get_sum_costs(&(graphs[i]))/2.9e3 << ",";
		std::cout << get_crit_costs(&(graphs[i]),false) << ",";
		std::cout << (double)get_sum_costs(&(graphs[i]))/get_crit_costs(&(graphs[i]),false) << ",";
		std::cout << (double)get_sum_costs(&(graphs[i]))/get_crit_costs(&(sg_t),true) << ",";
		std::cout << get_sum_costs(&sg_t) << "\n\n";

	}
	//exit(0);
	std::vector<std::string> sim_names;
	std::vector<MCScheduler *> scheder;

	for(unsigned i=0;i<graphs.size();++i)
	{


		/*std::cout << "ETF\n";
		scheder.push_back(new ETFScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_etf"));
*/
		std::cout << "DLS\n";
		scheder.push_back(new DLSAlgorithm(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_dls"));

/*
		std::cout << "TF\n";
		scheder.push_back(new TFScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_tf"));
		std::cout << "MCP\n";
		scheder.push_back(new MCPScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_mcp"));

		std::cout << "LVL\n";
		scheder.push_back(new LVLScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_lvl"));

		std::cout << "DSC\n";
		scheder.push_back(new DSCAlgorithm(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_dsc_lb"));
		((ClusterScheduler *)scheder.back())->set_mapping(ClusterScheduler::LOAD_BALANCE);

		std::cout << "DSC\n";
		scheder.push_back(new DSCAlgorithm(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_dsc_cm"));
		((ClusterScheduler *)scheder.back())->set_mapping(ClusterScheduler::COMM_MIN);

		std::cout << "LC\n";
		scheder.push_back(new LCScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_lc_lb"));
		((ClusterScheduler *)scheder.back())->set_mapping(ClusterScheduler::LOAD_BALANCE);

		std::cout << "LC\n";
		scheder.push_back(new LCScheduler(&(graphs[i]),sp));
		sim_names.push_back(infiles[i]+std::string("_lc_cm"));
		((ClusterScheduler *)scheder.back())->set_mapping(ClusterScheduler::COMM_MIN);
*/

	}
	std::vector<std::string> names;
	std::vector<UnorderedSchedule> schedes;

	for(unsigned i=0;i<scheder.size();++i)
	{
		std::stringstream ss("_unbound");
		if(unbound)
		{
			schedes.push_back(scheder[i]->get_schedule_unbound());
			names.push_back(sim_names[i] + ss.str());
			if(print_graphs) gRead.write(names.back() + std::string(".graphml"),scheder[i]->get_graph(),schedes.back());
		}
		for(stype j : cores)
		{
			ss.str("");
			ss << "_"<< j;
			std::cout << "" << sim_names[i] << ss.str() << ", ";
			UnorderedSchedule stemp = scheder[i]->get_schedule(j,0);
			schedes.push_back(stemp);
			names.push_back(sim_names[i] + ss.str());

			std::cout << names.back() << "," << makespan(scheder[i]->get_graph(), sp,stemp) << "," << (double)stemp.get_costs()/makespan(scheder[i]->get_graph(), sp,stemp) << "\n";
			std::cout << names.back() << "," << imp_makespan(scheder[i]->get_graph(), sp,stemp) << "," << (double)stemp.get_costs()/imp_makespan(scheder[i]->get_graph(), sp,stemp) << "\n";
		}
	}

	std::cout << "all schedes are ready\n";
	//exit(0);
	for(unsigned i=0;i<graphs.size();++i)
	{
		if(graphs[i].at(0)->get_costs() == 0 )
		{
			graphs[i].delete_zero_source_node();
		}
	}

	std::stringstream ss;
	std::vector<MCClassSimulation *> sims;
	std::cout << "collect sims\n";
	for(unsigned i=0;i<schedes.size();++i)
	{
		ss.str("");
		ss << names[i] <<  para_suffix;
		sims.push_back(new sim_type(&(schedes[i]),ss.str()+std::string("")));
		//sims.push_back(new sim_type(&(schedes[i]),ss.str()+std::string("_measure")));
		//sims.back()->set_measure_task(true,ss.str());
		//sims.back()->set_measure_recv(true,ss.str()+std::string("_recv"));
		//sims.back()->set_measure_send(true,names[i]+std::string("_send"));
	}


	ComparisonSimulation CS(sim_prefix + sim_name.c_str());
	for(unsigned i=0;i<sims.size();++i)
	{
		CS.add_class_simulation((sims[i]));
	}
	std::cout << "printing files:\n";
	CS.toFile(sim_name);
	std::cout << "backup into file\n";
}




#endif /* TEST_OM_GRAPHS_HPP_ */
