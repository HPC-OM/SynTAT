/*
 * Schedule.hpp
 *
 *  Created on: 09.01.2015
 *      Author: marc
 */

#ifndef SCHEDULE_HPP_
#define SCHEDULE_HPP_

#include <set>

#include <Graph/SingleLinkGraph.hpp>
#include <Scheduling/MPIParameter.hpp>
#include <Scheduling/UnorderedSchedule.hpp>

struct Slot
{
	MCNode::cost_type start;
	MCNode::cost_type end;

	stype core;

	std::list<const MCNode *>::iterator prev;
	std::list<const MCNode *>::iterator succ;

	bool operator<(Slot const & in) const
	{
		return start<in.start || (start == in.start && end<in.end);
	}
};

class Schedule {
public:
	Schedule(const MCGraph *g, MPIParameter const & sp, stype size);

	virtual ~Schedule();

	void schedule(const MCNode * n, Slot const & s, stype core);

	Slot earliest_slot(const MCNode * n, stype core);

	Slot earliest_time(const MCNode * n, stype core);

	stype earliest_core(const MCNode * n);

	MCNode::cost_type earliest_start_time_on(const MCNode * n, stype core);

	MCNode::cost_type makespan() const;

	MCNode::cost_type makespan(stype core) const;

	stype size() const;

	std::list<const MCNode *> free() const;

	std::list<const MCNode *> & free();

	std::list<const MCNode *> new_free() const;

	UnorderedSchedule get_unordered_sched() const;

private:
	const MCGraph * g;
	const MCGraph *tg;
	SingleLinkGraph sg;
	const MPIParameter * sp;

	std::vector<std::list<const MCNode *> > sched;

	std::vector<MCNode::cost_type> node_start;
	std::vector<stype> node_deps;
	std::list<const MCNode *> free_nodes;
	std::list<const MCNode *> new_free_nodes;

	std::vector<std::list<const MCNode *>::iterator > free_its;

	std::vector<stype> task_map;

	std::vector<stype> act_recv;
	std::vector<stype> act_send;

	std::vector<std::set<Slot> > slots;

	std::vector<std::set<Slot>::iterator> current_open_slots; //speed up sched step
	std::vector<MCNode::cost_type> current_node_start;
	const MCNode * current_node;



	void flush();



};

#endif /* SCHEDULE_HPP_ */
