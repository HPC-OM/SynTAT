/*
 * MCSchedule.hpp
 *
 *  Created on: 09.07.2014
 *      Author: marc
 */

#ifndef MCSCHEDULE_HPP_
#define MCSCHEDULE_HPP_

#include <vector>
#include <list>

#include <Scheduling/MCSingleSchedule.hpp>
#include <Graph/MCNode.hpp>

class MCSchedule {
public:
	typedef MCSingleSchedule::size_type size_type;
	typedef MCSingleSchedule::cost_type cost_type;

	MCSchedule();

	MCSchedule(unsigned cores, unsigned * nodes);

	MCSchedule(unsigned cores);

	virtual ~MCSchedule();

	unsigned get_num_cores() const;

	unsigned get_num_tasks() const;

	unsigned get_node_num(unsigned core) const;

	unsigned size() const;

	/**
	 * Creats a new schedule for a core and inserts [node]
	 * @ret the index of the new single schedule
	 */
	unsigned push_back(const MCNode * node);

	/**
	 * Creats a new schedule for a core and inserts a list of nodes
	 * @ret the index of the new single schedule
	 */
	unsigned push_back(std::list<const MCNode *> const & in);

	void clear();

	unsigned get_num_task(unsigned core) const;

	MCSingleSchedule get_schedule(unsigned core_num) const;

	MCSingleSchedule & operator[](unsigned num);

	MCSingleSchedule operator[](unsigned num) const;

	cost_type get_costs() const;

	std::vector<unsigned> get_task_map() const;

	friend std::ostream & operator<<(std::ostream & in, MCSchedule const & sched);


private:
	std::vector<unsigned> task_map, node_map;
	std::vector<MCSingleSchedule> sched;
};

#endif /* MCSCHEDULE_HPP_ */
