/*
 * LVLScheduling.hpp
 *
 *  Created on: 16.09.2014
 *      Author: marc
 */

#ifndef LVLSCHEDULER_HPP_
#define LVLSCHEDULER_HPP_

#include <Scheduling/MCScheduler.hpp>

class LVLScheduler: public MCScheduler {
public:
	LVLScheduler(MCGraph const * in, MPIParameter const & sp);

	virtual ~LVLScheduler();

	UnorderedSchedule get_schedule(stype num_cores, stype * nodes);

	UnorderedSchedule get_schedule_unbound();

	UnorderedSchedule get_schedule_distributed();
};

#endif /* LVLSCHEDULER_HPP_ */
