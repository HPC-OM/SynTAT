/*
 * OrderedSchedule.hpp
 *
 *  Created on: 28.08.2014
 *      Author: marc
 */

#ifndef ORDEREDSCHEDULE_HPP_
#define ORDEREDSCHEDULE_HPP_

#include <Graph/MCNode.hpp>
#include <Graph/AnalysedNode.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <structs.hpp>

class SchedStream
{
public:
	typedef std::list<AnalysedNode> node_list;
		node_list sched;
		MCNode::cost_type sum_costs;
		AnalysedNode::Analyse type;

		const unsigned id;

		SchedStream(unsigned id) : id(id)
		{
			sum_costs = 0;
			type = AnalysedNode::ALAP;
		}

		unsigned get_id() const { return id; }

		bool operator<(SchedStream const & rhs) const { return sum_costs<rhs.sum_costs || (sum_costs == rhs.sum_costs && id < rhs.id); }

		void insert(AnalysedNode in) {
			insert(sched.end(),in);
		}

		void insert(node_list::iterator it,AnalysedNode in) {
			sched.insert(it, in);
			sum_costs += in.get_tlvl()+in.get_node()->get_costs();
		}

		MCNode::cost_type get_costs() const { return sum_costs; }

		node_list::const_iterator begin() const { return sched.begin(); }

		node_list::const_iterator end() const { return sched.end(); }

		node_list::iterator begin() { return sched.begin(); }

		node_list::iterator end() { return sched.end(); }

		struct p_cmp
		{
			bool operator()(const SchedStream * lhs, const SchedStream * rhs)
			{
				return *lhs < *rhs;
			}
		};

		friend std::ostream & operator<<(std::ostream & in, SchedStream const & s)
		{
			unsigned costs = 0;
			for(AnalysedNode a : s.sched)
			{
				costs += a.get_costs();
				in << "[id=" << a.get_node()->get_id() << ",val=" << a[s.type] << ",costs=" << costs << "]\n";
			}
			return in;
		}

private:

};



template<class Compare = SchedStream::p_cmp >
class OrderedSchedule {
public:

	OrderedSchedule(unsigned cores)
	{
		init(cores);
	}

	OrderedSchedule()
	{
		init(1u);
	}

	void init(unsigned cores)
	{
		for(unsigned i=0;i<cores;++i)
		{
			push_back();
		}
	}

	virtual ~OrderedSchedule() { }

	unsigned size() const;

	unsigned push_back()
	{
		unsigned index = sched.size();
		sched.push_back(SchedStream(index));
		lines.insert(&(sched[index]));
		return index;
	}

	void clear()
	{
		sched.clear();
		lines.clear();
	}

	std::vector<unsigned> get_task_map() const;

	SchedStream & operator[](unsigned num)
	{
		return sched[num];
	}

	SchedStream operator[](unsigned num) const
	{
		return sched[num];
	}

	/**
	 * Inserts a Node into the lowest ordered OrderedSchedule
	 * Keeps the OrderedSchedules ordered
	 * complexity is log([sched.size()])
	 */
	stype insert_in_first(AnalysedNode in)
	{
		return insert_in((*(lines.begin()))->get_id(),in);
	}

	stype insert_in(stype index, AnalysedNode in)
	{
		lines.erase(&(sched[index]));
		sched[index].insert(in);
		lines.insert(&(sched[index]));
		return index;
	}

	stype insert_in_last(AnalysedNode in)
	{
		return insert_in((*(lines.rbegin()))->get_id(),in);
	}

	operator UnorderedSchedule() const
	{
		UnorderedSchedule res(sched.size());
		for(unsigned i=0;i<sched.size();++i)
		{
			for(AnalysedNode n : sched[i])
			{
				res[i].push_back(n.get_node());
			}
		}
		return res;
	}

	friend std::ostream & operator<<(std::ostream & in, OrderedSchedule const & s)
	{
		for(unsigned i=0;i<s.sched.size();++i)
		{
			in << "OrderedSchedule for core " << i << ":\n";
			in << s.sched[i];
		}
		return in;
	}



private:

	std::vector<SchedStream> sched;
	std::set<SchedStream const *,Compare> lines;


};

#endif /* ORDEREDSCHEDULE_HPP_ */
