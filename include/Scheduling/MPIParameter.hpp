/*
 * MPIParameter.h
 *
 *  Created on: 12.12.2014
 *      Author: marc
 */

#ifndef MPIPARAMETER_H_
#define MPIPARAMETER_H_

#include <map>

#include <Graph/MCNode.hpp>
#include <Graph/SingleLinkGraph.hpp>

class MPIParameter
{
public:

	MPIParameter();

	virtual ~MPIParameter();

	MPIParameter(std::map<stype, MCNode::cost_type> const & recv, std::map<stype, MCNode::cost_type> const & send, std::map<stype,MCNode::cost_type> const & comm, std::map<stype,MCNode::cost_type> const & wait);

	MCNode::cost_type get_wait(stype num_req) const;

	MCNode::cost_type get_recv_costs(stype num_recvs) const;

	MCNode::cost_type get_send_costs(stype num_sends) const;

	MCNode::cost_type get_in_costs(stype num_recvs) const;

	MCNode::cost_type get_out_costs(stype num_sends) const;

	MCNode::cost_type get_latency(stype source_core = (stype)0, stype target_core = (stype)0) const;

	MCNode::cost_type get_comm_time(stype source_core = (stype)0, stype target_core = (stype)0) const;

	SingleLinkGraph apply_costs(const MCGraph * g) const;

	static MPIParameter default_min();

	static MPIParameter only_latency();

	static MPIParameter pthreads();

	static MPIParameter pthreads_man();

	static MPIParameter distributed();

	static MPIParameter shared();

	static MPIParameter default_skalar(double in);


private:
	double send,recv, waita, waitb, latency, comm;

	void init(std::map<stype, MCNode::cost_type> const & recv, std::map<stype, MCNode::cost_type> const & send, std::map<stype,MCNode::cost_type> const & comm, std::map<stype,MCNode::cost_type> const & wait);

};
#endif /* MPIPARAMETER_H_ */
