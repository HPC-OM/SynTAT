/*
 * ClusterScheduler.hpp
 *
 *  Created on: 21.11.2014
 *      Author: marc
 */

#ifndef CLUSTERSCHEDULER_HPP_
#define CLUSTERSCHEDULER_HPP_
//TODO order UnorderedSchedes


#include <Scheduling/MCScheduler.hpp>

class ClusterScheduler : public MCScheduler {
public:

	enum Mapping
	{
		LOAD_BALANCE, CLUSTER_CONNECT, COMM_MIN, ICS_EXT
	};

	enum Ordering
	{
		MCP,SEND
	};

	ClusterScheduler(MCGraph const * in, MPIParameter const & sp);

	virtual ~ClusterScheduler();

	void set_mapping(Mapping m);

	UnorderedSchedule get_schedule(unsigned num_cores, unsigned * nodes);

	UnorderedSchedule get_schedule_unbound();

	UnorderedSchedule get_schedule_distributed();

	virtual UnorderedSchedule get_cluster() = 0;

	void set_load_mapping(Mapping s);

	void set_ordering(Ordering o);

	//Orderings
	UnorderedSchedule get_mcp_order(UnorderedSchedule const & in);

	UnorderedSchedule get_send_first_order(UnorderedSchedule const & in);

	UnorderedSchedule get_dyn_send_first_order(UnorderedSchedule const & in);

protected:
	//Mappings
	UnorderedSchedule get_load_balance(stype size,UnorderedSchedule & in);

	UnorderedSchedule get_load_balance_connect(stype size,UnorderedSchedule & in);

	UnorderedSchedule get_cluster_connect(stype size,UnorderedSchedule & in);

	UnorderedSchedule get_comm_min(stype size,UnorderedSchedule & in);

	UnorderedSchedule get_comm_merge(stype size,UnorderedSchedule & in);

	UnorderedSchedule get_extend_ics(stype size,UnorderedSchedule & in);






private:
	Mapping maping;
	Ordering order;



};

#endif /* CLUSTERSCHEDULER_HPP_ */
