/*
 * UnorderedSchedule.hpp
 *
 *  Created on: 09.07.2014
 *      Author: marc
 */

#ifndef UNORDEREDSCHEDULE_HPP_
#define UNORDEREDSCHEDULE_HPP_

#include <vector>
#include <list>

#include <types.hpp>

#include <Scheduling/MCSingleSchedule.hpp>
#include <Graph/MCNode.hpp>

class UnorderedSchedule {
public:
	typedef MCSingleSchedule::size_type size_type;
	typedef MCSingleSchedule::cost_type cost_type;


	std::vector<MCSingleSchedule> sched;

	UnorderedSchedule();

	UnorderedSchedule(stype cores);

	virtual ~UnorderedSchedule();

	std::vector<stype> get_node_structure() const;

	stype get_num_cores() const;

	bool check() const;

	stype get_num_tasks() const;

	void clear_empty();

	stype size() const;

	void erase(stype core);

	/**
	 * Creats a new schedule for a core and inserts [node]
	 * @ret the index of the new single schedule
	 */
	stype push_back(const MCNode * node);

	/**
	 * Creats a new schedule for a core and inserts a list of nodes
	 * @ret the index of the new single schedule
	 */
	stype push_back(std::list<const MCNode *> const & in);

	void clear();

	stype get_num_task(stype core) const;

	MCSingleSchedule get_schedule(stype core_num) const;

	MCSingleSchedule & operator[](stype num);

	MCSingleSchedule operator[](stype num) const;

	std::vector<std::vector<std::pair<stype,bool> > > get_adj_matrix() const;

	cost_type get_costs() const;
	cost_type get_max_costs() const;

	std::vector<stype> get_task_map() const;

	friend std::ostream & operator<<(std::ostream & in, UnorderedSchedule const & sched);


private:
};

#endif /* UNORDEREDSCHEDULE_HPP_ */
