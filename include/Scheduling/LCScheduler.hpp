/*
 * LCScheduler.hpp
 *
 *  Created on: 10.07.2014
 *      Author: marc
 */

#ifndef LCSCHEDULER_HPP_
#define LCSCHEDULER_HPP_

#include <Scheduling/MCScheduler.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <Graph/SingleLinkGraph.hpp>
#include <Scheduling/ClusterScheduler.hpp>

class LCScheduler: public ClusterScheduler {
public:

	LCScheduler(MCGraph const * in, MPIParameter const & sp);

	virtual ~LCScheduler();

	virtual UnorderedSchedule get_cluster();

private:

	std::pair<std::list<const MCNode *>, MCNode::cost_type> rec_crit_path_not_visited(const MCNode * first, std::vector<bool> & visited) const;

	std::list<std::list<const MCNode*> > linear_cluster() const;

	UnorderedSchedule buffer;
};

#endif /* LCSCHEDULER_HPP_ */
