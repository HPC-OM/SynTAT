/*
 * ETFScheduler.hpp
 *
 *  Created on: 08.08.2014
 *      Author: marc
 */

#ifndef ETFSCHEDULER_HPP_
#define ETFSCHEDULER_HPP_

#include <Scheduling/MCScheduler.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <Scheduling/OrderedSchedule.hpp>
#include <Graph/MCGraphAnalyser.hpp>


class ETFScheduler: public MCScheduler {
public:
	ETFScheduler(const MCGraph * in, MPIParameter const & sp);

	virtual ~ETFScheduler();

	UnorderedSchedule get_schedule(unsigned num_cores, unsigned * nodes);

	UnorderedSchedule get_schedule_unbound();

	UnorderedSchedule get_schedule_distributed();

	UnorderedSchedule sub_schedule(UnorderedSchedule const & in, MCGraphAnalyser & ga) const;

private:
	struct mapped_mcp_compare
	{
		bool operator()(std::pair<MCGraphAnalyser::lvl_type,unsigned> const & lhs, std::pair<MCGraphAnalyser::lvl_type,unsigned> const & rhs) const
		{
			return lhs.first > rhs.first || (lhs.first == rhs.first && lhs.second < rhs.second);
		}
	};
};

#endif /* ETFSCHEDULER_HPP_ */
