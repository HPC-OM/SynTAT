/*
 * MCSingleSchedule.hpp
 *
 *  Created on: 24.07.2014
 *      Author: marc
 */

#ifndef MCSINGLESCHEDULE_HPP_
#define MCSINGLESCHEDULE_HPP_

#include <list>
#include <ostream>
#include <vector>

#include <Graph/MCNode.hpp>

class MCSingleSchedule {
public:

	typedef std::list<const MCNode *> sched_type;
	typedef unsigned size_type;
	typedef MCNode::cost_type cost_type;

	MCSingleSchedule();

	MCSingleSchedule(sched_type const & in);

	virtual ~MCSingleSchedule();

	sched_type::iterator begin();

	sched_type::iterator end();

	sched_type::const_iterator begin() const;

	sched_type::const_iterator end() const;

	sched_type::iterator insert(sched_type::iterator iterator, const MCNode * node);

	void insert(std::list<const MCNode *> const & in);

	const MCNode * back() const;

	const MCNode * front() const;

	void push_back(const MCNode * in);

	void push_front(const MCNode * in);

	size_type size() const;

	void clear();

	bool empty() const;

	cost_type get_costs() const;

	friend std::ostream & operator<<(std::ostream & in, MCSingleSchedule const & sched);

	std::vector<unsigned> get_ids() const;

	operator std::list<const MCNode*>() const
	{
		return sched;
	}


private:

	sched_type sched;

	cost_type current_costs;
};

#endif /* MCSINGLESCHEDULE_HPP_ */
