/*
 * DSCAlgorithm.hpp
 *
 *  Created on: 25.11.2014
 *      Author: marc
 */

#ifndef DSCALGORITHM_HPP_
#define DSCALGORITHM_HPP_

#include <Scheduling/ClusterScheduler.hpp>

class DSCAlgorithm: public ClusterScheduler {
public:
	DSCAlgorithm(const MCGraph * in, MPIParameter const & sp);

	virtual ~DSCAlgorithm();

	UnorderedSchedule get_cluster();
};

#endif /* DSCALGORITHM_HPP_ */
