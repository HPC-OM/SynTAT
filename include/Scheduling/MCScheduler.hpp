/*
 * MCScheduler.hpp
 *
 *  Created on: 10.07.2014
 *      Author: marc
 */

#ifndef MCSCHEDULER_HPP_
#define MCSCHEDULER_HPP_

#include <Graph/MCGraph.hpp>
#include <Graph/SingleLinkGraph.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <Graph/ClusterGraph.hpp>
#include <Scheduling/MPIParameter.hpp>
#include <structs.hpp>
#include <types.hpp>

class MCScheduler {
public:




	MCScheduler(MCGraph const * in, MPIParameter const & sp);

	virtual ~MCScheduler();

	virtual UnorderedSchedule get_schedule(stype num_cores, stype * nodes) = 0;

	MCGraph const * get_graph() const;

	UnorderedSchedule get_schedule(stype num_cores, stype *nodes, bool merge);

	MCNode::cost_type makespan(UnorderedSchedule const & in, MCGraph const * g) const;

	virtual std::vector<MCNode::cost_type> get_makespans(UnorderedSchedule const & in) const;

	virtual std::vector<MCNode::cost_type> get_makespans(UnorderedSchedule const & in, MCGraph const * g) const;

	virtual UnorderedSchedule get_schedule_unbound() = 0;

	virtual UnorderedSchedule get_schedule_distributed() = 0;

	UnorderedSchedule get_default_schedule();

	UnorderedSchedule get_single_cluster_sched() const;

	/** \brief Tries to reduce the number of cluster to num
	 *
	 */
	UnorderedSchedule bound_unbound_isolated(UnorderedSchedule const & in, stype num_clusters) const;

	/** \brief Reduces the schedule, if one single schedule fits without delay into another
	 *
	 */
	void trim();


protected:
	MCGraph const * g;
	const MPIParameter * sp;
private:
	MCGraph::cost_type shared_comm_costs;
	MCGraph::cost_type distri_comm_costs;
	std::vector<SingleLinkGraph> g_temp;

};
#endif /* MCSCHEDULER_HPP_ */
