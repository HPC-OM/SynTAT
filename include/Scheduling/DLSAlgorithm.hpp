/*
 * DLSAlgorithm.hpp
 *
 *  Created on: 16.09.2014
 *      Author: marc
 */

#ifndef DLSALGORITHM_HPP_
#define DLSALGORITHM_HPP_

#include <Scheduling/MCScheduler.hpp>
#include <Scheduling/Schedule.hpp>
#include <Graph/MCGraphAnalyser.hpp>

class DLSAlgorithm: public MCScheduler {
public:
	DLSAlgorithm(const MCGraph * in, MPIParameter const & sp);

	virtual ~DLSAlgorithm();

	UnorderedSchedule get_schedule(stype num_cores, stype * nodes);

	UnorderedSchedule get_schedule_unbound();

	UnorderedSchedule get_schedule_distributed();

	UnorderedSchedule get_schedule_linear(unsigned num_cores);

private:
	void do_sched_step(Schedule & sched, std::vector<AnalysedNode> const & vs);
};

#endif /* DLSALGORITHM_HPP_ */
