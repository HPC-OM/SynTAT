/*
 * types.hpp
 *
 *  Created on: 07.09.2014
 *      Author: marc
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include <set>

typedef unsigned stype;


template<typename Attr, typename E>
class AttrSet
{
private:

	struct Element
	{
		Attr a;
		E elem;
		stype id;

		Element(Attr a, E elem, stype id) : a(a), elem(elem), id(id)
		{ }

		bool operator<(Element const & in) const
		{
			return a > in.a || (a==in.a && id<in.id);
		}

		operator E()
		{
			return elem;
		}
	};

	typedef std::set<Element> set;

	set elems;

public:

	class iterator
	{
	public:

		iterator(typename set::iterator elem) : elem(elem)
		{

		}

		iterator & operator++()
		{

			return *this;
		}

		bool operator==(iterator it)
		{
			return elem == it.elem;
		}

		bool operator!=(iterator it)
		{
			return elem != it.elem;
		}

		E operator*()
		{
			return this->elem->elem;
		}

	private:
		 typename set::iterator elem;
	};

	class const_iterator
	{
	public:

		const_iterator(typename set::const_iterator elem) : elem(elem)
		{

		}

		const_iterator & operator++()
		{

			return *this;
		}

		bool operator==(iterator it)
		{
			return elem == it.elem;
		}

		bool operator!=(iterator it)
		{
			return elem != it.elem;
		}

		E operator*()
		{
			return this->elem->elem;
		}

	private:
		 typename set::const_iterator elem;
	};


	AttrSet()
	{

	}

	stype size() const
	{
		return elems.size();
	}

	void insert(Attr a, E elem)
	{
		(elems.insert(Element(a,elem,elems.size())));
	}

	iterator begin()
	{
		return elems.begin();
	}

	iterator end()
	{
		return elems.end();
	}

	const_iterator begin() const
	{
		return elems.begin();
	}

	const_iterator end() const
	{
		return elems.end();
	}

	iterator erase(const_iterator it)
	{
		return elems.erase(it);
	}

	void remove(E elem)
	{
		for(typename set::iterator it = elems.begin();it!=elems.end();++it)
		{
			if(it->elem==elem)
			{
				elems.erase(it);
				break;
			}
		}
	}




};

#endif /* TYPES_HPP_ */
