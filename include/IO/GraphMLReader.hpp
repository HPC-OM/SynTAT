/*
 * GraphMLReader.hpp
 *
 *  Created on: 01.08.2014
 *      Author: marc
 */

#ifndef GRAPHMLREADER_HPP_
#define GRAPHMLREADER_HPP_

#include <string>

#include <Graph/SingleLinkGraph.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <IO/XMLElement.hpp>
#include <IO/XMLDocument.hpp>


class GraphMLReader {
public:

	GraphMLReader()
	{
		node_cost_name = "CalcTime";
		node_id_name = "TaskID";
		node_id_prefix = "n";
		edge_comm_bool = "CommVarsBool";
		edge_comm_float = "CommVarsFloat";
		edge_comm_int = "CommVarsInt";

		node_name_length = 4;
	}

	SingleLinkGraph read(std::string const filename) const;

	void write(std::string const filename, MCGraph const * g) const;

	void write(std::string const filename, MCGraph const * g, UnorderedSchedule const & sched) const;

	void set_node_cost_name(std::string const st);

	void set_node_id_name(std::string const st);

	void set_node_id_prefix(std::string const st);


private:
	std::string node_cost_name;
	std::string node_id_name;
	std::string edge_node_ref_name;
	std::string edge_comm_float;
	std::string edge_comm_int;
	std::string edge_comm_bool;
	std::string node_id_prefix;

	unsigned node_name_length;

	static XMLElement get_graphml_root();

	static XMLElement get_graphml_graph();

	static std::string get_color(unsigned num, unsigned num_all);

	static XMLElement get_grapml_key(std::string const _for, std::string const _type, std::string const _name, std::string const _id);

	static XMLElement get_grapml_key_yed();

	template<typename value_type>
	static XMLElement get_graphml_data(std::string const _key, value_type val);

	static XMLElement get_graphml_data_yed(std::string const _key, unsigned node_num, unsigned cluster, unsigned num_clusters, MCNode::cost_type costs, XMLDocument & xmlD);

	static XMLElement get_graphml_node(unsigned id);

	static XMLElement get_graphml_edge(const MCNode * n, unsigned target,XMLDocument & xmlD);

	static std::string int_to_hex_str(unsigned in);

};

#endif /* GRAPHMLREADER_HPP_ */
