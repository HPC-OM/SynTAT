#ifndef XMLELEMENT_HPP
#define XMLELEMENT_HPP

#include <string>
#include <iostream>
#include <list>
#include <map>

#include <boost/lexical_cast.hpp>


class XMLElement
{
	public:
		/**\brief Default constructor.
		 *
		 */
		XMLElement() {}

		/** \brief Constructor for reading an XML file.
		 *  The string should contain an open element tag of a xml document (e.g. "<element spec="spec1">").
		 *  Automatically produces the attribute map attr (e.g. spec => spec1 )
		 */
		XMLElement(std::string const & in) : header(in)
		{
			init();
		}

		virtual ~XMLElement() {}

		std::string get_content() const;

		std::string get_header() const;

		void set_content(std::string const & in);

		std::string get_name() const;

		void set_name(std::string const & in);

		std::map<std::string,std::string>::const_iterator attr_begin() const;
		std::map<std::string,std::string>::const_iterator attr_end() const;

		std::list<XMLElement *>::const_iterator element_begin() const;
		std::list<XMLElement *>::const_iterator element_end() const;

		std::string get_attr(std::string const & name) const;

		void add_element(XMLElement * in);

		void add_attr(std::string name, std::string val);

		template<typename value_type>
		value_type get() const
		{
			value_type res;
			try {
				res = boost::lexical_cast<value_type>(content);
			}
			catch(std::exception & e)
			{
				res = (value_type)boost::lexical_cast<double>(content);
			}

			return res;
		}

		bool operator==(XMLElement const & in) const;

		friend std::ostream & operator<<(std::ostream & os, XMLElement const & xe);

	private:
		std::string header;
		std::string content;

		std::string name;
		std::map<std::string,std::string> attr;
		std::list<XMLElement *> elems;

		void init();

};

#endif // XMLELEMENT_HPP
