#ifndef XMLDOCUMENT_HPP
#define XMLDOCUMENT_HPP

#include <string>
#include <list>
#include <iostream>

#include <IO/XMLElement.hpp>


class XMLDocument
{

	public:
		XMLDocument()
		{
			root = NULL;
			standalone = false;
		}

		~XMLDocument() {}

		XMLElement * get_root();

		void set_root(XMLElement * in);

		void read(std::string const filename);

		void write(std::string const filename);

		unsigned size() const;

		friend std::ostream & operator<<(std::ostream & os, XMLDocument const & xe);

		std::list<XMLElement>::const_iterator elements_begin() const;

		std::list<XMLElement>::const_iterator elements_end() const;

		XMLElement * add_element(XMLElement const & in);

	private:

		XMLElement * root;
		std::list<XMLElement> elems;
		std::string version, encoding;
		bool standalone;

		void recursive_write(std::ofstream & str, XMLElement * in);

};

#endif // XMLDOCUMENT_HPP
