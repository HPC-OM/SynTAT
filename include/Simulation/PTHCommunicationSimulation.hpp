/*
 * PTHCommunicationSimulation.hpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#ifndef PTHCOMMUNICATIONSIMULATION_HPP_
#define PTHCOMMUNICATIONSIMULATION_HPP_

#include <Graph/MCNode.hpp>
#include <Simulation/MCCommunicationSimulation.hpp>

class PTHCommunicationSimulation : public MCCommunicationSimulation {
public:

	typedef float sim_comm_type;

	PTHCommunicationSimulation(const UnorderedSchedule * in);

	virtual ~PTHCommunicationSimulation();


protected:

	struct PTHCommunication
	{
		unsigned partner;
		unsigned byte;
		Communication_type ct;
		unsigned comm_id;
		unsigned core;
		unsigned partner_core;

		unsigned num_val;
		std::string mpi_string;
		unsigned old_id;
	};

	std::vector<stype> acc_num_sends;

	std::vector<std::list<PTHCommunication> > mpi_comms_in, mpi_comms_out;


	virtual std::string declarations();

	virtual std::string initializations();

	virtual std::string deinitializations();


	virtual std::string task_pre_comm(const MCNode * n);

	virtual std::string task_post_comm(const MCNode * n);

private:


	stype num_mpi_comms;

	void init();

};

#endif /* PTHCOMMUNICATIONSIMULATION_HPP_ */
