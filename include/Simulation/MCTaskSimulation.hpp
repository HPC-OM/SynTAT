/*
 * MCTaskSimulation.hpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */




#ifndef MCTASKSIMULATION_HPP_
#define MCTASKSIMULATION_HPP_

#include <sstream>
#include <string>
#include <vector>

#include <Scheduling/UnorderedSchedule.hpp>
#include <Simulation/MCSimulation.hpp>

class MCTaskSimulation : virtual public MCSimulation {
public:

	enum TaskType
	{
		WAIT, VECTOR
	};

	MCTaskSimulation(const UnorderedSchedule * in);

	virtual ~MCTaskSimulation();

	/** \brief Sets the kind of task to simulate a Flow-Graph
	 *
	 */
	void set_task_type(TaskType tc);


	/** \brief Returns needed functions.
	 *
	 */
	virtual std::string functions();

	/** \brief Returns the type of task simulation
	 *
	 */
	virtual TaskType get_task_type();

	std::vector<stype> get_node_structure();

	const UnorderedSchedule * get_sched();

	static std::string get_rdtsc_func_dec();

	static std::string get_rdtsc_func_imp();

	virtual void set_measure_task(bool in,std::string recv_name);

protected:
	double cycles_per_add;
	bool measure_task;
	std::string task_file;
	TaskType tc;

	const UnorderedSchedule * sched;

	std::vector<std::pair<unsigned,unsigned> > eq_vars; // first = node_id, second = costs

	std::string task(const MCNode * n);


	/** \brief Returns variables needed by the tasks
	 *
	 */
	virtual std::string declarations();


	virtual std::string initializations();
	/** \brief Returns the deinitializations of the variables of declarations()
	*
	*/
	virtual std::string deinitializations();

private:
	void init();


	std::string equation_vars();

	std::string equation_vars_init();

	std::string equation_vars_destr();



	std::string tsc_wait_task(const MCNode * n);

	std::string vector_task(const MCNode * n);

	std::string tsc_wait_equation_vars();

	std::string vector_equation_vars();

	std::string tsc_wait_equation_vars_init();

	std::string vector_equation_vars_init();

	std::string tsc_wait_equation_vars_destr();

	std::string vector_equation_vars_destr();

};

#endif /* MCTASKSIMULATION_HPP_ */
