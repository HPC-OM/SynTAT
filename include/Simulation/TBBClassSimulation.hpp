/*
 * TBBClassSimulation.hpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */

#ifndef TBBCLASSSIMULATION_HPP_
#define TBBCLASSSIMULATION_HPP_

#include <Simulation/MCClassSimulation.hpp>
#include <Simulation/TBBCommunicationSimulation.hpp>

#include <Simulation/ExecutionSimulation.hpp>
#include <Simulation/MCClassSimulation.hpp>

class TBBClassSimulation : public MCClassSimulation {
public:
	TBBClassSimulation(const UnorderedSchedule * in,std::string const & name);

	virtual ~TBBClassSimulation();

	std::string get_string();


protected:

	std::string main_function();

	virtual std::string cpp_execute_function();

	virtual std::string cpp_core_function(unsigned core_num);

	virtual std::string execute_function();

	virtual std::string core_function(unsigned core_num);

	std::string initializations();

	std::string cpp_task_funcs();

	std::string task_funcs();

private:

	TBBCommunicationSimulation * tbbcomm;
};

#endif /* TBBCLASSSIMULATION_HPP_ */
