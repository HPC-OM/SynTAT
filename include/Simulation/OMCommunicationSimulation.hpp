/*
 * OMCommunicationSimulation.hpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#ifndef OMCOMMUNICATIONSIMULATION_HPP_
#define OMCOMMUNICATIONSIMULATION_HPP_

#include <Graph/MCNode.hpp>
#include <Simulation/MCCommunicationSimulation.hpp>

class OMCommunicationSimulation : public MCCommunicationSimulation {
public:

	typedef float sim_comm_type;

	std::vector<std::list<std::list<const MCNode *> > > free_in_lvl;

	OMCommunicationSimulation(const UnorderedSchedule * in);

	virtual ~OMCommunicationSimulation();


protected:

	virtual std::string task_pre_comm(const MCNode * n);

	virtual std::string task_post_comm(const MCNode * n);


private:

	std::string recv_file, send_file;

	stype num_mpi_comms;

	void init();

	std::string vector_task(const MCNode * n);

	std::string wait_task(const MCNode * n);

};

#endif /* OMCOMMUNICATIONSIMULATION_HPP_ */
