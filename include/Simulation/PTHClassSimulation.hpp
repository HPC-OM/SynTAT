/*
 * PTHClassSimulation.hpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

// TODO virtual methoden passend für mpi, tbb und openmp integrieren

#ifndef PTHCLASSSIMULATION_HPP_
#define PTHCLASSSIMULATION_HPP_

#include <string>

#include <Simulation/PTHCommunicationSimulation.hpp>
#include <Simulation/ExecutionSimulation.hpp>
#include <Simulation/MCClassSimulation.hpp>

class PTHClassSimulation : public MCClassSimulation {
public:
	PTHClassSimulation(const UnorderedSchedule * in, std::string const & name);

	virtual ~PTHClassSimulation();

	std::string get_string();


protected:

	std::string main_function();

	virtual std::string cpp_execute_function();

	virtual std::string cpp_core_function(unsigned core_num);

	virtual std::string execute_function();

	virtual std::string core_function(unsigned core_num);

private:



};

#endif /* PTHCLASSSIMULATION_HPP_ */
