/*
 * MCCommunicationSimulation.hpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#ifndef MCCOMMUNICATIONSIMULATION_HPP_
#define MCCOMMUNICATIONSIMULATION_HPP_

#include <Graph/MCNode.hpp>
#include <Simulation/MCTaskSimulation.hpp>

class MCCommunicationSimulation : public MCTaskSimulation {
public:

	typedef float sim_comm_type;

	std::vector<stype> acc_num_sends;
	unsigned num_comms, max_comms;
	std::vector<stype> num_sends;


	MCCommunicationSimulation(const UnorderedSchedule * in);

	virtual ~MCCommunicationSimulation();

	static void set_comm_reduction(bool in);

	virtual void set_measure_recv(bool in,std::string recv_name);

	virtual void set_measure_send(bool in,std::string send_name);

	virtual std::string declarations();

	virtual std::string initializations();

	virtual std::string deinitializations();

	std::string task(const MCNode * n);

protected:


	enum Communication_type
	{
		SHARED, DISTRIBUTED
	};

	struct Communication
	{
		unsigned partner;
		unsigned byte;
		Communication_type ct;
		unsigned comm_id;
		unsigned core;
		unsigned partner_core;

		unsigned cbool;
		unsigned cfloat;
		unsigned cint;

		friend std::ostream & operator<<(std::ostream & in, Communication const & c);
	};

	virtual std::string task_pre_comm(const MCNode * n) = 0;

	virtual std::string task_post_comm(const MCNode * n) = 0;


	friend std::ostream & operator<<(std::ostream & in, Communication const & c);

	bool measure_recv,measure_send;
	std::string recv_file, send_file;

	std::vector<std::list<Communication> > comms_in, comms_out;
	std::vector<std::list<Communication> > cores_to_comms;



private:
	static bool reduction;

	void init();

	void find_communications();

};


#endif /* MCCOMMUNICATIONSIMULATION_HPP_ */
