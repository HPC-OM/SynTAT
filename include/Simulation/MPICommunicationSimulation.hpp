/*
 * MPICommunicationSimulation.hpp
 *
 *  Created on: 19.08.2014
 *      Author: marc
 */

#ifndef MPICOMMUNICATIONSIMULATION_HPP_
#define MPICOMMUNICATIONSIMULATION_HPP_

#include <Graph/MCNode.hpp>
#include <Simulation/MCCommunicationSimulation.hpp>

class MPICommunicationSimulation : public MCCommunicationSimulation {
public:

	typedef float sim_comm_type;

	MPICommunicationSimulation(const UnorderedSchedule * in);

	virtual ~MPICommunicationSimulation();

protected:

	struct MPICommunication
	{
		unsigned partner;
		unsigned byte;
		Communication_type ct;
		unsigned comm_id;
		unsigned core;
		unsigned partner_core;

		unsigned num_val;
		std::string mpi_string;
		unsigned old_id;
	};

	std::vector<std::list<MPICommunication> > mpi_comms_in, mpi_comms_out;

	std::string declarations();

	std::string initializations();

	std::string deinitializations();

private:

	stype num_mpi_comms;

	void init();

	virtual std::string task_pre_comm(const MCNode * n);

	virtual std::string task_post_comm(const MCNode * n);

	static std::string get_mpi_type(char);

	static std::string get_mpi_type(float);

	static std::string get_mpi_type(double);

};

#endif /* MPICOMMUNICATIONSIMULATION_HPP_ */
