/*
 * TBBTaskCommunication.hpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */

#ifndef TBBCOMMUNICATIONSIMULATION_HPP_
#define TBBCOMMUNICATIONSIMULATION_HPP_

#include <Simulation/MCCommunicationSimulation.hpp>
#include <Graph/MCNode.hpp>
#include <Graph/ClusterGraph.hpp>

class TBBCommunicationSimulation : public MCCommunicationSimulation {
public:

	ClusterGraph * cluster;

	typedef float sim_comm_type;

	TBBCommunicationSimulation(const UnorderedSchedule * in);

	virtual ~TBBCommunicationSimulation();

protected:

	std::string task_pre_comm(const MCNode * n);

	std::string task_post_comm(const MCNode * n);

	std::string declarations();

	std::string initializations();

	std::string deinitializations();

	void find_task_cluster();

private:

	void init();

};
#endif /* TBBCOMMUNICATIONSIMULATION_HPP_ */
