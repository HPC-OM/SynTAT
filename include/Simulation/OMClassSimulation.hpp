/*
 * OMClassSimulation.hpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

// TODO virtual methoden passend für mpi, tbb und openmp integrieren

#ifndef OMCLASSSIMULATION_HPP_
#define OMCLASSSIMULATION_HPP_

#include <string>

#include <Simulation/OMCommunicationSimulation.hpp>
#include <Simulation/ExecutionSimulation.hpp>
#include <Simulation/MCClassSimulation.hpp>

class OMClassSimulation : public MCClassSimulation {
public:
	OMClassSimulation(const UnorderedSchedule * in, std::string const & name);

	virtual ~OMClassSimulation();

	std::string get_string();



protected:

	std::string main_function();

	virtual std::string cpp_execute_function();

	virtual std::string cpp_core_function(unsigned core_num);

	virtual std::string execute_function();

	virtual std::string core_function(unsigned core_num);

private:

	OMCommunicationSimulation * omcomm;


};

#endif /* OMCLASSSIMULATION_HPP_ */
