/*
 * ComparisonSimulation.hpp
 *
 *  Created on: 22.09.2014
 *      Author: marc
 */

#ifndef COMPARISONSIMULATION_HPP_
#define COMPARISONSIMULATION_HPP_

#include <Simulation/ExecutionSimulation.hpp>
#include <Simulation/MCClassSimulation.hpp>

class ComparisonSimulation : public ExecutionSimulation {
public:

	ComparisonSimulation(std::string const & name);

	virtual ~ComparisonSimulation();

	void add_class_simulation(MCClassSimulation * in);

	std::string get_string();

	void toFile(std::string file);

	void toFile();


protected:

	std::string generate_bash_taurus();

	std::string generate_bash_venus();

	std::string generate_bash_home();

	std::string generate_main();

	std::string generate_make();

	virtual std::string declarations();

	virtual std::string initializations();

	virtual std::string deinitializations();

private:
	std::list<MCClassSimulation *> sims;
	bool mpi_needed,mpi_boost_needed, tbb_needed, pth_needed, om_needed;
	std::vector<stype> max_nodes, max_core_map;
	stype max_cores;

	double estimated_time;
};

#endif /* COMPARISONSIMULATION_HPP_ */
