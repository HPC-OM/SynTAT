/*
 * MCSimulation.hpp
 *
 *  Created on: 21.08.2014
 *      Author: marc
 */

#ifndef MCSIMULATION_HPP_
#define MCSIMULATION_HPP_

#include <sstream>
#include <string>
#include <vector>

#include <Scheduling/UnorderedSchedule.hpp>

class MCSimulation {

public:

	enum SIM_TYPE
	{
			MPI,MPI_BOOST,TBB,PTH,OM,NO_TYPE
	};

	MCSimulation();

	virtual ~MCSimulation();

	/** \brief Returns header includes
	 *
	*/
	std::string includes();

	static std::string ansi_parse(std::string in);

	static void set_repeat_count(stype count);

	SIM_TYPE get_sim_type() const;

protected:


	static stype repeat_count;
    SIM_TYPE type;

	enum Includeables
	{
		VECTOR,IOSTREAM,FSTREAM,MPI_LIB,TBB_LIB, PTH_LIB,OM_LIB, TBB_GRAPH,BOOST_BIND, BOOST_FUNCTION, UTILITY, BOOST_MPI_LIB /*<-- insert there new one */,COUNT
	};

	void add_includeable(Includeables in);


	virtual std::string declarations() = 0;

	virtual std::string initializations() = 0;

	virtual std::string deinitializations() = 0;

private:
	std::vector<bool> inc_libs;
};

#endif /* MCSIMULATION_HPP_ */
