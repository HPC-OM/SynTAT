/*
 * MCClassSimulation.hpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

// TODO virtual methoden passend für mpi, tbb und openmp integrieren

#ifndef MCCLASSSIMULATION_HPP_
#define MCCLASSSIMULATION_HPP_

#include <string>

#include <Simulation/ExecutionSimulation.hpp>
#include <Scheduling/UnorderedSchedule.hpp>
#include <Simulation/MCCommunicationSimulation.hpp>

class MCClassSimulation : public ExecutionSimulation {
public:
	MCClassSimulation(const UnorderedSchedule * in, const std::string & name);

	virtual ~MCClassSimulation() { }

	virtual std::string generate_class();

	virtual std::string generate_cpp();

	const UnorderedSchedule * get_sched();

	void set_measure_recv(bool in,std::string recv_name);

	void set_measure_send(bool in,std::string send_name);

	void set_measure_task(bool in,std::string task_name);

protected:

	MCCommunicationSimulation * comm;

	virtual std::string cpp_execute_function() = 0;

	virtual std::string cpp_core_function(unsigned core_num) = 0;

	virtual std::string execute_function() = 0;

	virtual std::string core_function(unsigned core_num) = 0;

	virtual std::string declarations();

	virtual std::string initializations();

	virtual std::string deinitializations();

private:

};

#endif /* MCCLASSSIMULATION_HPP_ */
