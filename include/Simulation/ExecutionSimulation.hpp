/*
 * ExecutionSimulation.hpp
 *
 *  Created on: 19.09.2014
 *      Author: marc
 */

#ifndef EXECUTIONSIMULATION_HPP_
#define EXECUTIONSIMULATION_HPP_

#include <string>

#include <Simulation/MCSimulation.hpp>

class ExecutionSimulation : virtual public MCSimulation {

public:
	ExecutionSimulation(std::string const & name);

	virtual ~ExecutionSimulation();

	virtual void toFile(std::string file);

	virtual void toFile();

	std::string get_name() const;

	/** \brief Returns the simulation program or a part of it as a std::string
		 *
		 */
	virtual std::string get_string() = 0;

protected:

	std::string name;

private:

};



#endif /* EXECUTIONSIMULATION_HPP_ */
