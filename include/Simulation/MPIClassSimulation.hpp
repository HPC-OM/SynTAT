/*
 * MPIClassSimulation.hpp
 *
 *  Created on: 12.08.2014
 *      Author: marc
 */

// TODO virtual methoden passend für mpi, tbb und openmp integrieren

#ifndef MPICLASSSIMULATION_HPP_
#define MPICLASSSIMULATION_HPP_

#include <string>

#include <Simulation/MPICommunicationSimulation.hpp>
#include <Simulation/ExecutionSimulation.hpp>
#include <Simulation/MCClassSimulation.hpp>

class MPIClassSimulation : public MCClassSimulation {
public:
	MPIClassSimulation(const UnorderedSchedule * in, std::string const & name);

	virtual ~MPIClassSimulation();

	std::string get_string();


protected:

	std::string main_function();

	virtual std::string cpp_execute_function();

	virtual std::string cpp_core_function(unsigned core_num);

	virtual std::string execute_function();

	virtual std::string core_function(unsigned core_num);

private:



};

#endif /* MPICLASSSIMULATION_HPP_ */
