/*
 * SparseMatrix.hpp
 *
 *  Created on: 05.09.2014
 *      Author: marc
 */

#ifndef SPARSEMATRIX_HPP_
#define SPARSEMATRIX_HPP_

#include <limits>
#include <unordered_map>
#include <types.hpp>

template<typename T>
class SparseVector
{
public:

	typedef std::unordered_map<stype,T> vec_type;
	typedef typename std::unordered_map<stype,T>::iterator iterator;
	typedef typename std::unordered_map<stype,T>::const_iterator const_iterator;

	SparseVector(){dim = 0; }

	SparseVector(stype size)
	{
		this->dim = size;
	}

	T & operator[](stype index)
	{
		return elems[index];
	}


	typename vec_type::iterator begin()
	{
		return elems.begin();
	}

	typename vec_type::const_iterator begin() const
	{
		return elems.begin();
	}

	typename vec_type::iterator end()
	{
		return elems.end();
	}

	typename vec_type::const_iterator end() const
	{
		return elems.end();
	}

	stype size() const
	{
		return dim;
	}

	void erase(stype index)
	{
		elems.erase(index);
	}

	bool exist(stype index) const
	{
		return elems.find(index) != elems.end();
	}

	const_iterator find(stype index) const
	{
		return elems.find(index);
	}

	iterator find(stype index)
	{
		return elems.find(index);
	}

private:
	stype dim;
	vec_type elems;
};

template<typename T>
class SparseMatrix
{
public:

	typedef SparseVector<SparseVector<T> > mat_type;
	typedef std::pair<stype,stype> upair;

	SparseMatrix(stype dim1, stype dim2)
	{
		this->dim1 = dim1;
		this->dim2 = dim2;
	}


	SparseVector<T> & operator[](stype index)
	{
		return elems[index];
	}

	SparseVector<T> operator[](stype index) const
	{
		return elems[index];
	}

	typename mat_type::iterator begin()
	{
		return elems.begin();
	}

	typename mat_type::const_iterator begin() const
	{
		return elems.begin();
	}

	typename mat_type::iterator end()
	{
		return elems.end();
	}

	typename mat_type::const_iterator end() const
	{
		return elems.end();
	}

	upair size() const
	{
		return upair(dim1,dim2);
	}

	void erase(stype index)
	{
		elems.erase(index);
	}

	void erase(stype i, stype j)
	{
		elems.erase(i);
		for(auto it = elems.begin();it!=elems.end();++it)
			it->second.erase(j);
	}

	upair max() const
	{
		upair res;
		T maximum = 0;
		bool flag = true;

		for(auto it=begin();it!=end()&&flag;++it)
			for(auto jt=it->second.begin();jt!=it->second.end();++jt)
				if(jt->second > maximum)
				{
					maximum = jt->second;
					flag = false;
					res.first = it->first;
					res.second = jt->first;
					break;
				}

		if(flag) return npos();
		else return res;
	}

	upair find(T val) const
	{
		upair res;
		bool flag = true;

		for(auto it=begin();it!=end()&&flag;++it)
			for(auto jt=it->second.begin();jt!=it->second.end();++jt)
				if(jt->second == val)
				{
					flag = false;
					res.first = it->first;
					res.second = jt->first;
					break;
				}

		if(flag) return npos();
		else return res;
	}

	static upair npos()
	{
		return upair(std::numeric_limits<stype>::infinity(),std::numeric_limits<stype>::infinity());
	}

	bool exist(stype i, stype j) const
	{
		if(elems.find(i) != elems.end())
		{
			return elems[i].exist(j);
		}
		else return false;
	}


private:
	stype dim1, dim2;
	mat_type elems;
};



typedef SparseVector<stype> USparseVector;
typedef SparseMatrix<stype> USparseMatrix;


#endif /* SPARSEMATRIX_HPP_ */
