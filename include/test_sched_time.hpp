/*
 * test_sched_time.hpp
 *
 *  Created on: 17.02.2015
 *      Author: marc
 */

#ifndef TEST_SCHED_TIME_HPP_
#define TEST_SCHED_TIME_HPP_


#include <iostream>
#include <unordered_map>
#include <ctime>

#include <Graph/TestGraphs.hpp>
#include <Graph/MCNode.hpp>
#include <Graph/MCGraph.hpp>
#include <Graph/MCGraph_lib.hpp>

#include <IO/GraphMLReader.hpp>
#include <Graph/MCGraphAnalyser.hpp>

#include <Scheduling/Schedule.hpp>
#include <Scheduling/MCPScheduler.hpp>
#include <Scheduling/ETFScheduler.hpp>
#include <Scheduling/TFScheduler.hpp>
#include <Scheduling/LCScheduler.hpp>
#include <Scheduling/DSCAlgorithm.hpp>
#include <Scheduling/DLSAlgorithm.hpp>
#include <Scheduling/LVLScheduler.hpp>

#include <Simulation/MPIClassSimulation.hpp>
#include <Simulation/TBBClassSimulation.hpp>
#include <Simulation/PTHClassSimulation.hpp>
#include <Simulation/OMClassSimulation.hpp>
#include <Simulation/ComparisonSimulation.hpp>
#include <omp.h>
#include <io_simple.hpp>


inline long long unsigned rdtsc() {
	unsigned long long hi, lo;
	asm volatile ("rdtsc" : "=a"(lo), "=d"(hi));
	return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

void test_sched_time(void)
{
	MPIParameter sp = MPIParameter::pthreads(), sp1 = MPIParameter::shared();
	std::string folder = "/home/marc/hpcom/tgsim_graphs/";


	std::list<stype> cores;

	for(unsigned i=1;i<=16;i+=1)
		cores.push_back(i);

	for(stype i=10;i<=100;i+=10)
	{
		MCScheduler * scheder;
		std::vector<std::string> names;
		SingleLinkGraph sg_t;
		//lvl_connect_graph(&sg_t,100,i,100,10000);
		rand_wert_sim(&sg_t,10,10000,500);
		//std::cout << "heat," << sg_t.size() << "," << sg_t.num_edges() << "\n";

		names.push_back("DSC");
		scheder = (new DSCAlgorithm(&sg_t,sp));

		/*
		names.push_back("ETF");
		scheder = new ETFScheduler(&sg_t,sp);

		names.push_back("MCP");
		scheder = (new MCPScheduler(&sg_t,sp));

		names.push_back("TF");
		scheder = (new TFScheduler(&sg_t,sp));

		names.push_back("LVL");
		scheder = (new LVLScheduler(&sg_t,sp));

		names.push_back("DLS");
		scheder = (new DLSAlgorithm(&sg_t,sp));

		names.push_back("DSC");
		scheder = (new DSCAlgorithm(&sg_t,sp));

		names.push_back("LC");
		scheder = (new LCScheduler(&sg_t,sp));
*/
		unsigned long long start_t, end_t;

			start_t = clock();
			UnorderedSchedule sched = scheder->get_schedule(i,0);
			end_t = clock();
			std::cout << /*(double)sg_t.num_edges()/sg_t.size() << "," <<  sg_t.size()-1 << "," << sg_t.num_edges() << "," <<*/
						(double)(end_t-start_t)/CLOCKS_PER_SEC << "\n";

		delete scheder;

	}

}





#endif /* TEST_SCHED_TIME_HPP_ */
